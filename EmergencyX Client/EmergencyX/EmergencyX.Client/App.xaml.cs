using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using EmergencyX.Client.Styles;
using EmergencyX.Client.ViewModels;
using EmergencyX.Client.Views;
using EmergencyX.Core.Data;
using EmergencyX.Core.Logic;
using log4net;
using log4net.Config;
using ReactiveUI;
using Splat;

namespace EmergencyX.Client
{
	public class App : Application
	{
		private static readonly ILog Log =
			LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public static readonly string LogFile = Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "EmergencyX",
			"EmergencyXClient.log"
		);

		public static Window GetMainWindow
		{
			get
			{
				return Current.ApplicationLifetime switch
				{
					IClassicDesktopStyleApplicationLifetime desktopStyleApplicationLifetime =>
						desktopStyleApplicationLifetime.MainWindow,
					ISingleViewApplicationLifetime singleViewApplicationLifetime =>
						throw new
							NotSupportedException(), // in theory this should only be returned on a mobile device
					_ => null
				};
			}
		}

		public App()
		{
			// Register all views for this app
			Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());

			// Register additional encodings
			Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

			// create logger
			var logrepo = LogManager.GetRepository(Assembly.GetEntryAssembly());
			log4net.GlobalContext.Properties["LogFileName"] = LogFile;
			XmlConfigurator.Configure(logrepo, new FileInfo("log4net.config"));
			Log.Info("Loaded log4net");
			AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
			Task.Run(ModStoreLoader.ModStoreContent.Load);
		}

		private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			Console.WriteLine(e.ExceptionObject);
			Log.Fatal(e.ExceptionObject);
#if DEBUG
			throw (Exception)e.ExceptionObject;
#endif
		}

		public override void Initialize()
		{
			AvaloniaXamlLoader.Load(this);
		}

		public override void OnFrameworkInitializationCompleted()
		{
			if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
			{
				desktop.MainWindow = new MainWindow
				{
					DataContext = new MainWindowViewModel(),
				};
			}

			base.OnFrameworkInitializationCompleted();
		}
	}
}