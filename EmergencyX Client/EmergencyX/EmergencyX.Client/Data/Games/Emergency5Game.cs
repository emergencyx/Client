using ReactiveUI;

namespace EmergencyX.Client.Data.Games
{
	public class InstalledGame : ReactiveObject
	{
		private string _name;
		private string _installationPath;
		private string _version;
		private int _numberOfMods;
		private bool _isOutdated;

		public string Name
		{
			get => _name;
			set => this.RaiseAndSetIfChanged(ref _name, value);
		}

		public string InstallationPath
		{
			get => _installationPath;
			set => this.RaiseAndSetIfChanged(ref _installationPath, value);
		}

		public string Version
		{
			get => _version;
			set => this.RaiseAndSetIfChanged(ref _version, value);
		}

		public int NumberOfMods
		{
			get => _numberOfMods;
			set => this.RaiseAndSetIfChanged(ref _numberOfMods, value);
		}

		public bool IsOutdated
		{
			get => _isOutdated;
			set => this.RaiseAndSetIfChanged(ref _isOutdated, value);
		}
	}
}