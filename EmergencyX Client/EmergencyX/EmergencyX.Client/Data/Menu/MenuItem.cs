using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DynamicData;
using EmergencyX.Client.ViewModels;

namespace EmergencyX.Client.Data.Menu
{
	public class MenuItem
	{
		private string _name;
		private ObservableCollection<SubItem> _subItems;
		private Type _view;

		public MenuItem(string name, ObservableCollection<SubItem> subItems, Type view = null)
		{
			Name = name;
			SubItems = subItems;
			View = view;
		}

		public string Name
		{
			get => _name;
			set => _name = value;
		}

		public ObservableCollection<SubItem> SubItems
		{
			get => _subItems;
			set => _subItems = value;
		}

		public Type View
		{
			get => _view;
			set => _view = value;
		}
	}
}