using System;

namespace EmergencyX.Client.Data.Menu
{
	public class OpenViewRequestPayload
	{
		private object _obj;

		public object Obj
		{
			get => _obj;
			set => _obj = value;
		}

		public Type ObjType
		{
			get => _objType;
			set => _objType = value;
		}

		private Type _objType;

		public OpenViewRequestPayload(object o, Type type)
		{
			Obj = o;
			ObjType = type;
		}
	}
}