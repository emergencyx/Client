using System;
using EmergencyX.Core.Logic.Settings;
using ReactiveUI;

namespace EmergencyX.Client.Data.Menu
{
	public class SubItem : ReactiveObject
	{
		private string _name;
		private string _technicalName;
		private Type _view;
		private bool _isActive;
		private bool _isFavourite;
		private bool _isStarVisible;

		public SubItem(string name, string parentName, Type view = null)
		{
			Name = name;
			TechnicalName = string.Concat(parentName, ".", name);
			View = view;
			if (Settings.AppSettings.FavouriteMenuItem == TechnicalName)
			{
				IsFavourite = true;
			}
			else
			{
				IsFavourite = false;
			}
		}

		public string Name
		{
			get => _name;
			set => this.RaiseAndSetIfChanged(ref _name, value);
		}

		public Type View
		{
			get => _view;
			set => this.RaiseAndSetIfChanged(ref _view, value);
		}

		public string TechnicalName
		{
			get => _technicalName;
			set => this.RaiseAndSetIfChanged(ref _technicalName, value);
		}

		public bool IsActive
		{
			get => _isActive;
			set => this.RaiseAndSetIfChanged(ref _isActive, value);
		}

		public bool IsFavourite
		{
			get => _isFavourite;
			set => this.RaiseAndSetIfChanged(ref _isFavourite, value);
		}

		public bool IsStarVisible
		{
			get => _isStarVisible;
			set => this.RaiseAndSetIfChanged(ref _isStarVisible, value);
		}

		public override string ToString()
		{
			return Name;
		}
	}
}