using EmergencyX.Core.Data.NetworkRequest;
using ReactiveUI;

namespace EmergencyX.Client.Data.ModStore
{
	public class DownloadableItem : ReactiveObject
	{
		private string _requestedVersion;
		private Project _project;
		private string _updateFromVersion;

		public string RequestedVersion
		{
			get => _requestedVersion;
			set => _requestedVersion = value;
		}

		public Project Project
		{
			get => _project;
			set => _project = value;
		}

		public string UpdateFromVersion
		{
			get => _updateFromVersion;
			set => _updateFromVersion = value;
		}

		public DownloadableItem()
		{
		}

		public DownloadableItem(string requestedVersion, Project project, string updateFromVersion) : this()
		{
			RequestedVersion = requestedVersion;
			Project = project;
			UpdateFromVersion = updateFromVersion;
		}
	}
}