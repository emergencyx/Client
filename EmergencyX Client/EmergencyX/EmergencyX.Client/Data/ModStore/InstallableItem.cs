using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Enum;
using ReactiveUI;

namespace EmergencyX.Client.Data.ModStore
{
	public class InstallableItem : ReactiveObject
	{
		private string _fileName;
		private Artifact _artifact;
		private PhonebookGames _game;

		public string FileName
		{
			get => _fileName;
			set => this.RaiseAndSetIfChanged(ref _fileName, value);
		}

		public Artifact Artifact
		{
			get => _artifact;
			set => this.RaiseAndSetIfChanged(ref _artifact, value);
		}

		public PhonebookGames Game
		{
			get => _game;
			set => this.RaiseAndSetIfChanged(ref _game, value);
		}

		public InstallableItem(string fileName, Artifact artifact, PhonebookGames game)
		{
			FileName = fileName;
			Artifact = artifact;
			Game = game;
		}
	}
}