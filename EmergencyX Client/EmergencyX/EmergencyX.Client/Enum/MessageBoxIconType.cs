namespace EmergencyX.Client.Enum
{
	public enum MessageBoxIconType
	{
		None,
		Exclamationmark,
		Questionmark,
		Error,
		Folder,
		Cog
	}
}