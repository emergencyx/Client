using System;

namespace EmergencyX.Client.Events
{
	public class EnableDisableMenuEventArgs : EventArgs
	{
		private bool _disableMenu;

		public bool DisableMenu
		{
			get => _disableMenu;
			set => _disableMenu = value;
		}

		public EnableDisableMenuEventArgs(bool disableMenu)
		{
			DisableMenu = disableMenu;
		}
	}
}