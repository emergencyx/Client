using System;
using EmergencyX.Client.Data.Menu;
using EmergencyX.Client.ViewModels;

namespace EmergencyX.Client.Events
{
	public class MenuSelectionChangedEventArgs : EventArgs
	{
		private Type _selectedView;

		public Type SelectedView
		{
			get => _selectedView;
			set => _selectedView = value;
		}

		public OpenViewRequestPayload Payload
		{
			get => _payload;
			set => _payload = value;
		}

		private OpenViewRequestPayload _payload;
	}
}