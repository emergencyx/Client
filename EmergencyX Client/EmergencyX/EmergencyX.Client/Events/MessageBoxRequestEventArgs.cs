using System;
using EmergencyX.Client.Enum;

namespace EmergencyX.Client.Events
{
	public class MessageBoxRequestEventArgs : EventArgs
	{
		public string Message { get; set; }
		public MessageBoxIconType MessageBoxIconType { get; set; }

		public MessageBoxRequestEventArgs(string message)
		{
			Message = message;
			MessageBoxIconType = MessageBoxIconType.None;
		}

		public MessageBoxRequestEventArgs(string message,
			MessageBoxIconType messageBoxIconType)
		{
			Message = message;
			MessageBoxIconType = messageBoxIconType;
		}
	}
}