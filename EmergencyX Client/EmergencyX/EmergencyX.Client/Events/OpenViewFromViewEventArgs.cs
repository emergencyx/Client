using System;
using EmergencyX.Client.Data.Menu;

namespace EmergencyX.Client.Events
{
	public class OpenViewFromViewEventArgs : EventArgs
	{
		private string _menuItemName;
		private OpenViewRequestPayload _payload;

		public string MenuItemName
		{
			get => _menuItemName;
			set => _menuItemName = value;
		}

		public OpenViewRequestPayload Payload
		{
			get => _payload;
			set => _payload = value;
		}

		public OpenViewFromViewEventArgs(string menuItemName)
		{
			MenuItemName = menuItemName;
		}

		public OpenViewFromViewEventArgs(string menuItemName, OpenViewRequestPayload payload)
		{
			MenuItemName = menuItemName;
			Payload = payload;
		}
	}
}