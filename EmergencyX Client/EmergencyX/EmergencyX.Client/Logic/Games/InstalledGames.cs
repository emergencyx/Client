using System;
using System.Collections.ObjectModel;
using System.IO;
using EmergencyX.Client.Data.Games;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Emergency4.Logic.Modifications;
using EmergencyX.Emergency5.Logic;
using EmergencyX.Emergency5.Logic.Modifications;

namespace EmergencyX.Client.Logic.Games
{
	public class InstalledGames
	{
		public static ObservableCollection<InstalledGame> GetInstalledGamesForHome()
		{
			var list = new ObservableCollection<InstalledGame>();
			if (IsEmergency5Installed)
			{
				var item = new InstalledGame();
				item.Name = "Emergency 5";

				if (SettingsEditor.GameSettings.ParameterGroups != null && !string.IsNullOrEmpty(
					    SettingsEditor.GameSettings.ParameterGroups
						    .Em5GameSettingsGroup.Version))
				{
					item.Version = SettingsEditor.GameSettings.ParameterGroups.Em5GameSettingsGroup
						.Version;
				}
				else
				{
					item.Version = "0.0.0";
				}

				var itemVersionFromGameAppJson = "";
				if (string.IsNullOrEmpty(item.Version) &&
				    !string.IsNullOrEmpty(SettingsEditor.GameApp.Application.ApplicationVersion))
				{
					itemVersionFromGameAppJson =
						SettingsEditor.GameApp.Application.ApplicationVersion;
				}

				// in early game versions the game version is not in the settings, only in the game.app(.json) file
				if (string.IsNullOrEmpty(item.Version) &&
				    !string.IsNullOrEmpty(itemVersionFromGameAppJson))
				{
					item.Version = itemVersionFromGameAppJson;
				}

				if ((item.Version != null && item.Version != "4.2.0") ||
				    (itemVersionFromGameAppJson != "4.2.0" &&
				     !string.IsNullOrEmpty(itemVersionFromGameAppJson)))
				{
					item.IsOutdated = true;
				}
				else
				{
					item.IsOutdated = false;
				}

				item.InstallationPath =
					GamePathLoader.LoadEmergencyInstallationPath();
				item.NumberOfMods = ModificationService.LoadModList().Count;
				list.Add(item);
			}

			if (IsEmergency4Installed)
			{
				var installationPath = Emergency4GameLoader.LoadEmergencyInstallationPath();
				var modsDirectory = Emergency4GameLoader.GetEmergency4ModsPath;
				var modCount = Emergency4ModificationService.FromModsDirectory(modsDirectory).Count;
				var version =
					Emergency4GameLoader.GetEmergency4VersionOrGuess(
						new DirectoryInfo(installationPath));

				list.Add(new InstalledGame()
				{
					Name = "Emergency 4",
					Version = version,
					InstallationPath = installationPath,
					NumberOfMods = modCount
				});
			}

			if (IsEmergencyOneInstalled)
			{
				throw new NotImplementedException();
			}

			return list;
		}

		public static bool IsEmergency5Installed =>
			!string.IsNullOrEmpty(GamePathLoader.LoadEmergencyInstallationPath());

		public static bool IsEmergency4Installed => !string.IsNullOrEmpty(Emergency4GameLoader
			.LoadEmergencyInstallationPath());

		public static bool IsEmergencyOneInstalled => false;
	}
}