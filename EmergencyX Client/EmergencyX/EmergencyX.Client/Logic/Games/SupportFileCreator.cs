using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using EmergencyX.Core.Util;

namespace EmergencyX.Client.Logic.Games
{
	public class SupportFileCreator
	{
		private List<string> _filesForSupportFile;
		private string _supportPath;

		public List<string> FilesForSupportFile
		{
			get => _filesForSupportFile;
			set => _filesForSupportFile = value;
		}

		public SupportFileCreator(string supportPath)
		{
			FilesForSupportFile = new List<string>();
			SupportPath = supportPath;
		}

		public string SupportPath
		{
			get { return _supportPath; }
			set { _supportPath = value; }
		}

		/// <summary>
		/// Creates a ZIP file from the FilesForSupportFile property
		/// </summary>
		/// <returns>The path to the created support file.</returns>
		public string CreateSupportZip(string filename)
		{
			// copy all files into temp dir
			var appDataBasePath = PathUtil.GetEmxAppDataPath();
			var appDataPath = SupportPath;
			var zip = Path.GetFullPath(Path.Combine(appDataBasePath, "EmxEm5Support.zip"));

			if (!Directory.Exists(appDataPath))
			{
				Directory.CreateDirectory(appDataPath);
			}
			else if (Directory.Exists(appDataPath) && Directory.EnumerateFiles(appDataPath).Any())
			{
				Directory.Delete(appDataPath, true);
				Directory.CreateDirectory(appDataPath);
			}

			if (File.Exists(zip))
			{
				File.Delete(zip);
			}

			foreach (var file in FilesForSupportFile)
			{
				File.Copy(file, Path.GetFullPath(Path.Combine(appDataPath, Path.GetFileName(file))));
				File.Delete(file);
			}

			ZipFile.CreateFromDirectory(Path.GetFullPath(appDataPath),
				Path.GetFullPath(Path.Combine(appDataBasePath, filename)),
				CompressionLevel.Fastest, false);

			return zip;
		}
	}
}