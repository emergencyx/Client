using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using log4net;
using Pfim;
using ImageFormat = Pfim.ImageFormat;

namespace EmergencyX.Client.Logic.Images
{
	public class DdsConverter
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(DdsConverter));

		public static Stream GetThumbnail(string path)
		{
			Stream thumb = new MemoryStream();
			using (var img = Pfimage.FromFile(path))
			{
				PixelFormat format;

				if (img.Compressed)
				{
					img.Decompress();
				}

				switch (img.Format)
				{
					case ImageFormat.Rgba32:
						format = PixelFormat.Format32bppArgb;
						break;
					case ImageFormat.Rgb24:
						format = PixelFormat.Format24bppRgb;
						break;
					default:
						Log.Error("Could not convert dds file to Bitmap Stream");
						throw new NotSupportedException();
				}

				var handle = GCHandle.Alloc(img.Data, GCHandleType.Pinned);
				try
				{
					var data = Marshal.UnsafeAddrOfPinnedArrayElement(img.Data, 0);
					var bitmap = new Bitmap(img.Width, img.Height, img.Stride, format, data);
					bitmap.Save(thumb, System.Drawing.Imaging.ImageFormat.Png);
					thumb.Position = 0;
				}
				catch (Exception e)
				{
					Log.Error(e);
				}
				finally
				{
					handle.Free();
				}
			}

			return thumb;
		}
	}
}