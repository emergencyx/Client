using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace EmergencyX.Client.Logic.Images
{
	public class TifConverter
	{
		public static Stream GetJpegMemoryThumbnail(string path, int width, int height)
		{
			Stream thumb = new MemoryStream();
			using (Image i = Image.FromFile(path))
			{
				new Bitmap(i.GetThumbnailImage(width, height, null, IntPtr.Zero)).Save(thumb,
					ImageFormat.Jpeg);
				thumb.Position = 0;
			}

			return thumb;
		}

		public static string ConvertTifTo(string exportType, string path)
		{
			string name;
			ImageFormat format;
			switch (exportType.ToLower().Trim())
			{
				case "jpg":
					name = path.Replace(".tif", ".jpg");
					format = ImageFormat.Jpeg;
					break;
				case "png":
					name = path.Replace(".tif", ".png");
					format = ImageFormat.Png;
					break;
				case "gif":
					name = path.Replace(".tif", ".gif");
					format = ImageFormat.Gif;
					break;
				default:
					throw new NotSupportedException();
			}

			if (File.Exists(name))
			{
				return name;
			}

			using (Image image = Image.FromFile(path))
			{
				image.Save(name, format);
			}

			return name;
		}
	}
}