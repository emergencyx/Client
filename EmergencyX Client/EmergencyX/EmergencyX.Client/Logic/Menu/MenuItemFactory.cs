using System;
using System.Collections.ObjectModel;
using EmergencyX.Client.Data.Menu;
using EmergencyX.Client.Logic.Games;
using EmergencyX.Client.ViewModels;

namespace EmergencyX.Client.Logic.Menu
{
	public class MenuItemFactory
	{
		public MenuItemFactory()
		{
		}

		public ObservableCollection<MenuItem> GetMenuItems()
		{
			var list = new ObservableCollection<MenuItem>();
			list.Add(GetMenuItem(MenuType.EmergencyX));

			if (InstalledGames.IsEmergency5Installed)
			{
				list.Add(GetMenuItem(MenuType.Emergency5));
			}

			if (InstalledGames.IsEmergency4Installed)
			{
				list.Add(GetMenuItem(MenuType.Emergency4));
			}

			if (InstalledGames.IsEmergencyOneInstalled)
			{
				list.Add(GetMenuItem(MenuType.EmergencyOne));
			}

			return list;
		}

		private MenuItem GetMenuItem(MenuType type)
		{
			switch (type)
			{
				case MenuType.Emergency5:
					return GetEmergency5MenuItem();
				case MenuType.Emergency4:
					return GetEmergency4MenuItem();
				case MenuType.EmergencyOne:
					return GetEmergencyOneMenuItem();
				case MenuType.EmergencyX:
					return GetEmergencyXClientMenuItem();
			}

			throw new InvalidOperationException();
		}

		private MenuItem GetEmergencyOneMenuItem()
		{
			throw new NotImplementedException();
		}

		private MenuItem GetEmergency4MenuItem()
		{
			var itemName = Assets.Resources.MenuItemEmergency4Name;
			var subItems = GetStandardGameSubItems(itemName, MenuType.Emergency4);
			subItems.Add(new SubItem(Assets.Resources.SubMenuItemFMS,
				Assets.Resources.MenuItemEmergency4Name, typeof(EmergencyXFMSViewModel)));
			return new MenuItem(itemName, subItems);
		}

		private MenuItem GetEmergency5MenuItem()
		{
			var itemName = Assets.Resources.MenuItemEmergency5Name;
			var subItems = GetStandardGameSubItems(itemName, MenuType.Emergency5);
			subItems.Add(new SubItem(Assets.Resources.SubMenuItemNameCacheClearing,
				Assets.Resources.MenuItemEmergency5Name, typeof(Em5CacheClearingViewModel)));
			subItems.Add(new SubItem(Assets.Resources.SubMenuItemNameCreateSupportZip,
				Assets.Resources.MenuItemEmergency5Name,
				typeof(Em5CreateSupportPackViewModel)));
			subItems.Add(new SubItem(Assets.Resources.SubItemNameEM5Settings,
				Assets.Resources.MenuItemEmergency5Name, typeof(Em5SettingsViewModel)));
			subItems.Add(new SubItem(Assets.Resources.SubMenuItemNameEm5ModdingTools,
				Assets.Resources.MenuItemEmergency5Name, typeof(Em5ModdingToolsViewModel)));
			var item = new MenuItem(itemName, subItems);

			return item;
		}

		private ObservableCollection<SubItem> GetStandardGameSubItems(string parentName, MenuType type)
		{
			var list = new ObservableCollection<SubItem>();

			Type typeMod;
			Type typeScreenshots = null;

			switch (type)
			{
				case MenuType.Emergency5:
					typeMod = typeof(Em5ModListViewModel);
					typeScreenshots = typeof(Em5ScreenshotViewModel);
					break;
				case MenuType.Emergency4:
					typeMod = typeof(Em4ModListViewModel);
					break;
				case MenuType.EmergencyOne:
					throw new NotImplementedException();
				default:
					throw new InvalidOperationException();
			}


			list.Add(new SubItem(Assets.Resources.SubItemNameModifications, parentName, typeMod));
			if (typeScreenshots != null)
			{
				list.Add(new SubItem(Assets.Resources.SubItemNameScreenshots, parentName,
					typeScreenshots));
			}

			return list;
		}

		private MenuItem GetEmergencyXClientMenuItem()
		{
			// Get name of menu item for EmergencyX client
			var emergencyXMenuItemName = Assets.Resources.MenuItemNameEmergencyXClient;

			// get sub items for EmergencyX client
			var subItems = new ObservableCollection<SubItem>();
			subItems.Add(new SubItem(Assets.Resources.SubMenuItemHome, emergencyXMenuItemName,
				typeof(HomeViewModel)));
			subItems.Add(new SubItem(Assets.Resources.SubMenuItemModStore,
				Assets.Resources.MenuItemNameEmergencyXClient,
				typeof(ModStoreViewModel)));
			subItems.Add(new SubItem(Assets.Resources.SubMenuItemNameSettings, emergencyXMenuItemName,
				typeof(SettingsViewModel)));

			// create EmergencyX client menu item
			var item = new MenuItem(emergencyXMenuItemName, subItems);

			return item;
		}
	}
}