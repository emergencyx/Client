using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Events;
using EmergencyX.Core.Logic;
using log4net;

namespace EmergencyX.Client.Logic.NamedPipeMessages;

public class NamedPipeMessageHandler
{
    public static readonly ILog Log =
        LogManager.GetLogger(typeof(NamedPipeMessageHandler));

    public static NamedPipeActionType Handle(CommunicationServerMessageReceivedEvent message)
    {
        if (!NamedPipeMessageValidation.ValidatePattern(message.Message))
        {
            Log.Debug("Emergency Explorer received an invalid message through the named pipe connection.");
            return NamedPipeActionType.None;
        }

        return NamedPipeMessageValidation.ValidateActionType(message.Message);
    }

    public static Project HandleDownloadAction(CommunicationServerMessageReceivedEvent message)
    {
        var id = message.Message.Split("=");

        if (id.Length != 2)
        {
            Log.Error("Unable to correctly split ID");
            return null;
        }

        var project = NamedPipeMessageValidation.ValidateEmergencyExplorerProjectId(id[1]);

        if (project != null)
        {
            return project;
        }

        Log.Error("Got invalid id via named pipe connection");
        return null;
    }
}