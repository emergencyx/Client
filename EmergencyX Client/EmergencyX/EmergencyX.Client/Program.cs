﻿using System;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.ReactiveUI;

namespace EmergencyX.Client
{
	class Program
	{
		// Initialization code. Don't use any Avalonia, third-party APIs or any
		// SynchronizationContext-reliant code before AppMain is called: things aren't initialized
		// yet and stuff might break.
		public static void Main(string[] args) => BuildAvaloniaApp()
			.StartWithClassicDesktopLifetime(args);

		// Avalonia configuration, don't remove; also used by visual designer.
		public static AppBuilder BuildAvaloniaApp()
			=> Environment.OSVersion.Platform == PlatformID.Win32Windows
				? AppBuilder.Configure<App>()
					.UsePlatformDetect()
					.LogToTrace()
					.UseReactiveUI() // needed when Avalonia 0.11 releases and we can use composite corner rendering
				: AppBuilder.Configure<App>()
					.UsePlatformDetect()
					.LogToTrace()
					.UseReactiveUI();
	}
}