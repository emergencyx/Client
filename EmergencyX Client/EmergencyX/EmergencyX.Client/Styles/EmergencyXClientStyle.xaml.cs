using Avalonia.Markup.Xaml;

namespace EmergencyX.Client.Styles
{
	public class EmergencyXClientStyle : Avalonia.Styling.Styles
	{
		public EmergencyXClientStyle() => AvaloniaXamlLoader.Load(this);
	}
}