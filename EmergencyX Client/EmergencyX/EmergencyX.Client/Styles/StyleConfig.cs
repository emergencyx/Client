using System;
using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Markup.Xaml.Styling;
using Avalonia.Media;
using EmergencyX.Client.Assets;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Windows.Logic;

namespace EmergencyX.Client.Styles
{
	public class StyleConfig
	{
		private static StyleConfig _styleManager;
		private StyleInclude _lightStyle;
		private StyleInclude _darkStyle;
		private static readonly object _lock = new object();

		/// <summary>
		/// The list of available styles
		/// </summary>
		public static List<string> GetStyles
		{
			get
			{
				var list = new List<string>();
				list.Add(Resources.ThemeNameLight);
				list.Add(Resources.ThemeNameDark);
				list.Add(Resources.ThemeNameAutomatic);
				return list;
			}
		}

		/// <summary>
		/// Singletone StyleConfig instance
		/// </summary>
		private static StyleConfig StyleManager
		{
			get
			{
				if (_styleManager == null)
				{
					lock (_lock)
					{
						if (_styleManager == null)
						{
							_styleManager = new StyleConfig();
						}
					}
				}

				return _styleManager;
			}
			set => _styleManager = value;
		}

		/// <summary>
		/// Provides the light variant of Avalonia UI
		/// </summary>
		private StyleInclude LightStyle
		{
			get => _lightStyle;
			set => _lightStyle = value;
		}

		/// <summary>
		/// Provides the dark variant of Avalonia UI
		/// </summary>
		private StyleInclude DarkStyle
		{
			get => _darkStyle;
			set => _darkStyle = value;
		}

		/// <summary>
		/// The default constructor for the style manager
		/// </summary>
		private StyleConfig()
		{
			// this could be done inline with out the variable but it seems to not work, at this time
			var uri = new Uri("avares://EmergencyX.Client/Styles/Theme/CustomStyleLight.xaml");
			LightStyle =
				new StyleInclude(uri)
				{
					Source = uri
				};

			uri = new Uri("avares://EmergencyX.Client/Styles/Theme/CustomStyleDark.xaml");
			DarkStyle =
				new StyleInclude(uri)
				{
					Source = uri
				};
		}

		/// <summary>
		/// Is responsible for sorting out default styles and providing the currently configured style setting
		/// </summary>
		/// <returns>The name of the style theme to use</returns>
		/// <exception cref="NotSupportedException">Is thrown when no configured style is used on non windows systems.</exception>
		public static string GetSelectedStyle()
		{
			var setting = Settings.AppSettings.AppStyle;

			if (string.IsNullOrEmpty(setting))
			{
				// try to detect if dark mode should be used
				IUiInformation information;

				if (Platform.IsWindows)
				{
					information = new UiInformation();
				}
				else
				{
					throw new NotSupportedException();
				}

				if (information.IsDarkMode())
				{
					setting = Resources.ThemeNameDark;
				}
			}

			if (GetStyles.All(x => x != setting))
			{
				return GetStyles[0];
			}

			return setting;
		}

		/// <summary>
		/// Exposes a functionality to the outside of this class that allows reloading the apps style
		/// </summary>
		public static void LoadSelectedStyle()
		{
			StyleManager.ApplySelectedStyle();
		}

		/// <summary>
		/// Applys the selected style to the current app
		/// </summary>
		private void ApplySelectedStyle()
		{
			Application.Current.Styles[0] = GetStyle();
		}

		private StyleInclude GetStyle()
		{
			if (Application.Current.Styles.Count == 0)
			{
				return LightStyle;
			}

			if (GetSelectedStyle() == Resources.ThemeNameLight)
			{
				return LightStyle;
			}
			else if (GetSelectedStyle() == Resources.ThemeNameDark)
			{
				return DarkStyle;
			}
			else
			{
				// on non windows systems always use the light theme with automatic selection
				if (GetSelectedStyle() == Resources.ThemeNameAutomatic && !Platform.IsWindows)
				{
					return LightStyle;
				}

				// try to determine if Windows runs "dark" 
				IUiInformation information = new UiInformation();
				if (information.IsDarkMode())
				{
					return DarkStyle;
				}
				else
				{
					return LightStyle;
				}
			}
		}

		/// <summary>
		/// Saves a selected style to the settings class of the app.
		/// </summary>
		/// <param name="style">The name of the style that is set.</param>
		public static void SaveSelectedStyle(string style)
		{
			if (GetStyles.All(x => x != style))
			{
				return;
			}

			Settings.AppSettings.AppStyle = style;
			StyleManager.ApplySelectedStyle();
		}
	}
}