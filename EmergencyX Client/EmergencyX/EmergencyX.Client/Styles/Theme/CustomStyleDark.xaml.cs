using Avalonia.Markup.Xaml;

namespace EmergencyX.Client.Styles.Theme
{
	public class CustomStyleDark : Avalonia.Styling.Styles
	{
		public CustomStyleDark() => AvaloniaXamlLoader.Load(this);
	}
}