using Avalonia.Markup.Xaml;

namespace EmergencyX.Client.Styles.Theme
{
	public class CustomStyleLight : Avalonia.Styling.Styles
	{
		public CustomStyleLight() => AvaloniaXamlLoader.Load(this);
	}
}