using System.Collections.Generic;
using Avalonia.Controls;

namespace EmergencyX.Client.Utility.Avalonia
{
	public class OpenFileUtil
	{
		public static OpenFileDialog GetOpenFileWindow(string title, List<FileDialogFilter> filters)
		{
			var filePicker = new OpenFileDialog();
			filePicker.Title = title;
			filePicker.Filters = filters;

			return filePicker;
		}

		public static OpenFileDialog GetOpenFileWindow(string title, List<FileDialogFilter> filters,
			bool allowmultiple)
		{
			var filePicker = new OpenFileDialog();
			filePicker.Title = title;
			filePicker.Filters = filters;
			filePicker.AllowMultiple = allowmultiple;

			return filePicker;
		}

		public static OpenFileDialog GetOpenFileWindow(string title, List<FileDialogFilter> filters,
			bool allowmultiple, string start)
		{
			var filePicker = new OpenFileDialog();
			filePicker.Title = title;
			filePicker.Filters = filters;
			filePicker.AllowMultiple = allowmultiple;
			filePicker.Directory = start;

			return filePicker;
		}
	}
}