using System;
using System.IO;
using Avalonia.Controls;

namespace EmergencyX.Client.Utility.Avalonia
{
	public class OpenPathUtil
	{
		/// <summary>
		/// Returns a pre configured Avalonia Open Folder Dialog
		/// </summary>
		/// <param name="startDir">The path that the dialog starts with. Can be set to "String.Empty", then the Program Files directory will be used.</param>
		/// <param name="dialogTitel">Optional dialog title, otherwise the program title is used.</param>
		/// <returns></returns>
		public static OpenFolderDialog OpenPathDialog(string startDir, string dialogTitel = null)
		{
			if (string.IsNullOrEmpty(startDir))
			{
				startDir = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
			}

			if (string.IsNullOrEmpty(dialogTitel))
			{
				dialogTitel = Assets.Resources.ApplicationTitle;
			}

			var dialog = new OpenFolderDialog();
			dialog.Directory = Path.GetFullPath(startDir);
			dialog.Title = dialogTitel;

			return dialog;
		}
	}
}