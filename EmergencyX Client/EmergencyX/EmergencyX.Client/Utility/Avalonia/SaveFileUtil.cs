using Avalonia.Controls;

namespace EmergencyX.Client.Utility.Avalonia
{
	public class SaveFileUtil
	{
		public static SaveFileDialog GetSaveFileWindow(string title, string file, string directory)
		{
			var fileSaver = new SaveFileDialog();
			fileSaver.Title = title;
			fileSaver.InitialFileName = file;
			fileSaver.Directory = directory;
			return fileSaver;
		}

		public static SaveFileDialog GetSaveFileWindow(string title, string file, string directory,
			string extension)
		{
			var fileSaver = new SaveFileDialog();
			fileSaver.DefaultExtension = extension;
			fileSaver.Title = title;
			fileSaver.InitialFileName = file;
			fileSaver.Directory = directory;
			return fileSaver;
		}
	}
}