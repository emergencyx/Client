using System;
using System.Linq;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic;

namespace EmergencyX.Client.Utility.Views
{
	public static class Tools
	{
		public static bool IsModOutdated(IGameModification modification, string installedVersionNumber)
		{
			var hash = modification.GetProjectFileHash();

			var onlineProjectList =
				ModStoreLoader.ModStoreContent.GetReleaseAndProjectByProjectFileHash(hash, out var result);

			if (!onlineProjectList)
			{
				return false;
			}

			var latestVersion = ModStoreLoader.ModStoreContent.GetProjectOverview().First(x => x.Id == result.project.Id);

			if (latestVersion.LatestHash ==
			    hash)
			{
				return false;
			}

			var isSuccessParseLocalVersion =
				Version.TryParse(installedVersionNumber, out var installedVersion);
			var isSuccessParseOnlineVersion =
				Version.TryParse(latestVersion.LatestVersion, out var onlineVersion);

			if (!isSuccessParseLocalVersion || !isSuccessParseOnlineVersion)
			{
				// if the version number of that online or local version can't be determined then just assume it is outdated.
				// we already know at this point it is installed
				return true;
			}

			return onlineVersion > installedVersion;
		}
	}
}