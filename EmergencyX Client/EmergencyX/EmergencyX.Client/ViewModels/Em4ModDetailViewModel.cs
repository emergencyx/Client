using System;
using System.Diagnostics;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Avalonia.Media.Imaging;
using EmergencyX.Client.Assets;
using EmergencyX.Client.Data.Menu;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Logic.Images;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Events;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Emergency4.Logic.Modifications;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em4ModDetailViewModel : ViewModelBase, IActivatableViewModel
	{
		private IGameModification _modification;

		private static readonly ILog Log =
			LogManager.GetLogger(typeof(Em4ModDetailViewModel));

		#region Events

		public EventHandler<ModificationChangedEventArgs> ModificationChangedEvent;
		public EventHandler<OpenViewFromViewEventArgs> RequestOpenViewEventFromDetailView;

		#endregion

		public Em4ModDetailViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated(disposables =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			#region AssignCommands

			DeleteGameModification = ReactiveCommand.Create(ExecuteDeleteGameModificationAsync);
			OpenBelongingModificationDirectory =
				ReactiveCommand.Create(ExecuteOpenBelongingModificationDirectory);
			RunModification = ReactiveCommand.Create(ExecuteRunModification);
			RunModificationWithFms = ReactiveCommand.Create(ExecuteRunModificationWithFms);

			#endregion
		}

		private void ExecuteRunModificationWithFms()
		{
			Task.Run(ExecuteRunModification);
			OnRequestOpenViewEventFromDetailView(new OpenViewFromViewEventArgs(
				string.Format("{0}.{1}",
					Assets.Resources.MenuItemEmergency4Name, Assets.Resources.SubMenuItemFMS),
				new OpenViewRequestPayload(Modification, Modification.GetType())));
		}

		public IGameModification Modification
		{
			get => _modification;
			set
			{
				this.RaiseAndSetIfChanged(ref _modification, value);
				this.RaisePropertyChanged("IsDefaultModIcon");
				this.RaisePropertyChanged("GetGameModificationIconPath");
			}
		}

		public bool IsDefaultModIcon => GetGameModificationIconPath == null;

		public Bitmap GetGameModificationIconPath
		{
			get
			{
				try
				{
					if (Modification == null)
					{
						return null;
					}

					var path = Modification.GetModIconName;
					path = Path.Combine(path, Modification.GetModIconName);

					if (File.Exists(path))
					{
						Stream thumbnail;

						try
						{
							thumbnail = DdsConverter.GetThumbnail(path);
						}
						catch (Exception e)
						{
							Log.Error(e);
							return null;
						}

						if (thumbnail == null)
						{
							return null;
						}

						var result = new Bitmap(thumbnail);
						return result;
					}
				}
				catch (Exception e)
				{
					Log.Error(e);
					return null;
				}

				return null;
			}
		}

		#region Commands

		public ReactiveCommand<Unit, Unit> DeleteGameModification { get; }
		public ReactiveCommand<Unit, Unit> OpenBelongingModificationDirectory { get; }
		public ReactiveCommand<Unit, Unit> RunModification { get; }

		public ReactiveCommand<Unit, Unit> RunModificationWithFms { get; }

		#endregion

		public ViewModelActivator Activator { get; }

		~Em4ModDetailViewModel()
		{
			ModificationChangedEvent = null;
			RequestOpenViewEventFromDetailView = null;
			MessageBoxRequestEventHandler = null;
		}

		protected virtual void OnModificationChanged(ModificationChangedEventArgs e)
		{
			ModificationChangedEvent?.Invoke(this, e);
		}

		protected virtual void OnRequestOpenViewEventFromDetailView(OpenViewFromViewEventArgs e)
		{
			RequestOpenViewEventFromDetailView?.Invoke(this, e);
		}

		#region CommandImplementations

		private async void ExecuteDeleteGameModificationAsync()
		{
			try
			{
				OnModificationChanged(
					new ModificationChangedEventArgs(ModificationChangeType.ModificationIsDeleting,
						null));
				Task.Run(() => Emergency4ModificationService.Uninstall(Modification));
				OnModificationChanged(new ModificationChangedEventArgs(
					ModificationChangeType.ModificationDeleted,
					Modification));
				Modification = null;
			}
			catch (Exception e)
			{
				Log.Error($"Mod '{Modification?.Name}' uninstall failed.");
				Log.Error(e);
				OnModificationChanged(new ModificationChangedEventArgs(
					ModificationChangeType.ModificationDeleted,
					new Exception(Resources.ExceptionModUninstallFailed, e)));
			}
		}

		private void ExecuteOpenBelongingModificationDirectory()
		{
			var path = Path.GetFullPath(Modification.DataPath);
			var startCommand = new ProcessStartInfo("explorer.exe", path);

			if (Platform.IsWindows)
			{
				try
				{
					Process.Start(startCommand);
				}
				catch (Exception e)
				{
					Log.Error(e);
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
				}
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		private void ExecuteRunModification()
		{
			Emergency4GameLoader.RunEmergency4(Modification.Name);
		}

		#endregion
	}
}