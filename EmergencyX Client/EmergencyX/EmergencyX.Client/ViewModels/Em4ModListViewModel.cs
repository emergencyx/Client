using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using Avalonia.Collections;
using Avalonia.Controls;
using EmergencyX.Client.Assets;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Utility.Avalonia;
using EmergencyX.Core.Annotations;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Events;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Emergency4.Logic.Modifications;
using EmergencyX.Emergency5.Exceptions;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em4ModListViewModel : ViewModelBase, IActivatableViewModel
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(Em4ModListViewModel));

		private string _dialogMessage;
		private AvaloniaList<Em4ModDetailViewModel> _em4ModificationList;
		private double _installationCurrent;
		private string _installationName;
		private double _installationTotal;
		private bool _isDialogRequired;
		private bool _isInstallationLoading;
		private bool _isErrorDialog;
		private bool _isDisableGui;

		#region Commands

		public ReactiveCommand<Unit, Unit> DialogOkCommand { get; }
		public ReactiveCommand<Unit, Unit> RunEmergency4 { get; }
		public ReactiveCommand<Unit, Unit> InstallEmergency4Mod { get; }
		public ReactiveCommand<Unit, Unit> GoToSettingsCommand { get; }

		public ReactiveCommand<Unit, Unit> OpenModsDir { get; }

		public ReactiveCommand<Unit, Unit> OpenEm4GameDir { get; }

		#endregion

		public ViewModelActivator Activator { get; }

		public Em4ModListViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated(disposables =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
						foreach (var item in Em4ModificationList)
						{
							item.ModificationChangedEvent -= OnModificationChanged;
							item.MessageBoxRequestEventHandler -=
								PassMessageBoxRequestToMainView;
							item.RequestOpenViewEventFromDetailView -=
								RequestOpenViewEventFromDetailView;
						}
					})
					.DisposeWith(disposables);
			});

			#endregion

			#region AssignCommands

			InstallEmergency4Mod = ReactiveCommand.Create(ExecuteInstallModificationAsync);
			DialogOkCommand = ReactiveCommand.Create(ExecuteDialogOkCommand);
			GoToSettingsCommand = ReactiveCommand.Create(ExecuteGoToSettingsCommand);
			OpenModsDir = ReactiveCommand.Create(ExecuteOpenModDirectoryCommand);
			OpenEm4GameDir = ReactiveCommand.Create(ExecuteOpenEm4DirectoryCommand);
			RunEmergency4 = ReactiveCommand.Create(ExecuteRunEmergency4);

			#endregion

			Em4ModificationList = new AvaloniaList<Em4ModDetailViewModel>();
			IsInstallationLoading = false;
			IsDisableGui = false;
			Load();
		}

		public AvaloniaList<Em4ModDetailViewModel> Em4ModificationList
		{
			get => _em4ModificationList;
			set => this.RaiseAndSetIfChanged(ref _em4ModificationList, value);
		}

		public bool IsInstallationLoading
		{
			get => _isInstallationLoading;
			set
			{
				this.RaiseAndSetIfChanged(ref _isInstallationLoading, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public bool IsErrorDialog
		{
			get => _isErrorDialog;
			set
			{
				this.RaiseAndSetIfChanged(ref _isErrorDialog, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public string InstallationName
		{
			get => _installationName;
			set
			{
				if (value != string.Empty)
				{
					this.RaiseAndSetIfChanged(ref _installationName,
						string.Concat(Resources.TextBloxkModInstallationWorkingOn,
							value));
					return;
				}

				this.RaiseAndSetIfChanged(ref _installationName, value);
			}
		}

		public double InstallationTotal
		{
			get => _installationTotal;
			set => this.RaiseAndSetIfChanged(ref _installationTotal, value);
		}

		public double InstallationCurrent
		{
			get => _installationCurrent;
			set => this.RaiseAndSetIfChanged(ref _installationCurrent, value);
		}

		public bool IsDialogRequired
		{
			get => _isDialogRequired;
			set
			{
				_isDialogRequired = this.RaiseAndSetIfChanged(ref _isDialogRequired, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public bool IsDisableGui
		{
			get => _isDisableGui;
			set => this.RaiseAndSetIfChanged(ref _isDisableGui, value);
		}


		public string GetEmergency4InstallationInformation
		{
			get => string.Format(Assets.Resources.InformationTextEmergency4Installation,
				Emergency4GameLoader.LoadEmergencyInstallationPath());
		}

		public bool GetIsDialogOrLoadingScreen
		{
			get
			{
				var temp = IsDialogRequired || IsInstallationLoading || IsErrorDialog;
				DisableGui(temp);
				return temp;
			}
		}

		public string DialogMessage
		{
			get => _dialogMessage;
			set => _dialogMessage = this.RaiseAndSetIfChanged(ref _dialogMessage, value);
		}

		private void ExecuteDialogOkCommand()
		{
			DialogMessage = null;
			IsDialogRequired = false;
			IsErrorDialog = false;
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
		}

		private void ExecuteGoToSettingsCommand()
		{
			OnOpenViewFromViewEvent(new OpenViewFromViewEventArgs(string.Format("{0}.{1}",
				Assets.Resources.MenuItemNameEmergencyXClient,
				Assets.Resources.SubMenuItemNameSettings)));
		}

		private void Load()
		{
			Em4ModificationList.Clear();

			// It would be possible to do this without the foreach loop but it is necessary because only this way we can subscribe to the delete event of the item
			var installationPath = Emergency4GameLoader.LoadEmergencyInstallationPath();
			var modsDirectory = Emergency4GameLoader.GetEmergency4ModsPath;

			if (!modsDirectory.Exists)
			{
				Log.Info("Modifications directory not found, not loading anything");
				Log.Info($"Path is {installationPath}");

				OnRequestMessageBox(new MessageBoxRequestEventArgs(string.Format(
					Resources.ExceptionAnErrorOccuredLogFile,
					App.LogFile), MessageBoxIconType.Exclamationmark));

				return;
			}

			foreach (var modification in Emergency4ModificationService.FromModsDirectory(modsDirectory))
			{
				var model = new Em4ModDetailViewModel { Modification = modification };
				model.ModificationChangedEvent += OnModificationChanged;
				model.MessageBoxRequestEventHandler += PassMessageBoxRequestToMainView;
				model.RequestOpenViewEventFromDetailView += RequestOpenViewEventFromDetailView;
				Em4ModificationList.Add(model);
			}
		}

		private void RequestOpenViewEventFromDetailView(object? sender, OpenViewFromViewEventArgs e)
		{
			OnOpenViewFromViewEvent(e);
		}

		private void EnableDialog(string message, bool isErrorDialog)
		{
			IsDialogRequired = true;
			DialogMessage = message;
			IsErrorDialog = isErrorDialog;
		}

		private void DisableGui(bool disableGui)
		{
			IsDisableGui = disableGui;
		}

		#region EventHandler

		private void PassMessageBoxRequestToMainView([CanBeNull] object sender, MessageBoxRequestEventArgs e)
		{
			OnRequestMessageBox(e);
		}

		private void OnModificationChanged(object sender, ModificationChangedEventArgs e)
		{
			if (!(sender is Em4ModDetailViewModel))
			{
				return;
			}

			switch (e.Type)
			{
				case ModificationChangeType.ModificationIsDeleting:
					DisableGui(true);
					return;
				case ModificationChangeType.ModificationDeleted when e.Payload is Exception exception:
					EnableDialog(exception.Message, true);
					break;
				case ModificationChangeType.ModificationDeleted:
					OnModificationDeleted(e.Payload as IGameModification);
					break;
			}
		}

		private void OnModificationDeleted(IGameModification modification)
		{
			Em4ModificationList.Remove(Em4ModificationList.Single(x => x.Modification == modification));
			DisableGui(false);
		}

		private void OnModificationInstallationStartedEvent(object sender,
			ModificationInstallationStartedEventArgs e)
		{
			// set name of the current zip file
			InstallationName = e.ModName;
			InstallationTotal = e.NoOfFiles;

			InstallationCurrent = 0;

			// unhide progress gui
			if (!IsInstallationLoading)
			{
				IsInstallationLoading = true;
			}
		}

		private void OnModificationInstallationProgress(object sender, InstallationProgressEventArgs e)
		{
			// report progress to gui
			InstallationCurrent = e.Progress;
		}

		private void OnModificationInstallationEnded(object sender, ModificationInstallationEndEventArgs e)
		{
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));

			// hide progress gui, and reload mod list
			InstallationName = string.Empty;
			if (IsInstallationLoading)
			{
				IsInstallationLoading = !IsInstallationLoading;
			}

			InstallationTotal = 0;
			InstallationCurrent = 0;
			Load();

			if (!e.IsFinnishedWithError)
			{
				EnableDialog(Assets.Resources.DialogMessageInstallSuccess, false);
			}
		}

		#endregion

		#region CommandImplementations

		/// <summary>
		///     Runs Emergency 4
		/// </summary>
		/// <exception cref="NotSupportedException"></exception>
		private void ExecuteRunEmergency4()
		{
			try
			{
				Emergency4GameLoader.RunEmergency4();
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
		}

		/// <summary>
		///     Opens the emergency 4 mods dir
		/// </summary>
		/// <exception cref="NotSupportedException"></exception>
		private void ExecuteOpenModDirectoryCommand()
		{
			var path = System.IO.Path.GetFullPath(Emergency4GameLoader.GetEmergency4ModsPath.FullName);
			var startCommand = new ProcessStartInfo("explorer.exe", path);

			if (Platform.IsWindows)
			{
				try
				{
					Process.Start(startCommand);
				}
				catch (Exception e)
				{
					Log.Error(e);
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
				}
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>
		///     Opens the Emergency 4 dir
		/// </summary>
		/// <exception cref="NotSupportedException"></exception>
		private void ExecuteOpenEm4DirectoryCommand()
		{
			Emergency4GameLoader.OpenEmergency4InstallDir();
		}

		/// <summary>
		///     Calls the Emergency mod installer
		/// </summary>
		private async void ExecuteInstallModificationAsync()
		{
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(true));

			// setup for the open file dialoge
			var filter = new List<FileDialogFilter>();
			var extension = new List<string> { "e4mod", "zip" };
			filter.Add(new FileDialogFilter
				{ Name = Resources.OpenFileTextE4ModFiles, Extensions = extension });

			try
			{
				// ask for zip files
				var files = await OpenFileUtil
					.GetOpenFileWindow(Resources.OpenFilePickModTitle, filter, true)
					.ShowAsync(App.GetMainWindow);
				if (files == null || files.Length == 0)
				{
					OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
					DisableGui(false);
					OnRequestMessageBox(
						new MessageBoxRequestEventArgs(Assets.Resources
							.ErrorMessageNothingSelected));
					return;
				}

				// add handlers for progress reporting
				var installationService = new Emergency4ModificationService();
				installationService.ModificationInstallationStartedEvent +=
					OnModificationInstallationStartedEvent;
				installationService.ModificationInstallationProgressEvent +=
					OnModificationInstallationProgress;
				installationService.ModificationEndedEvent += OnModificationInstallationEnded;
				// install the mod in a new thread
				var directory = Emergency4GameLoader.LoadEmergencyInstallationPath();
				await installationService.InstallAsync(files,
					new DirectoryInfo(Path.Combine(directory, "Mods")));
			}
			catch (NotSufficentSpaceForModInstallException diskSpaceException)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(Resources.ExceptionDiskSpaceMessage, true);
				Log.Error(diskSpaceException);
			}
			catch (NotSupportedException notSupportedException)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(Resources.ExceptionNotSupportedMessage, true);
				Log.Error(notSupportedException);
			}
			catch (Exception e)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(string.Format(Resources.ExceptionAnErrorOccuredLogFile, App.LogFile),
					true);
				Log.Error(e);
			}
		}

		#endregion
	}
}