using System;
using System.Reactive;
using System.Reactive.Disposables;
using EmergencyX.Client.Assets;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Emergency5.Logic;
using EmergencyX.Emergency5.Utils;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5CacheClearingViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(Em5CacheClearingViewModel));

		private bool _isShowCacheClearDialog;
		private bool _isBeginCacheClear;
		private string _cacheClearStatus;

		public bool IsShowCacheClearDialog
		{
			get => _isShowCacheClearDialog;
			set => this.RaiseAndSetIfChanged(ref _isShowCacheClearDialog, value);
		}

		public bool IsBeginCacheClear
		{
			get => _isBeginCacheClear;
			set => this.RaiseAndSetIfChanged(ref _isBeginCacheClear, value);
		}

		public string CacheClearStatus
		{
			get => _cacheClearStatus;
			set => this.RaiseAndSetIfChanged(ref _cacheClearStatus, value);
		}

		public string CacheClearDescriptionText
		{
			get => string.Format(Resources.DialogCacheClearingFunctionDescription,
				GamePathLoader.LoadEm5CachePath());
		}

		public ViewModelActivator Activator { get; }

		#region Commands

		public ReactiveCommand<Unit, Unit> BeginClearCacheCommand { get; }
		public ReactiveCommand<Unit, Unit> EndClearCacheCommand { get; }

		#endregion

		public Em5CacheClearingViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			#region AssignCommands

			BeginClearCacheCommand = ReactiveCommand.Create(ExecuteBeginClearCacheCommand);
			EndClearCacheCommand = ReactiveCommand.Create(ExecuteEndClearCacheCommand);

			#endregion

			IsShowCacheClearDialog = false;
			IsBeginCacheClear = true;
		}

		private void ExecuteEndClearCacheCommand()
		{
			IsShowCacheClearDialog = false;
			IsBeginCacheClear = true;
			CacheClearStatus = string.Empty;
		}

		private void ExecuteBeginClearCacheCommand()
		{
			try
			{
				CacheClearing.ClearCache(GamePathLoader.LoadEm5CachePath());
				CacheClearStatus = Assets.Resources.MessageCacheClearSuccess;
			}
			catch (Exception e)
			{
				CacheClearStatus = string.Format("Error: {0}. {1}", e.Message,
					Assets.Resources.ErrorMessageApendixCacheClearFailed);
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}

			IsShowCacheClearDialog = true;
			IsBeginCacheClear = false;
		}
	}
}