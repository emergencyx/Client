using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Avalonia.Controls;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Logic.Games;
using EmergencyX.Client.Utility.Avalonia;
using EmergencyX.Core.Util;
using EmergencyX.Emergency5.Logic;
using EmergencyX.Windows.Logic;
using log4net;
using Newtonsoft.Json;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5CreateSupportPackViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(Em5CreateSupportPackViewModel));

		private bool _isCreationRunning;

		public bool IsCreationRunning
		{
			get => _isCreationRunning;
			set => this.RaiseAndSetIfChanged(ref _isCreationRunning, value);
		}

		public ViewModelActivator Activator { get; }

		#region Commands

		public ReactiveCommand<Unit, Unit> BeginSupportPackCreation { get; }

		#endregion

		public Em5CreateSupportPackViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			#region AssignCommands

			BeginSupportPackCreation = ReactiveCommand.Create(ExecuteBeginSupportPackCreationAsync);

			#endregion

			IsCreationRunning = false;
		}

		private async void ExecuteBeginSupportPackCreationAsync()
		{
			IsCreationRunning = true;
			var dxdiag = Path.Combine(Path.GetTempPath(), "DxDiag.txt");
			var logTemp = Path.Combine(Path.GetTempPath(), "em5logstemp");
			string[] logs;
			string zip;
			string zipname = "EmxEm5Support.zip";
			string supportPath = Path.GetFullPath(Path.Combine(PathUtil.GetEmxAppDataPath(), "support"));
			try
			{
				// create DxDiag file
				await Task.Run(() => DxDiag.CreateDxDiagFile(dxdiag));

				// options for OpenFileDialog
				var filter = new List<FileDialogFilter>();
				var extension = new List<string>();
				extension.Add("log");
				filter.Add(new FileDialogFilter()
					{ Name = Assets.Resources.OpenFileDialogLogFiles, Extensions = extension });
				;
				logs = await OpenFileUtil
					.GetOpenFileWindow(Assets.Resources.OpenFileSelectLog, filter, true, Path
						.GetFullPath(GamePathLoader.LoadEm5LogPath()))
					.ShowAsync(App.GetMainWindow);

				if (logs == null || logs.Length == 0)
				{
					// cancel
					IsCreationRunning = false;
					OnRequestMessageBox(
						new MessageBoxRequestEventArgs(
							Assets.Resources.ErrorMessageNoLogSelected,
							MessageBoxIconType.Exclamationmark));
					return;
				}

				// copy logs to a temp destination so they don't get deleted
				if (!Directory.Exists(logTemp))
				{
					Directory.CreateDirectory(logTemp);
				}

				string tempLog = string.Empty;
				for (int i = 0; i < logs.Length; i++)
				{
					tempLog = Path.Combine(logTemp, Path.GetFileName(logs[i]));
					File.Copy(logs[i], tempLog);
					logs[i] = tempLog;
					tempLog = string.Empty;
				}

				// create emergency 5 settings file
				var settings = Path.Combine(Path.GetTempPath(), "em5settings.json");
				var currentEm5Settings = SettingsEditor.GetSerializedGameSettings();

				if (!File.Exists(settings))
				{
					File.Create(settings).Close();
				}

				File.WriteAllText(settings, currentEm5Settings);

				// create Zip with logs
				var creator = new SupportFileCreator(supportPath);
				creator.FilesForSupportFile.Add(dxdiag);
				creator.FilesForSupportFile.AddRange(logs);
				creator.FilesForSupportFile.Add(settings);
				zip = creator.CreateSupportZip(zipname);

				// save the file
				var savepath = await SaveFileUtil.GetSaveFileWindow(
						Assets.Resources.SaveFileDialogTitleSaveSupportZip, zipname,
						Path.GetFullPath(
							Environment.GetFolderPath(Environment.SpecialFolder.Desktop)))
					.ShowAsync(App.GetMainWindow);

				if (savepath == string.Empty)
				{
					// cancel
					IsCreationRunning = false;
					OnRequestMessageBox(
						new MessageBoxRequestEventArgs(
							Assets.Resources.ErrorMessageNothingSelected));
					return;
				}

				File.Copy(zip, Path.GetFullPath(savepath), true);

				// clean up
				Directory.Delete(supportPath, true);
				File.Delete(zip);
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
			finally
			{
				IsCreationRunning = false;
			}

			IsCreationRunning = false;
		}
	}
}