using System;
using System.Diagnostics;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Avalonia.Media.Imaging;
using Avalonia.Threading;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Utility.Views;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Events;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Emergency5.Data;
using EmergencyX.Emergency5.Logic.Modifications;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5ModDetailViewModel : ViewModelBase, IActivatableViewModel
	{
		private static readonly ILog Log =
			LogManager.GetLogger(typeof(Em5ModDetailViewModel));

		private GameModification _modification;
		private bool _isOutdated;

		public GameModification Modification
		{
			get => _modification;
			set => this.RaiseAndSetIfChanged(ref _modification, value);
		}

		public bool IsDefaultModIcon => GetGameModificationIconPath == null;

		public Bitmap GetGameModificationIconPath
		{
			get
			{
				try
				{
					if (Modification == null)
					{
						return null;
					}

					var path = ModificationService.GetModPath(Modification.Name);
					path = System.IO.Path.Combine(path, Modification.GetModIconName);

					if (System.IO.File.Exists(path))
					{
						return new Bitmap(path);
					}
				}
				catch (Exception e)
				{
					Log.Error(e);
					return null;
				}

				return null;
			}
		}

		public string GetModVersionAsFormattedString =>
			string.Format("{0} {1}", Assets.Resources.WordWithColonVersion,
				Modification.EmergencyProject.Properties.Version);

		public bool IsOutdated
		{
			get => _isOutdated;
			set => this.RaiseAndSetIfChanged(ref _isOutdated, value);
		}

		public ViewModelActivator Activator { get; }

		#region Events

		public EventHandler<ModificationChangedEventArgs> ModificationChangedEvent;

		#endregion

		#region Commands

		public ReactiveCommand<Unit, Unit> DeleteGameModification { get; }

		public ReactiveCommand<Unit, Unit> EnableDisableModificationCommand { get; }

		public ReactiveCommand<Unit, Unit> IncreaseOrderingIndexOfModification { get; }
		public ReactiveCommand<Unit, Unit> DecreaseOrderingIndexOfModification { get; }

		public ReactiveCommand<Unit, Unit> OpenBelongingModificationDirectory { get; }

		#endregion

		public Em5ModDetailViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			#region AssignCommands

			DeleteGameModification = ReactiveCommand.Create(ExecuteDeleteGameModificationAsync);
			EnableDisableModificationCommand = ReactiveCommand.Create(ExcecuteEnableDisableModCommand);
			IncreaseOrderingIndexOfModification = ReactiveCommand.Create(ExecuteInreaseOrderingIndexOfMod);
			DecreaseOrderingIndexOfModification = ReactiveCommand.Create(ExecuteDecreaseOrderingIndexOfMod);
			OpenBelongingModificationDirectory =
				ReactiveCommand.Create(ExecuteOpenBelongingModificationDirectory);

			#endregion
		}

		~Em5ModDetailViewModel()
		{
			ModificationChangedEvent = null;
			MessageBoxRequestEventHandler = null;
		}

		protected virtual void OnModificationChanged(ModificationChangedEventArgs e)
		{
			ModificationChangedEvent?.Invoke(this, e);
		}


		public void CheckIfOutDated()
		{
			var isOutdated = Tools.IsModOutdated(Modification,
				Modification.EmergencyProject.Properties.Version);
			Dispatcher.UIThread.Post(() => { IsOutdated = isOutdated; }, DispatcherPriority.Send);
		}

		#region CommandImplementations

		private async void ExecuteDeleteGameModificationAsync()
		{
			OnModificationChanged(
				new ModificationChangedEventArgs(ModificationChangeType.ModificationIsDeleting, null));
			try
			{
				await Task.Run(() => ModificationService.UninstallModification(Modification));
				OnModificationChanged(
					new ModificationChangedEventArgs(ModificationChangeType.ModificationDeleted,
						Modification));
				Modification = null;
			}
			catch (Exception e)
			{
				Log.Error($"Mod '{Modification?.Name}' uninstall failed.");
				Log.Error(e);
				OnModificationChanged(new ModificationChangedEventArgs(
					ModificationChangeType.ModificationDeleted,
					new Exception(Assets.Resources.ExceptionModUninstallFailed, e)));
			}
		}

		private void ExcecuteEnableDisableModCommand()
		{
			Modification.IsEnabled = !Modification.IsEnabled;
			OnModificationChanged(
				new ModificationChangedEventArgs(ModificationChangeType.ActivationStateChanged, null));
		}

		private void ExecuteDecreaseOrderingIndexOfMod()
		{
			OnModificationChanged(
				new ModificationChangedEventArgs(ModificationChangeType.OrderingIndexDecrease, null));
		}

		private void ExecuteInreaseOrderingIndexOfMod()
		{
			OnModificationChanged(
				new ModificationChangedEventArgs(ModificationChangeType.OrderingIndexIncrease, null));
		}

		private void ExecuteOpenBelongingModificationDirectory()
		{
			var path = System.IO.Path.GetFullPath(Modification.DataPath);
			var startCommand = new ProcessStartInfo("explorer.exe", path);

			if (Platform.IsWindows)
			{
				try
				{
					Process.Start(startCommand);
				}
				catch (Exception e)
				{
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
					Log.Error(e);
				}
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		#endregion
	}
}