using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Avalonia.Collections;
using Avalonia.Controls;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Utility.Avalonia;
using EmergencyX.Core.Annotations;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Events;
using EmergencyX.Core.Logic;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Emergency5.Data;
using EmergencyX.Emergency5.Exceptions;
using EmergencyX.Emergency5.Logic;
using EmergencyX.Emergency5.Logic.Modifications;
using EmergencyX.Emergency5.Utils;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5ModListViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(Em5ModListViewModel));

		private AvaloniaList<Em5ModDetailViewModel> _em5ModificationList;
		private bool _isInstallationLoading;
		private bool _isDialogRequired;
		private bool _isErrorDialog;
		private bool _isDisableGui;
		private string _installationName;
		private string _dialogMessage;
		private double _installationTotal;
		private double _installationCurrent;

		public AvaloniaList<Em5ModDetailViewModel> Em5ModificationList
		{
			get => _em5ModificationList;
			set => this.RaiseAndSetIfChanged(ref _em5ModificationList, value);
		}

		public bool IsInstallationLoading
		{
			get => _isInstallationLoading;
			set
			{
				this.RaiseAndSetIfChanged(ref _isInstallationLoading, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public string InstallationName
		{
			get => _installationName;
			set
			{
				if (value != string.Empty)
				{
					this.RaiseAndSetIfChanged(ref _installationName,
						string.Concat(Assets.Resources.TextBloxkModInstallationWorkingOn,
							value));
					return;
				}

				this.RaiseAndSetIfChanged(ref _installationName, value);
			}
		}

		public double InstallationTotal
		{
			get => _installationTotal;
			set => this.RaiseAndSetIfChanged(ref _installationTotal, value);
		}

		public double InstallationCurrent
		{
			get => _installationCurrent;
			set => this.RaiseAndSetIfChanged(ref _installationCurrent, value);
		}

		public bool IsDialogRequired
		{
			get => _isDialogRequired;
			set
			{
				_isDialogRequired = this.RaiseAndSetIfChanged(ref _isDialogRequired, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public bool GetIsDialogOrLoadingScreen
		{
			get
			{
				var temp = IsDialogRequired || IsInstallationLoading || IsErrorDialog;
				DisableGui(temp);
				return temp;
			}
		}

		public bool IsErrorDialog
		{
			get => _isErrorDialog;
			set
			{
				this.RaiseAndSetIfChanged(ref _isErrorDialog, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public string DialogMessage
		{
			get => _dialogMessage;
			set => _dialogMessage = this.RaiseAndSetIfChanged(ref _dialogMessage, value);
		}

		public bool IsDisableGui
		{
			get => _isDisableGui;
			set => this.RaiseAndSetIfChanged(ref _isDisableGui, value);
		}

		public string GetEmergency5InstallationInformation
		{
			get => string.Format(Assets.Resources.InformationTextEmergency5Installation,
				GamePathLoader.LoadEmergencyInstallationPath());
		}

		public ViewModelActivator Activator { get; }

		#region Commands

		public ReactiveCommand<Unit, Unit> RunEmergency5 { get; }
		public ReactiveCommand<Unit, Unit> InstallEmergency5Mod { get; }

		public ReactiveCommand<Unit, Unit> DialogOkCommand { get; }

		public ReactiveCommand<Unit, Unit> OpenModDirectoryCommand { get; }

		public ReactiveCommand<Unit, Unit> OpenEm5DirectoryCommand { get; }

		public ReactiveCommand<Unit, Unit> GoToSettingsCommand { get; }

		#endregion

		public Em5ModListViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
						foreach (var item in Em5ModificationList)
						{
							item.ModificationChangedEvent -= OnModificationChanged;
							item.MessageBoxRequestEventHandler -=
								PassMessageBoxRequestToMainView;
						}
					})
					.DisposeWith(disposables);
			});

			#endregion

			#region AssignCommands

			RunEmergency5 = ReactiveCommand.Create(ExecuteRunEmergency5);
			InstallEmergency5Mod = ReactiveCommand.Create(ExecuteInstallModificationAsync);
			DialogOkCommand = ReactiveCommand.Create(ExecuteDialogOkCommand);
			OpenModDirectoryCommand = ReactiveCommand.Create(ExcecuteOpenModDirectoryCommand);
			OpenEm5DirectoryCommand = ReactiveCommand.Create(ExecuteOpenEm5DirectoryCommand);
			GoToSettingsCommand = ReactiveCommand.Create(ExecuteGoToSettingsCommand);

			#endregion

			Em5ModificationList = new AvaloniaList<Em5ModDetailViewModel>();
			IsInstallationLoading = false;
			IsDisableGui = false;
			Load();
		}

		private void ExecuteGoToSettingsCommand()
		{
			OnOpenViewFromViewEvent(new OpenViewFromViewEventArgs(string.Format("{0}.{1}",
				Assets.Resources.MenuItemNameEmergencyXClient,
				Assets.Resources.SubMenuItemNameSettings)));
		}

		private void ExecuteDialogOkCommand()
		{
			DialogMessage = null;
			IsDialogRequired = false;
			IsErrorDialog = false;
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
		}

		private void Load()
		{
			if (ModificationService.LoadModList().Count > 0)
			{
				ModificationService.CheckForIncorrectInstalledMods();
			}
			else
			{
				Log.Info("No modifications to check.");
			}

			Em5ModificationList.Clear();

			// It would be possible to do this without the foreach loop but it is necessary because only this way we can subscribe to the delete event of the item
			Em5ModDetailViewModel model;
			foreach (var modification in ModificationService.LoadModList())
			{
				if (!Directory.Exists(modification.DataPath))
				{
					continue;
				}

				model = new Em5ModDetailViewModel();
				model.Modification = modification;
				model.IsOutdated = false;
				model.ModificationChangedEvent += OnModificationChanged;
				model.MessageBoxRequestEventHandler += PassMessageBoxRequestToMainView;
				Em5ModificationList.Add(model);
			}

			Task.Run(CheckIfModsAreOutdatedAsync);
		}

		private void CheckIfModsAreOutdatedAsync()
		{
			// check if the Mod Store contains any mods
			if (ModStoreLoader.ModStoreContent.GetProjectOverview().Count == 0)
			{
				return;
			}

			foreach (var mod in Em5ModificationList)
			{
				Task.Run(mod.CheckIfOutDated);
			}
		}

		private void EnableDialog(string message, bool isErrorDialog)
		{
			IsDialogRequired = true;
			DialogMessage = message;
			IsErrorDialog = isErrorDialog;
		}

		private void DisableGui(bool disableGui)
		{
			IsDisableGui = disableGui;
		}

		#region EventHandler

		private void PassMessageBoxRequestToMainView([CanBeNull] object sender, MessageBoxRequestEventArgs e)
		{
			OnRequestMessageBox(e);
		}

		private void OnModificationChanged(object sender,
			ModificationChangedEventArgs e)
		{
			if (!(sender is Em5ModDetailViewModel))
			{
				return;
			}

			var model = sender as Em5ModDetailViewModel;

			switch (e.Type)
			{
				case ModificationChangeType.ModificationIsDeleting:
					DisableGui(true);
					return;
				case ModificationChangeType.ModificationDeleted when e.Payload is Exception exception:
					EnableDialog(exception.Message, true);
					return;
				// Remove the deleted element form the list
				case ModificationChangeType.ModificationDeleted:
					OnModificationDeleted(e.Payload as GameModification);
					return;
				case ModificationChangeType.OrderingIndexIncrease:
				case ModificationChangeType.OrderingIndexDecrease:
					OnModificationOrderingIndexChanged(e.Type, model);
					break;
				case ModificationChangeType.ActivationStateChanged:
					// nothing to do, only the following save is required
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			ModificationService.SaveModList(
				Em5ModificationList.Select(x => x.Modification).ToList());
		}


		private void OnModificationOrderingIndexChanged(ModificationChangeType t, Em5ModDetailViewModel model)
		{
			int currentIndex;
			Em5ModDetailViewModel itemThatGetsReplaced;
			switch (t)
			{
				case ModificationChangeType.OrderingIndexIncrease:
					currentIndex = Em5ModificationList.IndexOf(model);
					itemThatGetsReplaced = Em5ModificationList[currentIndex - 1];
					Em5ModificationList[currentIndex - 1] = model;
					Em5ModificationList[currentIndex] = itemThatGetsReplaced;
					break;
				case ModificationChangeType.OrderingIndexDecrease:
					currentIndex = Em5ModificationList.IndexOf(model);
					itemThatGetsReplaced = Em5ModificationList[currentIndex + 1];
					Em5ModificationList[currentIndex + 1] = model;
					Em5ModificationList[currentIndex] = itemThatGetsReplaced;
					break;
				default:
					Log.Warn("Ordering Index changed but is not increase or decrease");
					break;
			}
		}

		private void OnModificationDeleted(GameModification modification)
		{
			Em5ModificationList.Remove(Em5ModificationList.Single(x => x.Modification == modification));
			DisableGui(false);
		}

		private void OnModificationInstallationStartedEvent(object sender,
			ModificationInstallationStartedEventArgs e)
		{
			// set name of the current zip file
			InstallationName = e.ModName;
			InstallationTotal = e.NoOfFiles;

			// unhide progress gui
			if (!IsInstallationLoading)
			{
				IsInstallationLoading = true;
			}
		}

		private void OnModificationInstallationProgress(object sender, InstallationProgressEventArgs e)
		{
			// report progress to gui
			InstallationCurrent = e.Progress;
		}

		private void OnModificationInstallationEnded(object sender, ModificationInstallationEndEventArgs e)
		{
			// Enable menu again
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));

			// hide progress gui, and reload mod list
			InstallationName = string.Empty;
			if (IsInstallationLoading)
			{
				IsInstallationLoading = !IsInstallationLoading;
			}

			InstallationTotal = 0;
			InstallationCurrent = 0;
			Load();

			if (!e.IsFinnishedWithError)
			{
				EnableDialog(Assets.Resources.DialogMessageInstallSuccess, false);
			}
		}

		#endregion

		#region CommandImplementations

		/// <summary>
		/// Runs Emergency 5
		/// </summary>
		private void ExecuteRunEmergency5()
		{
			try
			{
				GameRunner.RunEmergency5(Settings.AppSettings.SkipLauncher);
			}
			catch (Exception e)
			{
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
				Log.Error(e);
			}
		}

		/// <summary>
		/// Opens the emergency 5 mods dir
		/// </summary>
		private void ExcecuteOpenModDirectoryCommand()
		{
			var path = Path.GetFullPath(GamePathLoader.LoadEm5AppDataModificationPath());
			var startCommand = new ProcessStartInfo("explorer.exe", path);

			if (Platform.IsWindows)
			{
				try
				{
					Process.Start(startCommand);
				}
				catch (Exception e)
				{
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
					Log.Error(e);
				}
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>
		/// Opens the Emergency 5 dir
		/// </summary>
		private void ExecuteOpenEm5DirectoryCommand()
		{
			GamePathLoader.OpenEmergency5InstallDir();
		}

		/// <summary>
		/// Calls the Emergency mod installer
		/// </summary>
		private async void ExecuteInstallModificationAsync()
		{
			// disable menu to prevent any accidents
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(true));

			// setup for the open file dialog
			var filter = new List<FileDialogFilter>();
			var extension = new List<string> { "zip" };
			filter.Add(new FileDialogFilter()
				{ Name = Assets.Resources.OpenFileTextZipFiles, Extensions = extension });

			try
			{
				// ask for zip files
				var result = await OpenFileUtil
					.GetOpenFileWindow(Assets.Resources.OpenFilePickModTitle, filter, true)
					.ShowAsync(App.GetMainWindow);
				if (result == null || result.Length == 0)
				{
					OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
					DisableGui(false);
					OnRequestMessageBox(
						new MessageBoxRequestEventArgs(Assets.Resources
							.ErrorMessageNothingSelected));
					return;
				}

				// add handlers for progress reporting
				var installationService = new ModificationService();
				installationService.ModificationInstallationStartedEvent +=
					OnModificationInstallationStartedEvent;
				installationService.ModificationInstallationProgressEvent +=
					OnModificationInstallationProgress;
				installationService.ModificationEndedEvent += OnModificationInstallationEnded;

				// install the mod in a new thread
				await installationService.InstallAsync(result);
				CacheClearing.ClearCache(GamePathLoader.LoadEm5CachePath());
			}
			catch (NotSufficentSpaceForModInstallException diskSpaceException)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(Assets.Resources.ExceptionDiskSpaceMessage, true);
				Log.Error(diskSpaceException);
			}
			catch (NotSupportedException notSupportedException)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(Assets.Resources.ExceptionNotSupportedMessage, true);
				Log.Error(notSupportedException);
			}
			catch (Exception e)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(
					string.Format(Assets.Resources.ExceptionAnErrorOccuredLogFile, App.LogFile),
					true);
				Log.Error(e);
			}
		}

		#endregion
	}
}