using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Threading;
using EmergencyX.Client.Assets;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Utility.Avalonia;
using EmergencyX.Core.Events;
using EmergencyX.Core.Logic;
using EmergencyX.Core.Util;
using EmergencyX.Emergency4.Logic.Modifications;
using EmergencyX.Emergency5.Enum;
using EmergencyX.Emergency5.Events;
using EmergencyX.Emergency5.Logic;
using EmergencyX.Emergency5.Logic.Modifications;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5ModdingToolsViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(Em5ModdingToolsViewModel));

		private bool _isEm5UpdateZipCreationRunning;
		private bool _isEmergencyModPackageCreatorRunning;
		private bool _isGeneratingHash;
		private string _fileOne;
		private string _fileTwo;
		private string _diffText;
		private string _hashText;
		private double _diffProgress;
		private double _diffTotal;

		public bool IsEm5UpdateZipCreationRunning
		{
			get => _isEm5UpdateZipCreationRunning;
			set
			{
				this.RaiseAndSetIfChanged(ref _isEm5UpdateZipCreationRunning, value);
				this.RaisePropertyChanged("GetDisableGui");
			}
		}

		public ViewModelActivator Activator { get; }

		#region Commands

		public ReactiveCommand<Unit, Unit> CreateEm5UpdateZip { get; }

		public ReactiveCommand<Unit, Unit> SelectFileOneCommand { get; }

		public ReactiveCommand<Unit, Unit> SelectFileTwoCommand { get; }

		public ReactiveCommand<Unit, Unit> StartModZipEditorCommand { get; }
		
		public ReactiveCommand<Unit, Unit> GenerateSha512Command { get; }
		
		public ReactiveCommand<Unit,Unit> CopyCommand { get; }

		public string FileOne
		{
			get => _fileOne;
			set
			{
				this.RaiseAndSetIfChanged(ref _fileOne, value);
				this.RaisePropertyChanged("GetFileOneName");
				this.RaisePropertyChanged("GetLast30CharsOfFileOneName");
			}
		}

		public string FileTwo
		{
			get => _fileTwo;
			set
			{
				this.RaiseAndSetIfChanged(ref _fileTwo, value);
				this.RaisePropertyChanged("GetFileTwoName");
				this.RaisePropertyChanged("GetLast30CharsOfFileTwoName");
			}
		}

		public double DiffProgress
		{
			get => _diffProgress;
			set => this.RaiseAndSetIfChanged(ref _diffProgress, value);
		}

		public double DiffTotal
		{
			get => _diffTotal;
			set => this.RaiseAndSetIfChanged(ref _diffTotal, value);
		}

		public string DiffText
		{
			get => _diffText;
			set => this.RaiseAndSetIfChanged(ref _diffText, value);
		}

		public string GetFileOneName => Path.GetFileName(FileOne);
		public string GetFileTwoName => Path.GetFileName(FileTwo);

		public string GetLast30CharsOfFileOneName
		{
			get
			{
				var temp = PathUtil.GetLast30CharsOfFileName(Path.GetFileName(FileOne));

				if (string.IsNullOrEmpty(temp))
				{
					return Resources.ModdingToolsTargetNullValue;
				}

				return $"...{temp}";
			}
		}

		public string GetLast30CharsOfFileTwoName
		{
			get
			{
				var temp = PathUtil.GetLast30CharsOfFileName(Path.GetFileName(FileTwo));

				if (string.IsNullOrEmpty(temp))
				{
					return Resources.ModdingToolsTargetNullValue;
				}

				return $"...{temp}";
			}
		}

		public bool IsEmergencyModPackageCreatorRunning
		{
			get => _isEmergencyModPackageCreatorRunning;
			set
			{
				this.RaiseAndSetIfChanged(ref _isEmergencyModPackageCreatorRunning, value);
				this.RaisePropertyChanged("GetDisableGui");
				this.RaisePropertyChanged("GetShowLoadingScreen");
				this.RaisePropertyChanged("LoadingScreenText");
			}
		}
		
		public bool IsGeneratingHash
		{
			get => _isGeneratingHash;
			set
			{
				this.RaiseAndSetIfChanged(ref _isGeneratingHash, value);
				this.RaisePropertyChanged("GetDisableGui");
				this.RaisePropertyChanged("GetShowLoadingScreen");
				this.RaisePropertyChanged("LoadingScreenText");
			}
		}
		
		public string HashText
		{
			get => _hashText;
			set
			{
				this.RaiseAndSetIfChanged(ref _hashText, value);
				this.RaisePropertyChanged("GetHashTextBoxVisible");
			}
		}

		public string LoadingScreenText
		{
			get
			{
				if (IsEmergencyModPackageCreatorRunning)
				{
					return Assets.Resources.DialogMessageCreatingFile;
				} else if (IsGeneratingHash)
				{
					return Assets.Resources.DialogMessageGeneratingHash;
				}
				else
				{
					return string.Empty;
				}
			}
		}

		public bool GetDisableGui => IsEmergencyModPackageCreatorRunning || IsEm5UpdateZipCreationRunning || IsGeneratingHash;
		public bool GetShowLoadingScreen => IsEmergencyModPackageCreatorRunning || IsGeneratingHash; 
		public bool GetHashTextBoxVisible => !string.IsNullOrEmpty(HashText);

		#endregion

		public Em5ModdingToolsViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated(disposables =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			#region AssignCommands

			CreateEm5UpdateZip = ReactiveCommand.Create(ExecuteCreateEm5UpdateZipAsync);

			SelectFileOneCommand = ReactiveCommand.Create(ExecuteSelectFileOneCommandAsync);
			SelectFileTwoCommand = ReactiveCommand.Create(ExecuteSelectFileTwoCommandAsync);

			StartModZipEditorCommand = ReactiveCommand.Create(ExecuteGenerateEmergencyModPackage);

			GenerateSha512Command = ReactiveCommand.Create(ExecuteGenerateSha512HashAsync);

			CopyCommand = ReactiveCommand.Create(ExecuteCopyCommand);

			#endregion

			IsEm5UpdateZipCreationRunning = false;
			DiffText = String.Empty;
		}

		private void ExecuteCopyCommand()
		{
			if (string.IsNullOrEmpty(HashText))
			{
				return;
			}

			if (App.GetMainWindow.Clipboard != null)
			{
				App.GetMainWindow.Clipboard.SetTextAsync(HashText);
			}
		}

		private async void ExecuteGenerateSha512HashAsync()
		{
			
			Dispatcher.UIThread.Post(() => IsGeneratingHash = true);
			HashText = String.Empty;
			// setup for the open file dialog
			var filter = new List<FileDialogFilter>();
			var extension = new List<string> { "zip", "json" };
			filter.Add(new FileDialogFilter()
				{ Name = Assets.Resources.OpenFileTextZipOrJsonFiles, Extensions = extension });
			
			// show dialog
			var result = await OpenFileUtil
				.GetOpenFileWindow(Assets.Resources.OpenFilePickModTitle, filter)
				.ShowAsync(App.GetMainWindow);

			if (result == null || result.Length == 0)
			{
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
				IsGeneratingHash = false;
				OnRequestMessageBox(
					new MessageBoxRequestEventArgs(Assets.Resources
						.ErrorMessageNothingSelected));
				return;
			}

			HashText = await Task.Run(() => ContentIdentification.GetSha512Async(result[0]));
			IsGeneratingHash = false;

		}

		private async void ExecuteSelectFileOneCommandAsync()
		{
			FileOne = await SelectModFileAsync();
		}

		private async void ExecuteSelectFileTwoCommandAsync()
		{
			FileTwo = await SelectModFileAsync();
		}

		private async Task<string> SelectModFileAsync()
		{
			string[] modZip;
			// options for OpenFileDialog
			var filter = new List<FileDialogFilter>();
			var extension = new List<string>();
			extension.Add("zip");
			filter.Add(new FileDialogFilter
				{ Name = Resources.OpenFileTextZipFiles, Extensions = extension });

			modZip = await OpenFileUtil
				.GetOpenFileWindow(Resources.ModdingToolsSelectModdingZIPTitle, filter, false,
					Path
						.GetFullPath(GamePathLoader.LoadEmergencyInstallationPath()))
				.ShowAsync(App.GetMainWindow);

			if (modZip == null || modZip.Length == 0)
			{
				OnRequestMessageBox(
					new MessageBoxRequestEventArgs(Resources.ErrorMessageNothingSelected));
				return null;
			}

			return modZip[0];
		}

		private async void ExecuteCreateEm5UpdateZipAsync()
		{
			if (string.IsNullOrEmpty(FileOne) || string.IsNullOrEmpty(FileTwo))
			{
				EndPatchGeneration(Resources.ErrorMessageNothingSelected);
				return;
			}

			if (FileOne == FileTwo)
			{
				EndPatchGeneration(Resources.ErrorMessageIdenticalFilesSelected);
				return;
			}

			var service = new ModificationService();
			service.ModificationDiffStartedEvent += OnModificationDiffStarted;
			service.ModificationDiffProgressedEvent += OnModificatioDiffProgress;

			// Disable menu to prevent "accidents"
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(true));
			IsEm5UpdateZipCreationRunning = true;

			var result = await Task.Run(() =>
				ModificationService.CompareEmergency5ModificationZips(FileOne, FileTwo, service));

			if (String.IsNullOrEmpty(result))
			{
				// Enable menu again
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
				EndPatchGeneration(string.Format(Resources.ExceptionAnErrorOccuredLogFile,
					App.LogFile));
				Log.Error(
					"An error occured during generation of the patch file. Please check the log for more information and contact the developer.");
			}

			var saveResultToPath = await OpenPathUtil
				.OpenPathDialog(Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
					Resources.DialogTitelSelectFolder)
				.ShowAsync(App.GetMainWindow);

			if (!Directory.Exists(saveResultToPath))
			{
				Log.Error(new DirectoryNotFoundException(saveResultToPath));

				// Enable menu again
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
				EndPatchGeneration(Resources.ErrorMessageDirectoryDoseNotExist);
				return;
			}

			File.Move(result, Path.Combine(saveResultToPath, Path.GetFileName(result)));

			if (File.Exists(Path.Combine(saveResultToPath, Path.GetFileName(result))))
			{
				File.Delete(result);
			}
			else
			{
				Log.Error(new FileNotFoundException(
					$"File {Path.Combine(saveResultToPath, Path.GetFileName(result))} not found! Failed to delete temporary files."));
				// Enable menu again
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
				EndPatchGeneration(string.Format(Resources.ExceptionAnErrorOccuredLogFile,
					App.LogFile));
			}

			// Enable menu again
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
			EndPatchGeneration();
		}

		private void EndPatchGeneration(string text = "")
		{
			FileOne = null;
			FileTwo = null;
			DiffText = String.Empty;
			IsEm5UpdateZipCreationRunning = false;

			if (!string.IsNullOrEmpty(text))
			{
				OnRequestMessageBox(new MessageBoxRequestEventArgs(text,
					MessageBoxIconType.Exclamationmark));
			}
		}

		private void OnModificatioDiffProgress(object sender, ModificationDiffProgressedEventArgs e)
		{
			Dispatcher.UIThread.Post(() => { DiffProgress = e.Progress; });
		}

		private void OnModificationDiffStarted(object sender, ModificationDiffStartedEventArgs e)
		{
			Dispatcher.UIThread.Post(() =>
			{
				DiffText = e.DiffStep switch
				{
					ModificationDiffStep.Prerequisites => Resources
						.DialogMessageCollectingData,
					ModificationDiffStep.FileCreation => Resources.DialogMessageCreatingFile,
					_ => String.Empty
				};

				DiffProgress = 0.0;
				DiffTotal = e.Total;
			});
		}

		private async Task<string> SelectModFolderAsync()
		{
			var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			// options for OpenFolderDialog
			path = await OpenPathUtil.OpenPathDialog(path, Resources.OpenFolderDialogModFolder)
				.ShowAsync(App.GetMainWindow);

			if (path == string.Empty)
			{
				OnRequestMessageBox(
					new MessageBoxRequestEventArgs(Resources.ErrorMessageNothingSelected));
				return null;
			}

			return path;
		}

		private async void ExecuteGenerateEmergencyModPackage()
		{
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(true));
			Dispatcher.UIThread.Post(() => IsEmergencyModPackageCreatorRunning = true);

			var path = await SelectModFolderAsync();

			if (string.IsNullOrEmpty(path) || !Directory.Exists(path))
			{
				EndModPackageGeneration(Resources.MessageDirectoryNotFound);
				return;
			}

			// find out if Em5 or Em4
			var searchModFile = Directory.GetFiles(path, "project.json", SearchOption.AllDirectories);

			if (searchModFile.Length == 0)
			{
				searchModFile = Directory.GetFiles(path, "e4mod.info", SearchOption.AllDirectories);
			}

			if (searchModFile.Length > 1 || searchModFile.Length == 0)
			{
				Log.Info("Nothing selected!");
				EndModPackageGeneration(Resources.DialogMessageFoundMoreThenOneProjectFile);
				return;
			}

			var isEm5 = searchModFile[0].EndsWith("project.json");

			string result = "";
			string err = "";

			try
			{
				if (isEm5)
				{
					Log.Info("Start Em5 process");
					result = await ModificationService.CreateModificationPackage(path);
				}
				else
				{
					Log.Info("Start Em4 process");
					result = await Emergency4ModificationService.GenerateModificationZip(
						path);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Log.Error(e);
				result = null;
				err = e.Message;
			}

			if (string.IsNullOrEmpty(result) || !File.Exists(result))
			{
				Log.Info("File selected is null or not existend");
				EndModPackageGeneration(
					string.Format(Resources.DialogMessageModPackageGenerationFailed,
						err));
				return;
			}

			// remove .. from path
			result = Path.GetFullPath(result);
			EndModPackageGeneration(string.Format(Resources.DialogMessageModPackageGenerated,
				result));
		}

		private void EndModPackageGeneration(string text = "")
		{
			IsEmergencyModPackageCreatorRunning = false;

			if (!string.IsNullOrEmpty(text))
			{
				OnRequestMessageBox(new MessageBoxRequestEventArgs(text,
					MessageBoxIconType.Exclamationmark));
			}
		}
	}
}