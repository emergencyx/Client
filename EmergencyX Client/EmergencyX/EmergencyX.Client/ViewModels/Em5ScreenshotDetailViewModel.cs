using System.Reactive.Disposables;
using Avalonia.Media.Imaging;
using EmergencyX.Client.Logic.Images;
using EmergencyX.Emergency5.Data;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5ScreenshotDetailViewModel : ViewModelBase, IActivatableViewModel
	{
		private Screenshot _screenshot;
		private Bitmap _thumbnail;
		private bool _isMarkedForExport;

		public ViewModelActivator Activator { get; }

		public Screenshot Screenshot
		{
			get => _screenshot;
			set
			{
				this.RaiseAndSetIfChanged(ref _screenshot, value);
				this.RaisePropertyChanged("GetPathAsSource");
			}
		}

		public Bitmap GetPathAsSource
		{
			get
			{
				if (_thumbnail == null)
				{
					_thumbnail =
						new Bitmap(TifConverter.GetJpegMemoryThumbnail(Screenshot.Path, 450,
							300));
				}

				return _thumbnail;
			}
		}

		public bool IsMarkedForExport
		{
			get => _isMarkedForExport;
			set => this.RaiseAndSetIfChanged(ref _isMarkedForExport, value);
		}

		public Em5ScreenshotDetailViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			IsMarkedForExport = false;
		}

		public void RemoveFromExport()
		{
			if (IsMarkedForExport)
			{
				IsMarkedForExport = false;
			}
		}
	}
}