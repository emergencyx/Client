using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using Avalonia.Collections;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Logic.Images;
using EmergencyX.Client.Utility.Avalonia;
using EmergencyX.Emergency5.Logic.Screenshots;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5ScreenshotViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(Em5ModListViewModel));

		private AvaloniaList<Em5ScreenshotDetailViewModel> _screenshots;
		private bool _isDisableGui;

		public AvaloniaList<Em5ScreenshotDetailViewModel> Screenshots
		{
			get => _screenshots;
			set => this.RaiseAndSetIfChanged(ref _screenshots, value);
		}

		public bool IsDisableGui
		{
			get => _isDisableGui;
			set => this.RaiseAndSetIfChanged(ref _isDisableGui, value);
		}


		#region Commands

		public ReactiveCommand<string, Unit> ExportImagesCommand { get; }
		public ReactiveCommand<Unit, Unit> MarkAllAsExport { get; }

		#endregion

		public ViewModelActivator Activator { get; }

		public Em5ScreenshotViewModel()
		{
			#region Setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			Screenshots = new AvaloniaList<Em5ScreenshotDetailViewModel>();
			Load();

			ExportImagesCommand = ReactiveCommand.Create<string>(ExecuteExportImageCommand);
			MarkAllAsExport = ReactiveCommand.Create(ExecuteMarkAllAsExport);
		}

		private void ExecuteMarkAllAsExport()
		{
			foreach (var image in Screenshots)
			{
				image.IsMarkedForExport = !image.IsMarkedForExport;
			}
		}

		private void ExecuteExportImageCommand(string type)
		{
			// check if there are any to export
			if (Screenshots.Count(x => x.IsMarkedForExport) == 0)
			{
				OnRequestMessageBox(
					new MessageBoxRequestEventArgs(Assets.Resources.ErrorMessageNothingSelected));
				return;
			}

			if (Screenshots.Count(x => x.IsMarkedForExport) == 1)
			{
				try
				{
					ExecuteScreenshotSingleExportAsync(type);
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					Log.Error(e);
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
				}

				return;
			}

			ExecuteScreenshotMultipleExportAsync(type);
		}

		private async void ExecuteScreenshotMultipleExportAsync(string type)
		{
			string extension = null;

			// check for supported input
			try
			{
				extension = CheckExportType(type);
				if (string.IsNullOrEmpty(extension))
				{
					throw new NotSupportedException();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
				return;
			}

			// ask user for a folder where these images should be put in
			string saveTo = await OpenPathUtil.OpenPathDialog(
				Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
				Assets.Resources.OpenFolderDialogTitlePickImageExportPath).ShowAsync(App.GetMainWindow);

			// cancel
			if (string.IsNullOrEmpty(saveTo) || !Directory.Exists(saveTo))
			{
				OnRequestMessageBox(new MessageBoxRequestEventArgs(
					Assets.Resources.ErrorMessageNothingSelected));
				return;
			}

			// convert each image
			foreach (var image in Screenshots.Where(x => x.IsMarkedForExport))
			{
				// convert...
				var convertedFile =
					TifConverter.ConvertTifTo(extension, image.Screenshot.Path);

				// where to put it
				string destinationFile =
					Path.GetFullPath(Path.Combine(saveTo, Path.GetFileName(convertedFile)));

				// ensure unique filename
				if (File.Exists(destinationFile))
				{
					var timeAsString = DateTime.Now.ToString("yyyyMMdd_HH-mm-ss-fff_");
					destinationFile = Path.GetFullPath(Path.Combine(saveTo,
						string.Concat(timeAsString, Path.GetFileName(destinationFile))));
				}

				// move file to export directory
				File.Move(convertedFile, destinationFile, false);

				// unmark export flag
				Screenshots.Single(x => x.Screenshot.ScreenshotName == image.Screenshot.ScreenshotName)
					.RemoveFromExport();
			}

			OnRequestMessageBox(
				new MessageBoxRequestEventArgs(Assets.Resources.DialogMessageImageExportSuccess));
		}

		/// <summary>
		/// Loads the data for the view
		/// </summary>
		private void Load()
		{
			Screenshots.Clear();
			var temp = new AvaloniaList<Em5ScreenshotDetailViewModel>();

			foreach (var screenshot in ScreenshotService.LoadScreenshots())
			{
				temp.Add(new Em5ScreenshotDetailViewModel()
				{
					Screenshot = screenshot,
				});
			}

			Screenshots.AddRange(temp.OrderByDescending(x => x.Screenshot.ScreenshotName));
		}

		/// <summary>
		/// Handles activities related to gui en/disableing
		/// </summary>
		/// <param name="disable">Indicate whether the gui should be disabled</param>
		private void DisableGui(bool disable)
		{
			IsDisableGui = disable;
		}

		/// <summary>
		/// Exports a single tif screenshot into the requested file type
		/// </summary>
		/// <param name="exportType">The type that is requested (eg jpg)</param>
		private async void ExecuteScreenshotSingleExportAsync(string exportType)
		{
			string extension = null;

			// check for supported input
			try
			{
				extension = CheckExportType(exportType);
				if (string.IsNullOrEmpty(extension))
				{
					throw new NotSupportedException();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
				return;
			}

			// convert...
			var convertedFile = TifConverter.ConvertTifTo(exportType,
				Screenshots.Single(x => x.IsMarkedForExport).Screenshot.Path);

			// name of the file for the save dialog
			var fileName = Path.GetFileName(convertedFile);

			// show a nice save file dialog
			var exports = await SaveFileUtil.GetSaveFileWindow("", fileName,
				Path.GetFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)),
				extension).ShowAsync(App.GetMainWindow);

			// cancel
			if (string.IsNullOrEmpty(exports) || !exports.EndsWith(extension))
			{
				OnRequestMessageBox(new MessageBoxRequestEventArgs(
					Assets.Resources.ErrorMessageNothingSelected));
				return;
			}

			File.Move(convertedFile, exports, true);

			Screenshots.Single(x => x.IsMarkedForExport).RemoveFromExport();
			OnRequestMessageBox(
				new MessageBoxRequestEventArgs(Assets.Resources.DialogMessageImageExportSuccess));
		}

		/// <summary>
		/// Checks if the given type is supported by the application
		/// </summary>
		/// <param name="exportType">The type that is to be checked (ie jgp</param>
		/// <returns></returns>
		/// <exception cref="NotSupportedException">This is thrown when an invalid extension is given</exception>
		private string CheckExportType(string exportType)
		{
			string extension;
			switch (exportType.ToLower().Trim())
			{
				case "jpg":
				case "gif":
				case "png":
					extension = exportType.ToLower().Trim();
					break;
				default:
					throw new NotSupportedException();
			}

			return extension;
		}
	}
}