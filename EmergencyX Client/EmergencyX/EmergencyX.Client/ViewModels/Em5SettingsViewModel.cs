using System;
using System.Reactive;
using System.Reactive.Disposables;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Emergency5.Logic;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class Em5SettingsViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(Em5SettingsViewModel));

		private bool _isShineThroughActive;
		private bool _isBuildingClippingActive;
		private bool _showGroundMarkersForAllOwnUnits;
		private int _screenshotResolutionX;
		private int _screenshotResolutionY;

		public ViewModelActivator Activator { get; }

		public bool IsShineThroughActive
		{
			get => _isShineThroughActive;
			set => this.RaiseAndSetIfChanged(ref _isShineThroughActive, value);
		}

		public bool IsBuildingClippingActive
		{
			get => _isBuildingClippingActive;
			set => this.RaiseAndSetIfChanged(ref _isBuildingClippingActive, value);
		}

		public bool ShowGroundMarkersForAllOwnUnits
		{
			get => _showGroundMarkersForAllOwnUnits;
			set => this.RaiseAndSetIfChanged(ref _showGroundMarkersForAllOwnUnits, value);
		}

		public int ScreenshotResolutionX
		{
			get => _screenshotResolutionX;
			set => this.RaiseAndSetIfChanged(ref _screenshotResolutionX, value);
		}

		public int ScreenshotResolutionY
		{
			get => _screenshotResolutionY;
			set => this.RaiseAndSetIfChanged(ref _screenshotResolutionY, value);
		}

		#region Commands

		public ReactiveCommand<string, Unit> SetPredefinedResolution { get; }

		#endregion

		public Em5SettingsViewModel()
		{
			#region Setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
						Apply();
					})
					.DisposeWith(disposables);
			});

			#endregion

			SetPredefinedResolution = ReactiveCommand.Create<string>(ExecuteSetPredefinedResolution);
			Load();
		}

		private void ExecuteSetPredefinedResolution(string resolution)
		{
			switch (resolution.ToLower())
			{
				case "4k":
					ScreenshotResolutionX = 2160;
					ScreenshotResolutionY = 3840;
					break;
				case "wqhd":
					ScreenshotResolutionX = 1440;
					ScreenshotResolutionY = 2560;
					break;
				case "fhd":
					ScreenshotResolutionX = 1080;
					ScreenshotResolutionY = 1920;
					break;
				default:
					// do nothing
					return;
			}
		}

		private void Apply()
		{
			try
			{
				SettingsEditor.GameSettings.ParameterGroups.Em5GameSettingsGroup.ShineThroughActive =
					IsShineThroughActive;
				SettingsEditor.GameSettings.ParameterGroups.Em5GameSettingsGroup
						.BuildingClippingActive =
					IsBuildingClippingActive;
				SettingsEditor.GameSettings.ParameterGroups.Em5GameSettingsGroup
						.ShowGroundMarkersForAllOwnUnits
					= ShowGroundMarkersForAllOwnUnits;
				SettingsEditor.GameSettings.ParameterGroups.Em5GameSettingsGroup
						.ScreenshotCapturingSize =
					string.Format("{0} {1}", ScreenshotResolutionX, ScreenshotResolutionY);
				SettingsEditor.SaveGameSettings();
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
		}

		private void Load()
		{
			try
			{
				IsShineThroughActive = SettingsEditor.GameSettings.ParameterGroups.Em5GameSettingsGroup
					.ShineThroughActive;
				IsBuildingClippingActive = SettingsEditor.GameSettings.ParameterGroups
					.Em5GameSettingsGroup
					.BuildingClippingActive;
				ShowGroundMarkersForAllOwnUnits = SettingsEditor.GameSettings.ParameterGroups
					.Em5GameSettingsGroup.ShowGroundMarkersForAllOwnUnits;
				LoadScreenshotRes();
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
		}

		private void LoadScreenshotRes()
		{
			var values = SettingsEditor.GameSettings.ParameterGroups.Em5GameSettingsGroup
				.ScreenshotCapturingSize.Split(" ");

			if (values.Length != 2)
			{
				Log.Error(new ArgumentException($"{values.Length} args given for screenshot res"));
				OnRequestMessageBox(new MessageBoxRequestEventArgs(
					Assets.Resources.ErrorMessageInvalidResolution,
					MessageBoxIconType.Exclamationmark));
			}

			ScreenshotResolutionX = Convert.ToInt32(values[0]);
			ScreenshotResolutionY = Convert.ToInt32(values[1]);
		}
	}
}