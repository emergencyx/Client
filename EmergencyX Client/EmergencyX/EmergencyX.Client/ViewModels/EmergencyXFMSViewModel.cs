using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;
using Avalonia.Collections;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Emergency4.Logic.Modifications;
using EmergencyX.FMS.Data;
using EmergencyX.FMS.Event;
using EmergencyX.FMS.Logic;
using EmergencyX.FMS.Utility;
using log4net;
using Newtonsoft.Json;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class EmergencyXFMSViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(EmergencyXFMSViewModel));

		private string _sessionName;
		private bool _sessionRunning;
		private string _sessionPassword;
		private string _sessionStatus;
		private string _sessionId;
		private string _sessionLink;
		private string _sessionConfig;
		private int _selectedModification;
		private AvaloniaList<Emergency4Modification> _availableModifications;
		private bool _isSessionLinkEnabled;
		private LogFileHandler _handler;


		public ViewModelActivator Activator { get; }

		public string SessionName
		{
			get => _sessionName;
			set
			{
				this.RaiseAndSetIfChanged(ref _sessionName, value);
				Log.Debug("Session name is " + _sessionName);
			}
		}

		public string SessionStatus
		{
			get => _sessionStatus;
			set
			{
				this.RaiseAndSetIfChanged(ref _sessionStatus, value);
				Log.Debug("Session status is " + _sessionStatus);
			}
		}

		public string SessionPassword
		{
			get => _sessionPassword;
			set => this.RaiseAndSetIfChanged(ref _sessionPassword, value);
		}

		public string SessionId
		{
			get => _sessionId;
			set
			{
				this.RaiseAndSetIfChanged(ref _sessionId, value);
				if (SessionStatus == "OK")
				{
					var url = Settings.AppSettings.FmsServer;
					if (url.EndsWith("/"))
					{
						SessionLink = String.Format("{0}show/{1}", url, SessionId);
					}
					else
					{
						SessionLink = String.Format("{0}/show/{1}", url, SessionId);
					}
				}

				Log.Debug("SessionId is " + _sessionId);
			}
		}

		public string SessionLink
		{
			get => _sessionLink;
			set
			{
				this.RaiseAndSetIfChanged(ref _sessionLink, value);
				if (_sessionLink != null)
				{
					IsSessionLinkEnabled = true;
				}
				else
				{
					IsSessionLinkEnabled = false;
				}

				Log.Debug("Session link is " + _sessionLink);
			}
		}

		public bool IsSessionLinkEnabled
		{
			get => _isSessionLinkEnabled;
			set
			{
				this.RaiseAndSetIfChanged(ref _isSessionLinkEnabled, value);
				Log.Debug("IsSessionLinkEnabled is " + _isSessionLinkEnabled);
			}
		}

		public LogFileHandler Handler
		{
			get => _handler;
			set => this.RaiseAndSetIfChanged(ref _handler, value);
		}

		public string SessionConfig
		{
			get => _sessionConfig;
			set => this.RaiseAndSetIfChanged(ref _sessionConfig, value);
		}

		public int SelectedModification
		{
			get => _selectedModification;
			set => this.RaiseAndSetIfChanged(ref _selectedModification, value);
		}

		public string GetGameObjectConfiguration =>
			Path.GetFullPath(
				FmsUtility.FindConfigurationFile(AvailableModifications[SelectedModification].DataPath,
					_sessionConfig));

		public AvaloniaList<Emergency4Modification> AvailableModifications
		{
			get => _availableModifications;
			set => this.RaiseAndSetIfChanged(ref _availableModifications, value);
		}

		#region Commands

		public ReactiveCommand<Unit, Unit> ActivateFmsCommand { get; }

		public ReactiveCommand<Unit, Unit> StopFmsCommand { get; }

		public ReactiveCommand<Unit, Unit> OpenFmsLinkCommand { get; }

		public bool SessionRunning
		{
			get => _sessionRunning;
			set => this.RaiseAndSetIfChanged(ref _sessionRunning, value);
		}

		#endregion

		public EmergencyXFMSViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated(disposables =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */

						if (SessionRunning)
						{
							SessionEnd();
						}
						
					})
					.DisposeWith(disposables);
			});

			#endregion

			ActivateFmsCommand = ReactiveCommand.Create(ExecuteActivateFmsCommand);
			StopFmsCommand = ReactiveCommand.Create(ExecuteStopFmsCommand);
			OpenFmsLinkCommand = ReactiveCommand.Create(ExecuteOpenFmsLinkCommand);
			SessionRunning = false;

			ReplaceDefaultSettingsWithInstallSpecificOnes();
			Load();
		}

		private void ExecuteOpenFmsLinkCommand()
		{
			try
			{
				if (!Platform.IsWindows)
				{
					throw new NotSupportedException();
				}

				var url = SessionLink;
				Process.Start(new ProcessStartInfo("cmd", $"/c start {url}")
					{ CreateNoWindow = true });
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
		}

		public EmergencyXFMSViewModel(Emergency4Modification modificationToStartFmsFor) : this()
		{
			var index = -1;

			if (AvailableModifications.Count(x => x.DataPath == modificationToStartFmsFor.DataPath) == 1)
			{
				index = AvailableModifications.IndexOf(
					AvailableModifications.Single(x =>
						x.DataPath == modificationToStartFmsFor.DataPath));
			}

			if (index == -1)
			{
				return;
			}

			SelectedModification = index;
			SessionStartAsync();
		}

		private void ExecuteStopFmsCommand()
		{
			SessionEnd();
		}

		private void ExecuteActivateFmsCommand()
		{
			SessionStartAsync(true);
		}

		private void Load()
		{
			AvailableModifications = new AvaloniaList<Emergency4Modification>();

			var modsDirectory = Emergency4GameLoader.GetEmergency4ModsPath;
			foreach (var modification in Emergency4ModificationService.FromModsDirectory(modsDirectory))
			{
				if (modification is not Emergency4Modification)
				{
					Log.Error("Found Emergency 4 mod which is not a Emergency 4 mod..");
					continue;
				}

				AvailableModifications.Add(modification as Emergency4Modification);
			}

			SelectedModification = AvailableModifications.Count > 0 ? 0 : -1;
		}

		private void ReplaceDefaultSettingsWithInstallSpecificOnes()
		{
			if (string.IsNullOrEmpty(Settings.AppSettings.FmsServer))
			{
				Settings.AppSettings.FmsServer = DefaultSettings.DefaultFmsServer;
			}

			SessionConfig = DefaultSettings.DefaultFmsGameObjectConfig;
		}

		/// <summary>
		/// Wird aufgerufen, wenn auf den Session starten Button geklickt wird (von dem Session Start Command)
		/// </summary>
		public async void SessionStartAsync(bool isManualStart = false)
		{
			Log.Info("Try session start");

			// only do this check if the fms is started automatically
			if (!isManualStart)
			{
				// check if game is not yet running (would be bad cuz then the log file is still containing old entries)
				int wait = 0;
				do
				{
					await Task.Run(() => Thread.Sleep(10_000));
					wait += 10_000;

					// only wait for 2 minutes which is when wait == 120_000
					if (wait > 120_000)
					{
						OnRequestMessageBox(new MessageBoxRequestEventArgs(Assets.Resources
							.DialogMessageEmergenc4DidNotStartInTime));
						return;
					}
				} while (!Emergency4GameLoader.IsEmergency4Running());
			}

#if !DEBUG
			if (!Emergency4GameLoader.GetEmergency4LogFile.Exists)
			{
				var message =
					String.Format(
						"Error: File {0} not found. Please make sure the path for {1} was correctly configured in the EmergencyX FMS Client.exe.config.",
						Emergency4GameLoader.GetEmergency4LogFile.FullName, "EM4LogFile");
				Log.Error(message);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(message,
					MessageBoxIconType.Exclamationmark));
				return;
			}

			if (!File.Exists(GetGameObjectConfiguration))
			{
				var message =
					String.Format(
						"Error: File {0} not found. Please make sure the path for {1} was correctly configured in the EmergencyX FMS Client.exe.config.",
						GetGameObjectConfiguration,
						"Game Object Configuration");
				Log.Error(message);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(message,
					MessageBoxIconType.Exclamationmark));
				return;
			}

#endif
			try
			{
				var data = new SessionStartData();

				if (string.IsNullOrEmpty(SessionName))
				{
					SessionName = DateTime.UtcNow.ToString();
				}

				data.Name = SessionName;
				data.Password = SessionPassword;
				data.Objects =
					JsonConvert.DeserializeObject<object>(
						File.ReadAllText(GetGameObjectConfiguration));

				Log.Info(data);

				SessionHandler.BaseUrl = Settings.AppSettings.FmsServer;
				var response = SessionHandler.SessionStart(JsonConvert.SerializeObject(data));

				Log.Info(response);

				SessionStatus = response.Status;
				if (SessionStatus == "OK")
				{
					SessionId = response.SessionId;
				}
				else
				{
					return;
				}

				ExecuteOpenFmsLinkCommand();
			}
			catch (Exception e)
			{
				Log.Error("Error in SessionStartAsync()", e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}

			try
			{
				// Session Update Interval
#if !DEBUG
				Handler = new LogFileHandler(Emergency4GameLoader.GetEmergency4LogFile.FullName);
#else
				Handler = new LogFileHandler(
					@"D:\Program Files (x86)\Steam\steamapps\common\EMERGENCY 4 Deluxe\logfile.txt");
#endif
				Handler.StartTimer();
				Handler.OnNewLogMessages += RunSessionUpdater;
				SessionRunning = true;
			}
			catch (Exception e)
			{
				Log.Error("Error on Handler", e);
			}
		}

		public void SessionEnd()
		{
			// turn of log reading
			Handler.Dispose();
			Handler.OnNewLogMessages -= RunSessionUpdater;
			Handler = null;

			// remove data from view state
			SessionId = null;
			SessionLink = null;
			SessionName = null;
			SessionStatus = null;
			SessionRunning = false;
		}

		/// <summary>
		/// Event Handler für das Event, welches ausgelöst wird, wenn neue relevante Log-Einträge gefunden wurden
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void RunSessionUpdater(object sender, ReportLogMessageEventArgs e)
		{
			Log.Info("Run Session Updater");

			// Relevante Daten für das Update der Session beim Server
			var sessionUpdateData = new SessionUpdateData();
			sessionUpdateData.SessionId = SessionId;
			var objects = e.UpdatedItems.ToList();

			// Kein Update nötig?
			if (objects.Count == 0)
			{
				Log.Info("Zero items to update");
				return;
			}

			// Der Server aktzeptiert nicht mehr als 100 Updates zur selben Zeit => Beschränkung auf 999 Stk.
			var requestData = new List<GameObject>();
			int counter = 0;

			Log.Info(String.Format("Updater item count is {0}", objects.Count.ToString()));

			for (int i = 0; i < objects.Count; i++)
			{
				requestData.Add(objects[i]);
				if ((i % 999 == 0 && i != 0) || i == objects.Count - 1)
				{
					try
					{
						sessionUpdateData.Objects = requestData;
						Log.Info(sessionUpdateData);
						SessionHandler.SessionUpdate(
							JsonConvert.SerializeObject(sessionUpdateData));
						requestData = new List<GameObject>();
					}
					catch (Exception er)
					{
						Log.Error(er);
					}

					counter++;
				}
			}
		}
	}
}