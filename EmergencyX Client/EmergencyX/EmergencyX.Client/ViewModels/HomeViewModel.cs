using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using EmergencyX.Client.Data.Games;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Logic.Games;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Emergency5.Logic;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class HomeViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(HomeViewModel));

		private ObservableCollection<InstalledGame> _games;
		private InstalledGame _emergency5;
		private InstalledGame _emergency4;
		private bool _isEm5Installed;
		private bool _isEm4Installed;

		public ViewModelActivator Activator { get; }

		private ObservableCollection<InstalledGame> Games
		{
			get => _games;
			set
			{
				this.RaiseAndSetIfChanged(ref _games, value);
				if (Games.Count(x => x.Name == "Emergency 5") == 1)
				{
					Emergency5 = Games.Single(x => x.Name == "Emergency 5");
					IsEm5Installed = true;
				}
				else
				{
					Emergency5 = new InstalledGame();
				}

				if (Games.Count(x => x.Name == "Emergency 4") == 1)
				{
					Emergency4 = Games.Single(x => x.Name == "Emergency 4");
					IsEm4Installed = true;
				}
				else
				{
					Emergency4 = new InstalledGame();
				}
			}
		}

		public InstalledGame Emergency5
		{
			get => _emergency5;
			set => this.RaiseAndSetIfChanged(ref _emergency5, value);
		}

		public InstalledGame Emergency4
		{
			get => _emergency4;
			set => this.RaiseAndSetIfChanged(ref _emergency4, value);
		}

		public bool IsEm5Installed
		{
			get => _isEm5Installed;
			set => this.RaiseAndSetIfChanged(ref _isEm5Installed, value);
		}

		public bool IsEm4Installed
		{
			get => _isEm4Installed;
			set => this.RaiseAndSetIfChanged(ref _isEm4Installed, value);
		}


		#region Commands

		public ReactiveCommand<string, Unit> RunGame { get; }

		public ReactiveCommand<string, Unit> RunEditor { get; }

		public ReactiveCommand<string, Unit> OpenGameDirectory { get; }

		public ReactiveCommand<string, Unit> OpenPatchSiteCommand { get; }

		public ReactiveCommand<Unit, Unit> OpenProjectSiteCommand { get; }

		// currently disabled due to EM Forum shutdown
		// public ReactiveCommand<Unit, Unit> OpenFanForumCommand { get; }

		#endregion

		public HomeViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			IsEm5Installed = false;
			IsEm4Installed = false;

			Games = InstalledGames.GetInstalledGamesForHome();

			#region setCommands

			RunGame = ReactiveCommand.Create<string>(ExecuteRunGame);

			RunEditor = ReactiveCommand.Create<string>(ExecuteRunEditor);

			OpenGameDirectory = ReactiveCommand.Create<string>(ExecuteOpenGameDirectory);

			OpenPatchSiteCommand = ReactiveCommand.Create<string>(ExceuteOpenPatchSiteCommand);

			OpenProjectSiteCommand = ReactiveCommand.Create(ExecuteOpenProjectSiteCommand);
			
			// currently disabled due to EM Forum shutdown
			// OpenFanForumCommand = ReactiveCommand.Create(ExecuteOpenFanForumCommand);

			#endregion

			// little test function to make sure the window didn't break
#if DEBUG
			Task.Run(SimulateError);
#endif
		}

		// currently disabled due to EM Forum shutdown
		// private void ExecuteOpenFanForumCommand()
		// {
		// 	if (!Platform.IsWindows)
		// 	{
		// 		throw new NotSupportedException();
		// 	}
		//
		// 	try
		// 	{
		// 		var url = @"https://www.emergency-forum.de/";
		// 		Process.Start(new ProcessStartInfo("cmd", $"/c start {url}")
		// 			{ CreateNoWindow = true });
		// 	}
		// 	catch (Exception e)
		// 	{
		// 		Log.Error(e);
		// 		OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
		// 			MessageBoxIconType.Exclamationmark));
		// 	}
		// }

		private void ExecuteOpenProjectSiteCommand()
		{
			if (!Platform.IsWindows)
			{
				throw new NotSupportedException();
			}

			try
			{
				var url = @"https://www.emergencyx.de/";
				Process.Start(new ProcessStartInfo("cmd", $"/c start {url}")
					{ CreateNoWindow = true });
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
		}

		private void ExceuteOpenPatchSiteCommand(string controlName)
		{
			if (!Platform.IsWindows)
			{
				throw new NotSupportedException();
			}

			// open the patch dl site for Em5
			if (controlName.ToLower().Contains("em5"))
			{
				try
				{
					var url = @"https://www.world-of-emergency.com/patch";
					Process.Start(new ProcessStartInfo("cmd", $"/c start {url}")
						{ CreateNoWindow = true });
				}
				catch (Exception e)
				{
					Log.Error(e);
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
				}
			}

			// open the patch dl site for em4
			if (controlName.ToLower().Contains("em4"))
			{
				try
				{
					var url =
						@"https://www.emergency-forum.de/filebase/index.php?filebase/433-patches-demos-und-videos/";
					Process.Start(new ProcessStartInfo("cmd", $"/c start {url}")
						{ CreateNoWindow = true });
				}
				catch (Exception e)
				{
					Log.Error(e);
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
				}
			}
		}

		private void ExecuteOpenGameDirectory(string controlName)
		{
			// open the EM5 game dir
			if (controlName.ToLower().Contains("em5"))
			{
				try
				{
					GamePathLoader.OpenEmergency5InstallDir();
				}
				catch (Exception e)
				{
					Log.Error(e);
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
				}
			}

			// open the EM4 game dir
			if (controlName.ToLower().Contains("em4"))
			{
				try
				{
					Emergency4GameLoader.OpenEmergency4InstallDir();
				}
				catch (Exception e)
				{
					Log.Error(e);
					OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
						MessageBoxIconType.Exclamationmark));
				}
			}
		}

		private void ExecuteRunEditor(string controlName)
		{
			try
			{
				// run Em5 editor
				if (controlName.ToLower().Contains("em5"))
				{
					GameRunner.RunEmergency5Editor();
				}

				// run Em4 editor
				if (controlName.ToLower().Contains("em4"))
				{
					// there is trouble running the editor with a Steam game, so... no
					if (Emergency4GameLoader.IsUsingSteamInstallation)
					{
						throw new NotSupportedException(Assets.Resources
							.ExceptionEditorNotSupportedWithSteam);
					}

					Emergency4GameLoader.RunEmergency4Editor();
				}
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
		}

		private void ExecuteRunGame(string controlName)
		{
			try
			{
				// Emergency 5
				if (controlName.ToLower().Contains("em4"))
				{
					Emergency4GameLoader.RunEmergency4();
				}

				// Emergency 5
				if (controlName.ToLower().Contains("em5"))
				{
					GameRunner.RunEmergency5(Settings.AppSettings.SkipLauncher);
				}
			}
			catch (Exception e)
			{
				Log.Error(e);
				OnRequestMessageBox(new MessageBoxRequestEventArgs(e.Message,
					MessageBoxIconType.Exclamationmark));
			}
		}

		private void SimulateError()
		{
			Thread.Sleep(5000);
			OnRequestMessageBox(new MessageBoxRequestEventArgs(
				"Dies ist eine sehr sehr sehr sehr lange Fehlermeldung die jede Vorstellungskrapft übersteigt. Hoffentlich wird es zu keinen Fehlern beim anzeigen dieser Meldun gkommen. Beispielsweiße könnte die Fehlermeldung den Rahmen des Fensters sprengen!!!!!!!!!!!!!",
				MessageBoxIconType.Exclamationmark));
		}
	}
}