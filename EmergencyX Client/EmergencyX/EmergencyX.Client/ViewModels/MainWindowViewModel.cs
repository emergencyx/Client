﻿using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Collections;
using Avalonia.Threading;
using EmergencyX.Client.Data.Menu;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Logic.NamedPipeMessages;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Events;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.NamedPipeConnection;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
    public class MainWindowViewModel : ViewModelBase, IActivatableViewModel
    {
        public static readonly ILog Log =
            LogManager.GetLogger(typeof(MainWindowViewModel));

        private object _currentView;
        private bool _disableMenu;
        private bool _showMessageBox;
        private bool _isErrorIcon;
        private string _dialogMessage;
        private MenuViewModel _menuViewModel;
        private CommunicationServer _namedPipeServer;

        public ViewModelActivator Activator { get; }

        public object CurrentView
        {
            get => _currentView;
            set => this.RaiseAndSetIfChanged(ref _currentView, value);
        }

        public MenuViewModel MenuViewModel
        {
            get => _menuViewModel;
            set => this.RaiseAndSetIfChanged(ref _menuViewModel, value);
        }

        public bool DisableMenu
        {
            get => _disableMenu;
            set => this.RaiseAndSetIfChanged(ref _disableMenu, value);
        }

        public bool ShowMessageBox
        {
            get => _showMessageBox;
            set
            {
                this.RaiseAndSetIfChanged(ref _showMessageBox, value);
                this.RaisePropertyChanged("IsDialog");
                if (_showMessageBox)
                {
                    OnEnableDisableEvent(this, new EnableDisableMenuEventArgs(true));
                }
                else
                {
                    OnEnableDisableEvent(this, new EnableDisableMenuEventArgs(false));
                }
            }
        }

        public bool IsDialog
        {
            get => ShowMessageBox;
        }

        public bool IsErrorIcon
        {
            get => _isErrorIcon;
            set => this.RaiseAndSetIfChanged(ref _isErrorIcon, value);
        }

        public string DialogMessage
        {
            get => _dialogMessage;
            set => this.RaiseAndSetIfChanged(ref _dialogMessage, value);
        }

        public CommunicationServer NamedPipeServer
        {
            get => _namedPipeServer;
            set => this.RaiseAndSetIfChanged(ref _namedPipeServer, value);
        }

        public ReactiveCommand<Unit, Unit> DialogSkipCommand { get; }
        public ReactiveCommand<Unit, Unit> OkCommand { get; }

        public ReactiveCommand<Unit, Unit> CopyCommand { get; }

        public MainWindowViewModel()
        {
            #region Setup

            Activator = new ViewModelActivator();
            this.WhenActivated((CompositeDisposable disposables) =>
            {
                /* handle activation */
                Disposable
                    .Create(() =>
                    {
                        /* handle deactivation */
                    })
                    .DisposeWith(disposables);
            });

            #endregion

            RefreshMenu();

            // Load favourite view if set otherwise load home
            var itemToLoad = Settings.AppSettings.FavouriteMenuItem;
            itemToLoad = string.IsNullOrEmpty(itemToLoad)
                ? string.Format("{0}.{1}", Assets.Resources.MenuItemNameEmergencyXClient,
                    Assets.Resources.SubMenuItemHome)
                : itemToLoad;

            MenuViewModel.InvokeOpenMenuCommand(itemToLoad);

            DialogSkipCommand = ReactiveCommand.Create(ExecuteDialogSkipCommand);
            OkCommand = ReactiveCommand.Create(ExecuteOkCommand);
            CopyCommand = ReactiveCommand.Create(ExecuteCopyCommand);
            NamedPipeServer = new CommunicationServer(NamedPipeSettings.MAIN_PIPE_NAME);
            NamedPipeServer.MessageReceivedHandler += OnMessageReceivedHandler;
            Task.Run(() => NamedPipeServer.RunClientAsync());
        }

        private void OnMessageReceivedHandler(object sender, CommunicationServerMessageReceivedEvent e)
        {
            // first general validation
            Log.Debug("Received message.");
            var type = NamedPipeMessageHandler.Handle(e);
            if (type != NamedPipeActionType.DownloadAction)
            {
                return;
            }

            // handle the individual action type
            var result = NamedPipeMessageHandler.HandleDownloadAction(e);
            if (result == null)
            {
                return;
            }

            var itemName = string.Format("{0}.{1}", Assets.Resources.MenuItemNameEmergencyXClient,
                Assets.Resources.SubMenuItemModStore);
            var payload = new OpenViewRequestPayload(result, result.GetType());
            Dispatcher.UIThread.Post(() =>
                OnOpenViewFromViewEvent(this, new OpenViewFromViewEventArgs(itemName, payload)));
        }

        private void ExecuteCopyCommand()
        {
            if (string.IsNullOrEmpty(DialogMessage))
            {
                return;
            }

            if (App.GetMainWindow.Clipboard != null)
            {
                App.GetMainWindow.Clipboard.SetTextAsync(DialogMessage);
            }
        }

        private void ExecuteDialogSkipCommand()
        {
            DialogMessage = String.Empty;
        }

        private void ExecuteOkCommand()
        {
            ShowMessageBox = false;
            DialogMessage = String.Empty;
            IsErrorIcon = false;
        }

        private void OnMenuSelectionChangedEvent(object sender, MenuSelectionChangedEventArgs e)
        {
            if (CurrentView != null)
            {
                (CurrentView as ViewModelBase).EnableDisableMenuEventHandler -= OnEnableDisableEvent;
                (CurrentView as ViewModelBase).OpenViewFromViewEventHandler -= OnOpenViewFromViewEvent;
                (CurrentView as ViewModelBase).RefreshMenuRequestEventHandler -= OnRefreshMenuEvent;
                (CurrentView as ViewModelBase).MessageBoxRequestEventHandler -= OnMessageBoxRequest;
            }

            var item = e.Payload == null
                ? System.Activator.CreateInstance(e.SelectedView)
                : System.Activator.CreateInstance(e.SelectedView, e.Payload.Obj);

            // only views are allowed
            if (!(item is ViewModelBase))
            {
                throw new InvalidOperationException();
            }

            CurrentView = null;
            (item as ViewModelBase).EnableDisableMenuEventHandler += OnEnableDisableEvent;
            (item as ViewModelBase).OpenViewFromViewEventHandler += OnOpenViewFromViewEvent;
            (item as ViewModelBase).RefreshMenuRequestEventHandler += OnRefreshMenuEvent;
            (item as ViewModelBase).MessageBoxRequestEventHandler += OnMessageBoxRequest;
            CurrentView = item;
        }

        private void OnMessageBoxRequest(object? sender, MessageBoxRequestEventArgs e)
        {
            ShowMessageBox = true;
            DialogMessage = e.Message;

            if (e.MessageBoxIconType == MessageBoxIconType.Exclamationmark)
            {
                IsErrorIcon = true;
            }
        }

        private void OnOpenViewFromViewEvent(object? sender, OpenViewFromViewEventArgs e)
        {
            if (e.Payload == null)
            {
                MenuViewModel.InvokeOpenMenuCommand(e.MenuItemName);
                return;
            }

            MenuViewModel.InvokeOpenMenuCommand(e.MenuItemName, e.Payload);
        }

        private void OnEnableDisableEvent(object sender, EnableDisableMenuEventArgs e)
        {
            if (!(sender is ViewModelBase) || (sender is MenuViewModel))
            {
                Log.Error("DisableEnable incorrectly invoked");
                Log.Info(sender.GetType());
                return;
            }

            DisableMenu = e.DisableMenu;
        }

        private void OnRefreshMenuEvent(object sender, EventArgs e)
        {
            RefreshMenu();
            MenuViewModel.InvokeOpenMenuCommand(string.Join('.',
                new AvaloniaList<string>()
                {
                    Assets.Resources.MenuItemNameEmergencyXClient,
                    Assets.Resources.SubMenuItemNameSettings
                }));
        }

        private void RefreshMenu()
        {
            MenuViewModel = new MenuViewModel();
            MenuViewModel.MenuSelectionChangedEvent += OnMenuSelectionChangedEvent;
        }
    }
}