using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avalonia.Controls;
using DynamicData.Binding;
using EmergencyX.Client.Data.Menu;
using EmergencyX.Client.Events;
using EmergencyX.Client.Logic.Menu;
using EmergencyX.Core.Logic.Settings;
using ReactiveUI;
using MenuItem = EmergencyX.Client.Data.Menu.MenuItem;

namespace EmergencyX.Client.ViewModels
{
	public class MenuViewModel : ViewModelBase, IActivatableViewModel
	{
		private ObservableCollection<MenuItem> _menuItemList;
		public ViewModelActivator Activator { get; }

		public ObservableCollection<MenuItem> MenuItemList
		{
			get => _menuItemList;
			set => this.RaiseAndSetIfChanged(ref _menuItemList, value);
		}

		#region Commands

		public ReactiveCommand<string, Unit> OpenMenuItemView { get; }

		public ReactiveCommand<string, Unit> SetMenuItemAsFavourite { get; }

		#endregion

		public EventHandler<MenuSelectionChangedEventArgs> MenuSelectionChangedEvent;

		public MenuViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				Settings.AppSettings.WhenValueChanged(x => x.ShowFavStars)
					.ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(InvokeNotifyGetStarsAreVisibleChanged);
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			MenuItemList = new ObservableCollection<MenuItem>();
			Load();
			OpenMenuItemView = ReactiveCommand.Create<string>(ExecuteOpenItemViewCommand);
			SetMenuItemAsFavourite = ReactiveCommand.Create<string>(ExecuteSetMenuItemAsFavourite);
			Settings.AppSettings.ShowFavStars = false;
		}

		private void Load()
		{
			var factory = new MenuItemFactory();
			MenuItemList.Clear();
			MenuItemList = factory.GetMenuItems();
		}

		private void ExecuteOpenItemViewCommand(string menuItemName)
		{
			var args = new MenuSelectionChangedEventArgs();
			var mainMenuItem = menuItemName.Split('.')[0];
			var subMenuItem = menuItemName.Split('.')[1];

			// handle not available items
			if (MenuItemList.All(x => x.Name != mainMenuItem))
			{
				mainMenuItem = Assets.Resources.MenuItemNameEmergencyXClient;
				subMenuItem = Assets.Resources.SubMenuItemHome;
			}
			else
			{
				if (MenuItemList.Single(x => x.Name == mainMenuItem).SubItems
				    .All(x => x.Name != subMenuItem))
				{
					mainMenuItem = Assets.Resources.MenuItemNameEmergencyXClient;
					subMenuItem = Assets.Resources.SubMenuItemHome;
				}
			}

			var selection = MenuItemList.Single(x => x.Name == mainMenuItem).SubItems
				.Single(x => x.Name == subMenuItem);

			args.SelectedView = selection.View;
			OnMenuItemChanged(args);

			// ToDo: Check at some point if Avalonia supports XAML conditions
			foreach (var menuItem in MenuItemList)
			{
				menuItem.SubItems.Where(x => x.IsActive).ToList().ForEach(x => x.IsActive = false);
			}

			selection.IsActive = !selection.IsActive;
		}

		private void ExecuteOpenItemViewCommand(string menuItemName, OpenViewRequestPayload payload)
		{
			var args = new MenuSelectionChangedEventArgs();
			var mainMenuItem = menuItemName.Split('.')[0];
			var subMenuItem = menuItemName.Split('.')[1];

			var selection = MenuItemList.Single(x => x.Name == mainMenuItem).SubItems
				.Single(x => x.Name == subMenuItem);

			args.SelectedView = selection.View;
			args.Payload = payload;
			OnMenuItemChanged(args);

			// ToDo: Check at some point if Avalonia supports XAML conditions
			foreach (var menuItem in MenuItemList)
			{
				menuItem.SubItems.Where(x => x.IsActive).ToList().ForEach(x => x.IsActive = false);
			}

			selection.IsActive = !selection.IsActive;
		}

		private void ExecuteSetMenuItemAsFavourite(string menuItemName)
		{
			// is it already set -- then remove it
			if (menuItemName == Settings.AppSettings.FavouriteMenuItem)
			{
				Settings.AppSettings.FavouriteMenuItem = String.Empty;
				Settings.AppSettings.Save();
				MenuItemList.Single(x => x.Name == menuItemName.Split('.')[0]).SubItems
					.Single(x => x.Name == menuItemName.Split('.')[1]).IsFavourite = false;
				return;
			}

			Settings.AppSettings.FavouriteMenuItem = menuItemName;
			Settings.AppSettings.Save();

			// first unset any favs
			// ToDo: Check at some point if Avalonia supports XAML conditions
			foreach (var menuItem in MenuItemList)
			{
				menuItem.SubItems.Where(x => x.IsFavourite).ToList()
					.ForEach(x => x.IsFavourite = false);
			}

			// get info to find the correct item
			var mainMenuItem = menuItemName.Split('.')[0];
			var subMenuItem = menuItemName.Split('.')[1];

			// set as new favourite
			MenuItemList.Single(x => x.Name == mainMenuItem).SubItems
				.Single(x => x.Name == subMenuItem).IsFavourite = true;
		}

		protected virtual void OnMenuItemChanged(MenuSelectionChangedEventArgs e)
		{
			MenuSelectionChangedEvent?.Invoke(this, e);
		}

		public void InvokeOpenMenuCommand(string menuItemTechnicalName)
		{
			ExecuteOpenItemViewCommand(menuItemTechnicalName);
		}

		public void InvokeOpenMenuCommand(string menuItemTechnicalName, OpenViewRequestPayload payload)
		{
			ExecuteOpenItemViewCommand(menuItemTechnicalName, payload);
		}

		private void InvokeNotifyGetStarsAreVisibleChanged(bool value)
		{
			foreach (var menuItem in MenuItemList)
			{
				foreach (var menuItemSubItem in menuItem.SubItems)
				{
					menuItemSubItem.IsStarVisible = value;
				}
			}
		}
	}
}