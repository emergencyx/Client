using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Avalonia.Collections;
using Avalonia.Input;
using Avalonia.Threading;
using EmergencyX.Client.Utility.Views;
using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Emergency4.Logic.Modifications;
using EmergencyX.Emergency5.Logic;
using EmergencyX.Emergency5.Logic.Modifications;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class ModStoreDetailViewModel : ViewModelBase, IActivatableViewModel
	{
		private static readonly ILog Log =
			LogManager.GetLogger(typeof(ModStoreDetailViewModel));

		private ProjectListItem _modification;
		private IGameModification _relatedLocalModification;
		private AvaloniaList<string> _versions;
		private int _selectedVersionIndex;
		private bool _isMarkedForDownload;
		private bool _isAlreadyInstalled;
		private bool _isOutdated;
		private bool _versionsAlreadyRequested;
		private string _currentVersion;

		public ProjectListItem Modification
		{
			get => _modification;
			set => this.RaiseAndSetIfChanged(ref _modification, value);
		}

		public bool IsMarkedForDownload
		{
			get => _isMarkedForDownload;
			set => this.RaiseAndSetIfChanged(ref _isMarkedForDownload, value);
		}

		public bool IsAlreadyInstalled
		{
			get => _isAlreadyInstalled;
			set
			{
				this.RaiseAndSetIfChanged(ref _isAlreadyInstalled, value);
				this.RaisePropertyChanged("GetIsAlreadyInstalledOrIsOutdated");
			}
		}

		public bool IsOutdated
		{
			get => _isOutdated;
			set
			{
				this.RaiseAndSetIfChanged(ref _isOutdated, value);
				this.RaisePropertyChanged("GetIsAlreadyInstalledOrIsOutdated");
			}
		}

		public AvaloniaList<string> Versions
		{
			get => _versions;
			set
			{
				this.RaiseAndSetIfChanged(ref _versions, value);
				this.RaisePropertyChanged("GetIsAlreadyInstalledOrIsOutdated");
			}
		}

		public bool VersionsAlreadyRequested
		{
			get => _versionsAlreadyRequested;
			set => this.RaiseAndSetIfChanged(ref _versionsAlreadyRequested, value);
		}

		public int SelectedVersionIndex
		{
			get => _selectedVersionIndex;
			set
			{
				if (value == -1)
				{
					// never select nothing
					value = 0;
				}

				this.RaiseAndSetIfChanged(ref _selectedVersionIndex, value);
			}
		}

		public string CurrentVersion
		{
			get => _currentVersion;
			set => this.RaiseAndSetIfChanged(ref _currentVersion, value);
		}

		public bool GetIsAlreadyInstalledOrIsOutdated => IsOutdated || IsAlreadyInstalled;

		#region Commands

		public ReactiveCommand<PointerEventArgs, Unit> LoadAllVersions { get; }

		#endregion

		public ViewModelActivator Activator { get; }

		private IGameModification RelatedLocalModification
		{
			get => !IsAlreadyInstalled ? null : _relatedLocalModification;
			set => this.RaiseAndSetIfChanged(ref _relatedLocalModification, value);
		}

		private ModStoreDetailViewModel()
		{
			#region setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				/* handle activation */
				Disposable
					.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			IsAlreadyInstalled = false;
			IsOutdated = false;
			VersionsAlreadyRequested = false;
			RelatedLocalModification = null;

			#region setCommands

			LoadAllVersions = ReactiveCommand.Create<PointerEventArgs>(ExecuteLoadAllVersionsAsync);

			#endregion
		}

		public ModStoreDetailViewModel(ProjectListItem item) : this()
		{
			Modification = item;
			Versions = new AvaloniaList<string> { Modification.LatestVersion };
			SelectedVersionIndex = 0;
			CheckIfIsAlreadyInstalledAsync(item.Game);
			LoadCurrentVersion();
		}

		private async void ExecuteLoadAllVersionsAsync(PointerEventArgs obj)
		{
			if (VersionsAlreadyRequested)
			{
				return;
			}

			Project result;
			try
			{
				// wait for versions
				result = await Task.Run(() =>
					ModStoreLoader.ModStoreContent.GetProjectById(Modification.Id));
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Log.Error(e);
				result = null;
			}

			VersionsAlreadyRequested = true;

			if (result == null)
			{
				return;
			}

			var temp = new AvaloniaList<string>();

			foreach (var release in result.Releases)
			{
				temp.Add(release.Version);
			}

			Versions = new AvaloniaList<string>(temp.OrderByDescending(x => x.Substring(0)));
			SelectedVersionIndex = 0;
		}

		private void CheckIfIsAlreadyInstalledAsync(PhonebookGames category)
		{
			switch (Modification.Game)
			{
				case PhonebookGames.EM4:

					IsEm4ModAlreadyInstalledAsync();

					break;

				case PhonebookGames.EM5:

					IsEm5ModAlreadyInstalled();

					break;
				case PhonebookGames.EMONE:
				default:
					throw new NotImplementedException();
			}

			if (IsAlreadyInstalled)
			{
				CheckIfIsOutdated();
			}
		}

		public void IsEm4ModAlreadyInstalledAsync()
		{
			if (string.IsNullOrEmpty(Emergency4GameLoader
				    .LoadEmergencyInstallationPath()) ||
			    !Directory.Exists(Emergency4GameLoader.GetEmergency4ModsPath.FullName))
			{
				// no check when em4 can not be found
				return;
			}

			var modAndReleaseRequest = GetEmergency4ModAndReleaseFromE4InfoHashAsync();

			// if its null, then there was no result when checking E4info hashs and it is not yet installed
			IsAlreadyInstalled = modAndReleaseRequest.Item1 != null;
			if (IsAlreadyInstalled)
			{
				RelatedLocalModification = modAndReleaseRequest.Item1;
			}
		}

		public void IsEm5ModAlreadyInstalled()
		{
			if (string.IsNullOrEmpty(GamePathLoader.LoadEmergencyInstallationPath()) ||
			    !Directory.Exists(GamePathLoader.LoadEmergencyInstallationPath()))
			{
				// no check if Emergency 5 can not be found
				return;
			}


			IsAlreadyInstalled = false;
			foreach (var release in ModStoreLoader.ModStoreContent
				         .GetProjectById(Modification.Id).Releases)
			{
				if (ModificationService.LoadModList().Count(x =>
					    x.GetProjectFileHash() == release.Hash) == 1)
				{
					IsAlreadyInstalled = true;
					RelatedLocalModification = ModificationService.LoadModList()
						.First(x => x.GetProjectFileHash() == release.Hash);
				}
			}
		}

		private void CheckIfIsOutdated()
		{
			switch (Modification.Game)
			{
				case PhonebookGames.EM4:
					CheckIfEm4ModIsOutdatedAsync();
					break;
				case PhonebookGames.EM5:
					CheckIfEm5ModIsOutdated();
					break;
				case PhonebookGames.EMONE:
				default:
					throw new NotSupportedException();
			}
		}

		private void CheckIfEm5ModIsOutdated()
		{
			if (string.IsNullOrEmpty(GamePathLoader.LoadEmergencyInstallationPath()) ||
			    !Directory.Exists(GamePathLoader.LoadEmergencyInstallationPath()))
			{
				// do not do anything if nothing is there
				return;
			}

			if (!IsAlreadyInstalled)
			{
				return;
			}

			var isOutdated = Tools.IsModOutdated(RelatedLocalModification,
				ModificationService.LoadModList().First(x => x.Name == RelatedLocalModification.Name)
					.EmergencyProject.Properties.Version);
			Dispatcher.UIThread.Post(() => { IsOutdated = isOutdated; }, DispatcherPriority.Send);
		}

		private void LoadCurrentVersion()
		{
			if (!IsAlreadyInstalled)
			{
				CurrentVersion = String.Empty;
				return;
			}

			CurrentVersion = Modification.Game switch
			{
				PhonebookGames.EM4 => Emergency4ModificationService
					.FromModsDirectory(Emergency4GameLoader.GetEmergency4ModsPath)
					.Single(x => x.Name == RelatedLocalModification.Name)
					.Name,
				PhonebookGames.EM5 => ModificationService.LoadModList()
					.Single(x => x.Name == RelatedLocalModification.Name)
					.EmergencyProject.Properties.Version,
				_ => throw new NotImplementedException()
			};
		}

		private void CheckIfEm4ModIsOutdatedAsync()
		{
			if (string.IsNullOrEmpty(Emergency4GameLoader.LoadEmergencyInstallationPath()) ||
			    !Directory.Exists(Emergency4GameLoader.GetEmergency4ModsPath.FullName))
			{
				// do not do anything when no Em4 is there
				return;
			}

			if (!IsAlreadyInstalled)
			{
				return;
			}

			var modAndReleaseRequest = GetEmergency4ModAndReleaseFromE4InfoHashAsync();

			var isOutdated =
				Tools.IsModOutdated(RelatedLocalModification, modAndReleaseRequest.Item2.Version);
			Dispatcher.UIThread.Post(() => { IsOutdated = isOutdated; }, DispatcherPriority.Send);
		}

		private (Emergency4Modification, Release) GetEmergency4ModAndReleaseFromE4InfoHashAsync()
		{
			(Emergency4Modification, Release) result = (null, null);

			Project detailedProject;
			try
			{
				// get mod from mod store (cache)
				detailedProject = ModStoreLoader.ModStoreContent.GetProjectById(Modification.Id);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Log.Error(e);
				return result;
			}

			// get all mods
			var list = new AvaloniaList<Emergency4Modification>();
			list.AddRange(Emergency4ModificationService
				.FromModsDirectory(Emergency4GameLoader.GetEmergency4ModsPath)
				.Select(x => new Emergency4Modification(x)));

			// run though the mods and compare if they are any identical hashes
			foreach (var emergency4Modification in list)
			{
				foreach (var release in detailedProject.Releases)
				{
					if (string.IsNullOrEmpty(release.Hash))
					{
						continue;
					}

					var hash = emergency4Modification.GetProjectFileHash();
					if (!hash.Equals(release.Hash))
					{
						continue;
					}

					// if the hash matches then our assumption was incorrect
					result = (emergency4Modification, release);
					break;
				}
			}

			return result;
		}
	}
}