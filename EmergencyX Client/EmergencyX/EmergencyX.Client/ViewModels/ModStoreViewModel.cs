using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using Avalonia.Collections;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.Networking;
using EmergencyX.Emergency5.Logic;
using ReactiveUI;
using System.Threading.Tasks;
using Avalonia.Threading;
using DynamicData.Binding;
using EmergencyX.Client.Assets;
using EmergencyX.Client.Data.ModStore;
using EmergencyX.Client.Events;
using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Events;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Emergency4.Logic.Modifications;
using EmergencyX.Emergency5.Exceptions;
using EmergencyX.Emergency5.Logic.Modifications;
using EmergencyX.Emergency5.Utils;
using log4net;

namespace EmergencyX.Client.ViewModels
{
	public class ModStoreViewModel : ViewModelBase, IActivatableViewModel
	{
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(ModStoreViewModel));

		private AvaloniaList<ModStoreDetailViewModel> _onlineModList;
		private AvaloniaList<string> _gameCategoryFilterList;
		private int _gameCategoryFilterListSelectedIndex;
		private double _completeDownloadSize;
		private double _currentDownloadProgress;
		private bool _isDownloading;
		private bool _isDialogRequired;
		private bool _isInstallationLoading;
		private bool _isFetchingData;
		private bool _hasAgreedToTerms;
		private bool _allowFilter;
		private bool _isDownloadButtonVisible;
		private string _installationName;
		private string _dialogMessage;
		private string _currentInstallationPath;
		private string _modificationNameFilter;
		private double _installationTotal;
		private double _installationCurrent;
		private Queue<DownloadableItem> _downloadQueue;
		private Queue<InstallableItem> _installationQueue;
		private DownloadableItem _currentDownloadItem;
		private Project _urlRequestedProject;

		public ViewModelActivator Activator { get; }

		public AvaloniaList<ModStoreDetailViewModel> OnlineModList
		{
			get => _onlineModList;
			set => this.RaiseAndSetIfChanged(ref _onlineModList, value);
		}

		public AvaloniaList<string> GameCategoryFilterList
		{
			get => _gameCategoryFilterList;
			set => this.RaiseAndSetIfChanged(ref _gameCategoryFilterList, value);
		}

		public int GameCategoryFilterListSelectedIndex
		{
			get => _gameCategoryFilterListSelectedIndex;
			set { this.RaiseAndSetIfChanged(ref _gameCategoryFilterListSelectedIndex, value); }
		}

		public double CompleteDownloadSize
		{
			get => _completeDownloadSize;
			set => this.RaiseAndSetIfChanged(ref _completeDownloadSize, value);
		}

		public double CurrentDownloadProgress
		{
			get => _currentDownloadProgress;
			set => this.RaiseAndSetIfChanged(ref _currentDownloadProgress, value);
		}

		public bool IsDialogRequired
		{
			get => _isDialogRequired;
			set
			{
				_isDialogRequired = this.RaiseAndSetIfChanged(ref _isDialogRequired, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public bool IsDownloading
		{
			get => _isDownloading;
			set
			{
				this.RaiseAndSetIfChanged(ref _isDownloading, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public bool IsInstallationLoading
		{
			get => _isInstallationLoading;
			set
			{
				this.RaiseAndSetIfChanged(ref _isInstallationLoading, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		private bool IsFetchingData
		{
			get => _isFetchingData;
			set
			{
				this.RaiseAndSetIfChanged(ref _isFetchingData, value);
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		private bool HasAgreedToTerms
		{
			get => _hasAgreedToTerms;
			set
			{
				this.RaiseAndSetIfChanged(ref _hasAgreedToTerms, value);
				Settings.AppSettings.HasAgreedToDownloadTerms = value;
				Settings.AppSettings.Save();
				this.RaisePropertyChanged("GetIsDialogOrLoadingScreen");
			}
		}

		public bool AllowFilter
		{
			get => _allowFilter;
			set => this.RaiseAndSetIfChanged(ref _allowFilter, value);
		}

		public string InstallationName
		{
			get => _installationName;
			set
			{
				if (value != string.Empty)
				{
					this.RaiseAndSetIfChanged(ref _installationName,
						string.Concat(Assets.Resources.TextBloxkModInstallationWorkingOn,
							value));
					return;
				}

				this.RaiseAndSetIfChanged(ref _installationName, value);
			}
		}

		public double InstallationTotal
		{
			get => _installationTotal;
			set => this.RaiseAndSetIfChanged(ref _installationTotal, value);
		}

		public double InstallationCurrent
		{
			get => _installationCurrent;
			set => this.RaiseAndSetIfChanged(ref _installationCurrent, value);
		}

		public string DialogMessage
		{
			get => _dialogMessage;
			set => _dialogMessage = this.RaiseAndSetIfChanged(ref _dialogMessage, value);
		}

		public string CurrentInstallationPath
		{
			get => _currentInstallationPath;
			set => this.RaiseAndSetIfChanged(ref _currentInstallationPath, value);
		}

		public string GetEmergency5InstallationInformation
		{
			get => string.Format(Assets.Resources.InformationTextEmergency5Installation,
				GamePathLoader.LoadEmergencyInstallationPath());
		}

		public string GetEmergency4InstallationInformation
		{
			get => GetIsEmergency4Installed
				? string.Format(Assets.Resources.InformationTextEmergency4Installation,
					Emergency4GameLoader.LoadEmergencyInstallationPath())
				: string.Empty;
		}

		public string ModificationNameFilter
		{
			get => _modificationNameFilter;
			set { this.RaiseAndSetIfChanged(ref _modificationNameFilter, value); }
		}

		public Queue<DownloadableItem> DownloadQueue
		{
			get => _downloadQueue;
			set => this.RaiseAndSetIfChanged(ref _downloadQueue, value);
		}

		public Queue<InstallableItem> InstallationQueue
		{
			get => _installationQueue;
			set => this.RaiseAndSetIfChanged(ref _installationQueue, value);
		}

		public DownloadableItem CurrentDownloadItem
		{
			get => _currentDownloadItem;
			set => this.RaiseAndSetIfChanged(ref _currentDownloadItem, value);
		}
		
		public Project UrlRequestedProject
		{
			get => _urlRequestedProject;
			set => this.RaiseAndSetIfChanged(ref _urlRequestedProject, value);
		}

		public bool IsDownloadButtonVisible
		{
			get => _isDownloadButtonVisible;
			set => this.RaiseAndSetIfChanged(ref _isDownloadButtonVisible, value);
		}

		public bool GetIsDialogOrLoadingScreen =>
			IsDialogRequired || IsDownloading || IsInstallationLoading || IsFetchingData ||
			(!HasAgreedToTerms);

		public bool GetIsEmergency4Installed
		{
			get => !string.IsNullOrEmpty(Emergency4GameLoader.LoadEmergencyInstallationPath());
		}

		public bool GetIsEmergency5Installed
		{
			get => !string.IsNullOrEmpty(GamePathLoader.LoadEmergencyInstallationPath());
		}

		#region Commands

		public ReactiveCommand<Unit, Unit> DownloadAndInstallModification { get; }

		public ReactiveCommand<Unit, Unit> DialogOkCommand { get; }

		public ReactiveCommand<string, Unit> AcceptTermsCommand { get; }

		public ReactiveCommand<Unit, Unit> GoToSettingsCommand { get; }
		
		public ReactiveCommand<Unit,Unit> DialogBeginDownloadCommand { get; }

		#endregion


		public ModStoreViewModel()
		{
			#region Setup

			Activator = new ViewModelActivator();
			this.WhenActivated((CompositeDisposable disposables) =>
			{
				ModStoreLoader.ModStoreContent.Load();
				/* handle activation */
				this.WhenValueChanged(x => x.GameCategoryFilterListSelectedIndex)
					.Throttle(TimeSpan.FromMilliseconds(400)).ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(FilterByIndex!);
				this.WhenValueChanged(x => x.ModificationNameFilter)
					.Throttle(TimeSpan.FromMilliseconds(400))
					.ObserveOn(RxApp.MainThreadScheduler).Subscribe(FilterByName!);
				Disposable.Create(() =>
					{
						/* handle deactivation */
					})
					.DisposeWith(disposables);
			});

			#endregion

			AllowFilter = false;
			OnlineModList = new AvaloniaList<ModStoreDetailViewModel>();
			CurrentDownloadProgress = 0;
			CompleteDownloadSize = 0;
			DownloadQueue = new Queue<DownloadableItem>();
			InstallationQueue = new Queue<InstallableItem>();
			HasAgreedToTerms = Settings.AppSettings.HasAgreedToDownloadTerms;
			GameCategoryFilterListSelectedIndex = 0;
			ModificationNameFilter = String.Empty;
			IsDownloadButtonVisible = false;

			DownloadAndInstallModification = ReactiveCommand.Create(ExecuteDownloadAndInstallModification);
			DialogOkCommand = ReactiveCommand.Create(ExecuteOkCommand);
			AcceptTermsCommand = ReactiveCommand.Create<string>(ExecuteAcceptTermsCommand);
			GoToSettingsCommand = ReactiveCommand.Create(ExecuteGoToSettingsCommand);
			DialogBeginDownloadCommand = ReactiveCommand.Create(ExecuteDownloadAndInstallModification);

			GameCategoryFilterList = new AvaloniaList<string>()
			{
				Assets.Resources.FilterOptionAllGames, PhonebookGames.EM4.ToString(),
				PhonebookGames.EM5.ToString()
			};

			AllowFilter = true;
		}

		public ModStoreViewModel(Project projectToDownload) : this()
		{
			UrlRequestedProject = projectToDownload;
		}

		public void FilterByIndex(int index)
		{
			if (!AllowFilter)
			{
				return;
			}

			LoadAsync();
		}

		public void FilterByName(string name)
		{
			if (!AllowFilter)
			{
				return;
			}

			LoadAsync();
		}

		private void ExecuteAcceptTermsCommand(string obj)
		{
			if (obj.Equals(Assets.Resources.DialogMessageAgreeToTermsYes))
			{
				HasAgreedToTerms = true;
			}
			else
			{
				HasAgreedToTerms = false;
			}
		}

		private void ExecuteGoToSettingsCommand()
		{
			OnOpenViewFromViewEvent(new OpenViewFromViewEventArgs(string.Format("{0}.{1}",
				Assets.Resources.MenuItemNameEmergencyXClient,
				Assets.Resources.SubMenuItemNameSettings)));
		}

		private void ExecuteOkCommand()
		{
			DialogMessage = null;
			IsDialogRequired = false;
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
		}

		/// <summary>
		/// Queues releases for download
		/// </summary>
		private void ExecuteDownloadAndInstallModification()
		{

			
			IsDownloadButtonVisible = false;
			IsDialogRequired = false;
			
			var processingList = new AvaloniaList<ModStoreDetailViewModel>();
			processingList.AddRange(OnlineModList.Where(x =>
				x.IsMarkedForDownload));


			if (processingList.Count == 0)
			{
				Log.Info("No modifications to download");
				OnRequestMessageBox(
					new MessageBoxRequestEventArgs(Assets.Resources.ErrorMessageNothingToDownload));
				return;
			}

			Project project = null;
			foreach (var item in processingList)
			{
				try
				{
					project = ModStoreLoader.ModStoreContent.GetProjectById(item.Modification.Id);
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					Log.Error(e);
					EnableDialog(
						string.Format(Assets.Resources.ExceptionAnErrorOccuredLogFile,
							App.LogFile));
					return;
				}

				// check if the mod is still installed
				if (item.Modification.Game == PhonebookGames.EM4)
				{
					item.IsEm4ModAlreadyInstalledAsync();
				}

				if (item.Modification.Game == PhonebookGames.EM5)
				{
					item.IsEm5ModAlreadyInstalled();
				}

				if (!item.IsAlreadyInstalled)
				{
					item.CurrentVersion = "";
				}

				DownloadQueue.Enqueue(new DownloadableItem(item.Versions[item.SelectedVersionIndex],
					project, item.CurrentVersion));
			}

			BeginDownload();
		}

		private async void LoadAsync(string message = null)
		{
			AllowFilter = false;
			OnlineModList.Clear();
			try
			{
				IsFetchingData = true;
				DialogMessage = Assets.Resources.DialogMessageIsFetchingProjects;
				var onlineMods = await Task.Run(ModStoreLoader.ModStoreContent.GetProjectOverview);
				foreach (var item in onlineMods)
				{
					// if the user filtered for names
					if (ModificationNameFilter != string.Empty &&
					    !item.Name.Contains(ModificationNameFilter))
					{
						continue;
					}

					// if the user filtered for category
					if (GameCategoryFilterListSelectedIndex != 0 && item.Game.ToString() !=
					    GameCategoryFilterList[GameCategoryFilterListSelectedIndex])
					{
						continue;
					}

					OnlineModList.Add(new ModStoreDetailViewModel(item));
				}
			}
			catch (Exception e)
			{
				Log.Error(e);
				IsFetchingData = false;
				DialogMessage = String.Empty;
			}

			IsFetchingData = false;
			DialogMessage = String.Empty;

			if (OnlineModList.Count == 0)
			{
				IsDialogRequired = true;
				DialogMessage = Assets.Resources.DialogMessageNoProjects;
				AllowFilter = true;
				return;
			}

			AllowFilter = true;

			// check if view started with download request 
			if (UrlRequestedProject != null && OnlineModList.Any(x => x.Modification.Id == UrlRequestedProject.Id))
			{
				Log.Debug($"Got UrlRequestedProject {UrlRequestedProject.Id}");
				
				OnlineModList.Single(x => x.Modification.Id == UrlRequestedProject.Id).IsMarkedForDownload = true;
				message = string.Format(Assets.Resources.DialogMessageStartDownloadUrl, UrlRequestedProject.Name);
				IsDownloadButtonVisible = true;
				UrlRequestedProject = null;
			}

			if (!string.IsNullOrEmpty(message))
			{
				Log.Debug(message);
				EnableDialog(message);
			}
		}

		private void BeginDownload()
		{
			if (DownloadQueue.Count == 0)
			{
				LoadAsync(Resources.DialogMessageInstallSuccess);
				return;
			}

			CurrentDownloadItem = DownloadQueue.Dequeue();

			// pick a random artifact from the current download item artifact list, every artifact is a download link to a mirror
			// but also take "from" property into account!
			var artifactsForRelease = CurrentDownloadItem.Project.Releases
				.Single(x => x.Version == CurrentDownloadItem.RequestedVersion).Artifacts.ToList();

			// if there is a download item that wants to update from a specific version version then use that to filter (if any from artifacts exist
			if (!string.IsNullOrEmpty(CurrentDownloadItem.UpdateFromVersion) &&
			    artifactsForRelease.Any(x => x.From == CurrentDownloadItem.UpdateFromVersion))
			{
				artifactsForRelease =
					artifactsForRelease.Where(x => x.From == CurrentDownloadItem.UpdateFromVersion)
						.ToList();
			}
			else
			{
				artifactsForRelease = artifactsForRelease.Where(x => string.IsNullOrEmpty(x.From)).ToList();
			}

			Artifact artifact = null;

			var rand = new Random().Next(0, artifactsForRelease.Count);
			artifact = artifactsForRelease[rand];

			// not needed anymore
			artifactsForRelease = null;

			// define the download target for the file
			string targetFile = String.Empty;

			if (CurrentDownloadItem.Project.Game is PhonebookGames.EM4)
			{
				targetFile = Emergency4GameLoader.GetEmergency4ModsPath.FullName;

				// ToDo: Revisit this
				targetFile = Path.GetFullPath(Path.Combine(targetFile,
					string.Format("{0}{1}", CurrentDownloadItem.Project.Name, ".zip")));
			}

			if (CurrentDownloadItem.Project.Game is PhonebookGames.EM5)
			{
				targetFile = Settings.AppSettings.InstallToEmergency5DataDirectory
					? GamePathLoader.LoadEm5DataDirModificationPath()
					: GamePathLoader.LoadEm5AppDataModificationPath();
				targetFile = Path.GetFullPath(Path.Combine(targetFile,
					string.Format("{0}{1}",
						OnlineModList.Single(x =>
								x.Modification.Id == CurrentDownloadItem.Project.Id)
							.Modification.Name, ".zip")));
			}

			if (string.IsNullOrEmpty(targetFile))
			{
				throw new NotSupportedException();
			}

			DialogMessage = Path.GetFileNameWithoutExtension(targetFile);

			var modsDir = GamePathLoader.LoadEm5AppDataModificationPath();

			// apparently people install mods with out ever having the game started, so this could happen
			if (!string.IsNullOrEmpty(modsDir)  && !Directory.Exists(modsDir))
			{
				Log.Error($"{modsDir} did not exist. Creating!");
				Directory.CreateDirectory(modsDir);
			}
			
			if (File.Exists(targetFile))
			{
				targetFile = Path.GetTempFileName();
				targetFile = Path.ChangeExtension(targetFile, ".zip");
			}

			// disable menu to prevent any accidents
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(true));

			try
			{
				InstallationQueue.Enqueue(
					new InstallableItem(targetFile, artifact, CurrentDownloadItem.Project.Game));

				ModStoreConnector.DownloadReleaseForProjectAsync(artifact, targetFile,
					ClientOnDownloadProgressChanged,
					ClientOnDownloadCompleteAsync);
			}
			catch (Exception e)
			{
				Log.Error(e);
				EnableDialog(
					string.Format(Assets.Resources.ExceptionAnErrorOccuredLogFile, App.LogFile));
			}
		}

		private async void VerifyDownloadAndBeginInstallationAsync(InstallableItem dataForInstallation)
		{
			// parsing data from input
			var zipFile = dataForInstallation.FileName;
			var hash = dataForInstallation.Artifact.Hash;
			
			if (!File.Exists(zipFile))
			{

				// in case some installs a mod an never having run the game
				// AND on a previous occasion something went wrong
				var modDir = GamePathLoader.LoadEm5AppDataModificationPath();
				if (!string.IsNullOrEmpty(modDir) && !Directory.Exists(modDir))
				{
					Log.Error("The EM5 app data mod directory still does not exist (we should have previously created it...). Please run your game once and try again.");
					EnableDialog(
						string.Format(Assets.Resources.ExceptionAnErrorOccuredLogFile, App.LogFile));
				}
				else
				{
					Log.Error(new FileNotFoundException("The file was not found. Possible ACL issue?",
						zipFile));
					EnableDialog(
						string.Format(Assets.Resources.ExceptionAnErrorOccuredLogFile, App.LogFile));
				}
				
				return;
			}

			// check for 0 byte file - seems to happen when EmX tries to download from GitLab for example
			// maybe Cloud Flare
			var fInfo = new FileInfo(zipFile);
			if (fInfo.Length == 0)
			{
				Log.Error(new FileNotFoundException(
					"The file returned a 0 byte file. Possible CloudFlare or something?",
					zipFile));
				EnableDialog(
					string.Format(Assets.Resources.ExceptionAnErrorOccuredLogFile, App.LogFile));
				return;
			}

			var computedHash = await ContentIdentification.GetSha512Async(zipFile);
			if (computedHash.ToLower() != hash.ToLower())
			{
				Log.Info(
					$"Hash value of downloaded data does not match server reported hash\n{computedHash.ToLower()} - {hash.ToLower()}");
				throw new Exception("Invalid hash value.");
			}

			CurrentInstallationPath = zipFile;

			// disable menu to prevent accidents
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(true));

			// at this stage our zip file has been validated, install the mod in a new thread

			AbstractModificationService installationService;
			switch (dataForInstallation.Game)
			{
				case PhonebookGames.EM4:
					installationService = new Emergency4ModificationService();
					break;
				case PhonebookGames.EM5:
					installationService = new ModificationService();
					break;
				case PhonebookGames.EMONE:
				default:
					throw new NotImplementedException();
			}

			installationService.ModificationInstallationStartedEvent = null;
			installationService.ModificationInstallationProgressEvent = null;
			installationService.ModificationEndedEvent = null;
			installationService.ModificationInstallationStartedEvent +=
				OnModificationInstallationStartedEvent;
			installationService.ModificationInstallationProgressEvent +=
				OnModificationInstallationProgress;
			installationService.ModificationEndedEvent += OnModificationInstallationEnded;

			try
			{
				switch (dataForInstallation.Game)
				{
					case PhonebookGames.EM5:
						await (installationService as ModificationService).InstallAsync(new[]
							{ zipFile });
						CacheClearing.ClearCache(GamePathLoader.LoadEm5CachePath());
						break;
					case PhonebookGames.EM4:
						await (installationService as Emergency4ModificationService)
							.InstallAsync(new[] { zipFile },
								new DirectoryInfo(
									Path.Combine(
										Emergency4GameLoader
											.LoadEmergencyInstallationPath(),
										"Mods")));
						break;
					case PhonebookGames.EMONE:
					default:
						throw new NotSupportedException();
				}
			}
			catch (NotSufficentSpaceForModInstallException diskSpaceException)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(Assets.Resources.ExceptionDiskSpaceMessage);
				Log.Error(diskSpaceException);
				// re enable menu
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
			}
			catch (NotSupportedException notSupportedException)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(Assets.Resources.ExceptionNotSupportedMessage);
				Log.Error(notSupportedException);
				// re enable menu
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
			}
			catch (Exception e)
			{
				OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true));
				EnableDialog(
					string.Format(Assets.Resources.ExceptionAnErrorOccuredLogFile, App.LogFile));
				Log.Error(e);

				// re enable menu
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
			}
			finally
			{
				installationService = null;
			}

			installationService = null;
		}

		private void EnableDialog(string message)
		{
			Log.Debug(message);
			IsDialogRequired = true;
			DialogMessage = message;
		}

		#region EventHandlerImplementation

		private void ClientOnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			if (CompleteDownloadSize == 0)
			{
				IsDownloading = true;
				CompleteDownloadSize = e.TotalBytesToReceive;
			}

			CurrentDownloadProgress = e.BytesReceived;
		}

		private async void ClientOnDownloadCompleteAsync(object sender, AsyncCompletedEventArgs e)
		{
			try
			{
				await Task.Run(() =>
				{
					VerifyDownloadAndBeginInstallationAsync(InstallationQueue.Dequeue());
					return Task.CompletedTask;
				});
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception);
				Log.Error(exception);
				Log.Error("Might be Windows UAC.");
			}
			finally
			{
				CurrentDownloadItem = new DownloadableItem();

				// now that the hashing has finished
				CurrentDownloadProgress = 0;
				CompleteDownloadSize = 0;
				IsDownloading = false;

				// re enable menu
				OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));
				//OnModificationInstallationEnded(this, new ModificationInstallationEndEventArgs(true)); // ToDo: Error handling is poor right now. However this dosen't work due to race condition
			}
		}

		private void OnModificationInstallationStartedEvent(object sender,
			ModificationInstallationStartedEventArgs e)
		{
			// set name of the current zip file
			InstallationName = e.ModName;
			InstallationTotal = e.NoOfFiles;

			// unhide progress gui
			if (!IsInstallationLoading)
			{
				IsInstallationLoading = true;
			}
		}

		private void OnModificationInstallationProgress(object sender, InstallationProgressEventArgs e)
		{
			// report progress to gui
			InstallationCurrent = e.Progress;
		}

		private void OnModificationInstallationEnded(object sender, ModificationInstallationEndEventArgs e)
		{
			// hide progress gui, and reload mod list
			InstallationName = string.Empty;
			if (IsInstallationLoading)
			{
				IsInstallationLoading = !IsInstallationLoading;
			}

			InstallationTotal = 0;
			InstallationCurrent = 0;

			if (File.Exists(CurrentInstallationPath))
			{
				File.Delete(CurrentInstallationPath);
			}

			// re enable menu
			OnEnableDisableMenuEvent(new EnableDisableMenuEventArgs(false));

			if (e.IsFinnishedWithError)
			{
				EnableDialog(string.Format(Resources.ExceptionAnErrorOccuredLogFile, App.LogFile));
				return;
			}

			Dispatcher.UIThread.InvokeAsync(BeginDownload);
		}

		#endregion
	}
}