using System;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reflection;
using Avalonia.Collections;
using EmergencyX.Client.Enum;
using EmergencyX.Client.Events;
using EmergencyX.Client.Styles;
using EmergencyX.Client.Utility.Avalonia;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.NamedPipeConnection;
using EmergencyX.Emergency4.Logic;
using EmergencyX.Windows.Enum;
using EmergencyX.Windows.Logic;
using log4net;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
    public class SettingsViewModel : ViewModelBase, IActivatableViewModel
    {
        public static readonly ILog Log =
            LogManager.GetLogger(typeof(SettingsViewModel));

        private bool _installToEmergency5DataDirectory;
        private bool _skipEm5Launcher;
        private string _emergency5Path;
        private string _emergency4Path;
        private string _selectedTheme;
        private bool _useLegacyDecompression;
        private bool _allowEmergencyXLinks;

        public ViewModelActivator Activator { get; }

        #region Commands

        public ReactiveCommand<Unit, Unit> SelectEmergency5InstallationPath { get; }
        public ReactiveCommand<Unit, Unit> SelectEmergency4InstallationPath { get; }

        public ReactiveCommand<Unit, Unit> SaveSettingsCommand { get; }

        #endregion

        public bool InstallToEmergency5DataDirectory
        {
            get => _installToEmergency5DataDirectory;
            set => this.RaiseAndSetIfChanged(ref _installToEmergency5DataDirectory, value);
        }

        public bool SkipEm5Launcher
        {
            get => _skipEm5Launcher;
            set => this.RaiseAndSetIfChanged(ref _skipEm5Launcher, value);
        }

        public string Emergency5Path
        {
            get => _emergency5Path;
            set => this.RaiseAndSetIfChanged(ref _emergency5Path, value);
        }

        public string Emergency4Path
        {
            get => _emergency4Path;
            set
            {
                this.RaiseAndSetIfChanged(ref _emergency4Path, value);
                this.RaisePropertyChanged("GetIfEm4PathIsEmpty");
            }
        }

        public string SelectedTheme
        {
            get => _selectedTheme;
            set
            {
                this.RaiseAndSetIfChanged(ref _selectedTheme, value);
                ApplyStyle();
            }
        }

        public AvaloniaList<string> Styles
        {
            get
            {
                var list = new AvaloniaList<string>();
                list.AddRange(StyleConfig.GetStyles);
                return list;
            }
        }

        public bool GetIfEm4PathIsEmpty
        {
            get => string.IsNullOrEmpty(Emergency4GameLoader.LoadEmergencyInstallationPath());
        }

        public bool UseLegacyDecompression
        {
            get => _useLegacyDecompression;
            set => this.RaiseAndSetIfChanged(ref _useLegacyDecompression, value);
        }

        public bool AllowEmergencyXLinks
        {
            get => _allowEmergencyXLinks;
            set => this.RaiseAndSetIfChanged(ref _allowEmergencyXLinks, value);
        }

        public SettingsViewModel()
        {
            #region Setup

            Activator = new ViewModelActivator();
            this.WhenActivated((CompositeDisposable disposables) =>
            {
                /* handle activation */
                Load();
                Disposable
                    .Create(() =>
                    {
                        /* handle deactivation */
                        Settings.AppSettings.ShowFavStars = false;
                    })
                    .DisposeWith(disposables);
            });

            #endregion

            #region AssignCommand

            SelectEmergency5InstallationPath =
                ReactiveCommand.Create(ExecuteSelectEmergency5InstallationPathAsync);
            SelectEmergency4InstallationPath =
                ReactiveCommand.Create(ExecuteSelectEmergency4InstallationPathAsync);
            SaveSettingsCommand = ReactiveCommand.Create(ExecuteSaveSettingsCommand);

            #endregion
        }

        private void ExecuteSaveSettingsCommand()
        {
            Apply();
            Settings.AppSettings.Save();
            OnRefreshMenuRequestEvent();
        }

        private async void ExecuteSelectEmergency4InstallationPathAsync()
        {
            var result = await OpenPathUtil.OpenPathDialog(String.Empty).ShowAsync(App.GetMainWindow);

            if (string.IsNullOrEmpty(result))
            {
                OnRequestMessageBox(new MessageBoxRequestEventArgs(
                    Assets.Resources.ErrorMessageNothingSelected));
                return;
            }

            result = Path.GetFullPath(result);
            if (!Directory.Exists(result))
            {
                OnRequestMessageBox(new MessageBoxRequestEventArgs(
                    Assets.Resources.ErrorMessageDirectoryDoseNotExist,
                    MessageBoxIconType.Exclamationmark));
                return;
            }

            if (!File.Exists(Path.Combine(result, @"Em4.exe")))
            {
                Log.Warn("Invalid Em4 directory picked.");
                OnRequestMessageBox(new MessageBoxRequestEventArgs(
                    Assets.Resources.ErrorMessageEm4NotFound, MessageBoxIconType.Exclamationmark));
                return;
            }

            Emergency4Path = Path.GetFullPath(result);
        }

        private async void ExecuteSelectEmergency5InstallationPathAsync()
        {
            var result = await OpenPathUtil.OpenPathDialog(String.Empty).ShowAsync(App.GetMainWindow);

            if (string.IsNullOrEmpty(result))
            {
                OnRequestMessageBox(new MessageBoxRequestEventArgs(
                    Assets.Resources.ErrorMessageNothingSelected));
                return;
            }

            result = Path.GetFullPath(result);
            if (!Directory.Exists(result))
            {
                OnRequestMessageBox(new MessageBoxRequestEventArgs(
                    Assets.Resources.ErrorMessageDirectoryDoseNotExist,
                    MessageBoxIconType.Exclamationmark));
                return;
            }

            if (!File.Exists(Path.Combine(result, @"bin\em5_launcher.exe")))
            {
                OnRequestMessageBox(
                    new MessageBoxRequestEventArgs(
                        Assets.Resources.ErrorMessageEm5LauncherNotFound,
                        MessageBoxIconType.Exclamationmark));
                return;
            }

            Emergency5Path = Path.GetFullPath(result);
        }

        private void Load()
        {
            Settings.AppSettings.ShowFavStars = true;

            InstallToEmergency5DataDirectory = Settings.AppSettings.InstallToEmergency5DataDirectory;
            SkipEm5Launcher = Settings.AppSettings.SkipLauncher;
            Emergency5Path = Settings.AppSettings.Emergency5ManualPath;
            Emergency4Path = Settings.AppSettings.Emergency4ManualPath;
            SelectedTheme = StyleConfig.GetSelectedStyle();
            UseLegacyDecompression = Settings.AppSettings.UseLegacyDecompression;
            AllowEmergencyXLinks = Settings.AppSettings.AllowEmergencyXLinks;
        }

        private void Apply()
        {
            // save values without direct consequence
            Settings.AppSettings.InstallToEmergency5DataDirectory = InstallToEmergency5DataDirectory;
            Settings.AppSettings.SkipLauncher = SkipEm5Launcher;
            Settings.AppSettings.Emergency5ManualPath = Emergency5Path;
            Settings.AppSettings.Emergency4ManualPath = Emergency4Path;
            Settings.AppSettings.UseLegacyDecompression = UseLegacyDecompression;

            /*
             *
             * Save values that have consequences:
             * AllowEmergencyXLinks impacts the Registry
             *
             */

            // if settings have been enabled but are now disabled
            if (Settings.AppSettings.AllowEmergencyXLinks && !AllowEmergencyXLinks)
            {
                var data = RegistryAccess.GetRegistryValueFromSubKey(NamedPipeSettings.MAIN_HANDLER_KEY_NAME,
                    SubKeys.CurrentClassesRoot, "");

                if (string.IsNullOrEmpty(data))
                {
                    Log.Warn("Settings say links are enabled, but seems disabled!");
                }

                DisableEmergencyXLinks();
            }

            // if settings have been disabled but are now enabled
            if (!Settings.AppSettings.AllowEmergencyXLinks && AllowEmergencyXLinks)
            {
                var data = RegistryAccess.GetRegistryValueFromSubKey(NamedPipeSettings.MAIN_HANDLER_KEY_NAME,
                    SubKeys.CurrentClassesRoot, "");

                if (!string.IsNullOrEmpty(data))
                {
                    Log.Warn("Settings say links are disabled, but seems enabled!");
                }

                EnableEmergencyXLinks();
            }

            // save what ever the value now is
            Settings.AppSettings.AllowEmergencyXLinks = AllowEmergencyXLinks;


            ApplyStyle();
        }

        private void ApplyStyle()
        {
            StyleConfig.SaveSelectedStyle(SelectedTheme);
        }

        private void EnableEmergencyXLinks()
        {
            CustomProtocolHandler.AddCustomProtocolHandler(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
        }

        private void DisableEmergencyXLinks()
        {
            CustomProtocolHandler.RemoveCustomProtocolHandler(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
        }
    }
}