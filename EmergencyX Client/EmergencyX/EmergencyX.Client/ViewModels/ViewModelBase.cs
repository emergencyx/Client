﻿using System;
using Avalonia.Threading;
using EmergencyX.Client.Events;
using ReactiveUI;

namespace EmergencyX.Client.ViewModels
{
	public class ViewModelBase : ReactiveObject
	{
		public EventHandler<EnableDisableMenuEventArgs> EnableDisableMenuEventHandler;
		public EventHandler<OpenViewFromViewEventArgs> OpenViewFromViewEventHandler;
		public EventHandler RefreshMenuRequestEventHandler;
		public EventHandler<MessageBoxRequestEventArgs> MessageBoxRequestEventHandler;

		protected virtual void OnEnableDisableMenuEvent(EnableDisableMenuEventArgs e)
		{
			Dispatcher.UIThread.Post(() => { EnableDisableMenuEventHandler?.Invoke(this, e); },
				DispatcherPriority.Send);
		}

		protected virtual void OnOpenViewFromViewEvent(OpenViewFromViewEventArgs e)
		{
			Dispatcher.UIThread.Post(() => { OpenViewFromViewEventHandler?.Invoke(this, e); },
				DispatcherPriority.Send);
		}

		protected virtual void OnRefreshMenuRequestEvent()
		{
			Dispatcher.UIThread.Post(
				() => { RefreshMenuRequestEventHandler?.Invoke(this, EventArgs.Empty); },
				DispatcherPriority.Send);
		}

		protected virtual void OnRequestMessageBox(MessageBoxRequestEventArgs e)
		{
			Dispatcher.UIThread.Post(() => { MessageBoxRequestEventHandler?.Invoke(this, e); },
				DispatcherPriority.Send);
		}
	}
}