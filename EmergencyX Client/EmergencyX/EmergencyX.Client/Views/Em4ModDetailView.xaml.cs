using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using EmergencyX.Emergency5.Data;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em4ModDetailView : ReactiveUserControl<Em4ModDetailViewModel>
	{
		#region ControlDefinition

		// Get the controls from the XAML view. This is done so we can programatically bind the properties in the constructor instead of in the XAML. This enables better memory handling and type checking
		public TextBlock ModificationNameTextBlock => this.FindControl<TextBlock>("ModificationName");
		public TextBlock ModificationAuthorTextBlock => this.FindControl<TextBlock>("ModificationAuthor");
		public Image CustomModIcon => this.FindControl<Image>("CustomModIcon");
		public Image DefaultModIcon => this.FindControl<Image>("DefaultModIcon");
		public Button BtnDeleteMod => this.FindControl<Button>("BtnDeleteMod");
		public Button ButtonOpenBelongingModDir => this.FindControl<Button>("ButtonOpenBelongingModDir");

		public Button ButtonRunModification => this.FindControl<Button>("ButtonRunModification");

		public Button ButtonRunModificationWithFms => this.FindControl<Button>("ButtonRunModificationWithFms");

		#endregion

		public Em4ModDetailView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.OneWayBind(ViewModel,
						viewModel => viewModel.Modification.Name,
						view => view.ModificationNameTextBlock.Text)
					.DisposeWith(disposableRegistration);

				this.OneWayBind(ViewModel,
						viewModel => viewModel.Modification.Author,
						view => view.ModificationAuthorTextBlock.Text)
					.DisposeWith(disposableRegistration);

				this.OneWayBind(ViewModel,
						viewModel => viewModel.GetGameModificationIconPath,
						view => view.CustomModIcon.Source)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel,
						viewModel => viewModel.IsDefaultModIcon,
						view => view.DefaultModIcon.IsVisible)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.DeleteGameModification,
					view => view.BtnDeleteMod.Command).DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.OpenBelongingModificationDirectory,
					view => view.ButtonOpenBelongingModDir.Command);
				this.OneWayBind(ViewModel, viewModel => viewModel.RunModification,
					view => view.ButtonRunModification.Command);
				this.OneWayBind(ViewModel, viewModel => viewModel.RunModificationWithFms,
					view => view.ButtonRunModificationWithFms.Command);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}