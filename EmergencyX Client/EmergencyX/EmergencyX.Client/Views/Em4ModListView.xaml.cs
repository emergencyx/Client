using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
    public class Em4ModListView : ReactiveUserControl<Em4ModListViewModel>
    {
        public Em4ModListView()
        {
            this.WhenActivated(disposableRegistration =>
            {
                this.OneWayBind(ViewModel, viewModel => viewModel.Em4ModificationList,
                    view => view.GetEm4ModListBox.ItemsSource);
            });

            AvaloniaXamlLoader.Load(this);
        }

        public ItemsRepeater GetEm4ModListBox => this.FindControl<ItemsRepeater>("Em4ModListBox");
    }
}