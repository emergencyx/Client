using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5CacheClearingView : ReactiveUserControl<Em5CacheClearingViewModel>
	{
		public StackPanel GetStackPanelBeginDeleteDialog =>
			this.FindControl<StackPanel>("StackPanelBeginDeleteDialog");

		public StackPanel GetStackPanelDeleteDialog => this.FindControl<StackPanel>("StackPanelDeleteDialog");

		public TextBlock GetTextBlockCacheClearStatus =>
			this.FindControl<TextBlock>("TextBlockCacheClearStatus");

		public Em5CacheClearingView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.OneWayBind(ViewModel, viewModel => viewModel.IsBeginCacheClear,
					view => view.GetStackPanelBeginDeleteDialog.IsEnabled);
				this.OneWayBind(ViewModel, viewModel => viewModel.IsBeginCacheClear,
					view => view.GetStackPanelBeginDeleteDialog.IsVisible);

				this.OneWayBind(ViewModel, viewModel => viewModel.IsShowCacheClearDialog,
					view => view.GetStackPanelDeleteDialog.IsEnabled);
				this.OneWayBind(ViewModel, viewModel => viewModel.IsShowCacheClearDialog,
					view => view.GetStackPanelDeleteDialog.IsVisible);

				this.OneWayBind(ViewModel, viewModel => viewModel.CacheClearStatus,
					view => view.GetTextBlockCacheClearStatus.Text);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}