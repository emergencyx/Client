using System.Reactive.Disposables;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5CreateSupportPackView : ReactiveUserControl<Em5CreateSupportPackViewModel>
	{
		public Em5CreateSupportPackView()
		{
			this.WhenActivated(disposableRegistration =>
			{
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}