using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using EmergencyX.Emergency5.Data;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5ModDetailView : ReactiveUserControl<Em5ModDetailViewModel>
	{
		#region ControlDefinition

		// Get the controls from the XAML view. This is done so we can programatically bind the properties in the constructor instead of in the XAML. This enables better memory handling and type checking
		public TextBlock ModificationNameTextBlock => this.FindControl<TextBlock>("ModificationName");
		public TextBlock ModificationAuthorTextBlock => this.FindControl<TextBlock>("ModificationAuthor");
		public TextBlock ModificationDescriptionTextBlock =>
			this.FindControl<TextBlock>("ModificationDescription");
		public Image CustomModIcon => this.FindControl<Image>("CustomModIcon");
		public Image DefaultModIcon => this.FindControl<Image>("DefaultModIcon");
		public Image DrawingPresenterModDeactivated =>
			this.FindControl<Image>("DrawingPresenterModDeactivated");
		public Image DrawingPresenterModActive =>
			this.FindControl<Image>("DrawingPresenterModActive");
		public Button BtnDeleteMod => this.FindControl<Button>("BtnDeleteMod");
		public Button BtnActivateDeactivateMod => this.FindControl<Button>("BtnActivateDeactivateMod");
		public Button BtnOrderDown => this.FindControl<Button>("BtnOrderDown");
		public Button BtnOrderUp => this.FindControl<Button>("BtnOrderUp");
		public TextBlock ModificationVersionTextBlock => this.FindControl<TextBlock>("ModificationVersion");
		public TextBlock ModificationIsOutdatedTextBlock =>
			this.FindControl<TextBlock>("ModificationIsOutdated");
		public Button ButtonOpenBelongingModDir => this.FindControl<Button>("ButtonOpenBelongingModDir");

		#endregion

		public Em5ModDetailView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.OneWayBind(ViewModel,
						viewModel => viewModel.Modification.Name,
						view => view.ModificationNameTextBlock.Text)
					.DisposeWith(disposableRegistration);

				this.OneWayBind(ViewModel,
						viewModel => viewModel.Modification.Author,
						view => view.ModificationAuthorTextBlock.Text)
					.DisposeWith(disposableRegistration);

				this.OneWayBind(ViewModel,
						viewModel => viewModel.Modification.GetDescriptionWithLineBreak,
						view => view.ModificationDescriptionTextBlock.Text)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel,
						viewModel => viewModel.GetGameModificationIconPath,
						view => view.CustomModIcon.Source)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel,
						viewModel => viewModel.IsDefaultModIcon,
						view => view.DefaultModIcon.IsVisible)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.IncreaseOrderingIndexOfModification,
					view => view.BtnOrderUp.Command).DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.DecreaseOrderingIndexOfModification,
					view => view.BtnOrderDown.Command).DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.EnableDisableModificationCommand,
						view => view.BtnActivateDeactivateMod.Command)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.DeleteGameModification,
					view => view.BtnDeleteMod.Command).DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.Modification.IsEnabled,
					view => view.DrawingPresenterModActive.IsEnabled);
				this.OneWayBind(ViewModel, viewModel => viewModel.Modification.IsEnabled,
					view => view.DrawingPresenterModDeactivated.IsEnabled, b => !b);
				this.OneWayBind(ViewModel, viewModel => viewModel.Modification.IsEnabled,
					view => view.DrawingPresenterModActive.IsVisible);
				this.OneWayBind(ViewModel, viewModel => viewModel.Modification.IsEnabled,
					view => view.DrawingPresenterModDeactivated.IsVisible, b => !b);
				this.OneWayBind(ViewModel, viewModel => viewModel.GetModVersionAsFormattedString,
					view => view.ModificationVersionTextBlock.Text);
				this.OneWayBind(ViewModel, viewModel => viewModel.IsOutdated,
					view => view.ModificationIsOutdatedTextBlock.IsVisible);
				this.OneWayBind(ViewModel, viewModel => viewModel.OpenBelongingModificationDirectory,
					view => view.ButtonOpenBelongingModDir.Command);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}