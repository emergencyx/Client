using System;
using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using EmergencyX.Emergency5.Data;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5ModListView : ReactiveUserControl<Em5ModListViewModel>
	{
		public ItemsRepeater GetEm5ModListBox => this.FindControl<ItemsRepeater>("Em5ModListBox");

		public Em5ModListView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.OneWayBind(ViewModel, viewModel => viewModel.Em5ModificationList,
					view => view.GetEm5ModListBox.ItemsSource);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}