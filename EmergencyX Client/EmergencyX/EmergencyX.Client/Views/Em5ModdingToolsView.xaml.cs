using System.Reactive.Disposables;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5ModdingToolsView : ReactiveUserControl<Em5ModdingToolsViewModel>
	{
		public ProgressBar GetDiffProgressbar => this.FindControl<ProgressBar>("DiffProgressbar");
		public Em5ModdingToolsView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.OneWayBind(ViewModel, viewModel => viewModel.DiffTotal,
					view => view.GetDiffProgressbar.Maximum);
				this.OneWayBind(ViewModel, viewModel => viewModel.DiffProgress,
					view => view.GetDiffProgressbar.Value);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}