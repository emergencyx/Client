using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using EmergencyX.Emergency5.Data;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5ScreenshotDetailView : ReactiveUserControl<Em5ScreenshotDetailViewModel>
	{
		public Image GetImageScreenshot => this.Find<Image>("ImageScreenshot");
		public TextBlock GetTextBlockScreenshotName => this.FindControl<TextBlock>("TextBlockScreenshotName");
		public Button GetButtonExportAsJPG => this.FindControl<Button>("ButtonExportAsJPG");
		public Button GetButtonExportAsPNG => this.FindControl<Button>("ButtonExportAsPNG");
		public Button GetButtonExportAsGIF => this.FindControl<Button>("ButtonExportAsGIF");

		public Em5ScreenshotDetailView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.OneWayBind(ViewModel, viewModel => viewModel.GetPathAsSource,
					view => view.GetImageScreenshot.Source);
				this.OneWayBind(ViewModel, viewModel => viewModel.Screenshot.ScreenshotName,
					view => view.GetTextBlockScreenshotName.Text);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}