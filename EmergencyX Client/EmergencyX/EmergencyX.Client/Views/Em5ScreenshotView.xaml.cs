using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5ScreenshotView : ReactiveUserControl<Em5ScreenshotViewModel>
	{
		public ItemsRepeater GetRepeaterEm5ScreenshotsView =>
			this.Find<ItemsRepeater>("RepeaterEm5ScreenshotsView");

		public Em5ScreenshotView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.OneWayBind(ViewModel, viewModel => viewModel.Screenshots,
					view => view.GetRepeaterEm5ScreenshotsView.ItemsSource);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}