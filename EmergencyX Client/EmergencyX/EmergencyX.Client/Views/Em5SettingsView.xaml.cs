using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class Em5SettingsView : ReactiveUserControl<Em5SettingsViewModel>
	{
		public CheckBox GetCheckBoxShineThroughActive =>
			this.FindControl<CheckBox>("CheckBoxShineThroughActive");

		public CheckBox GetCheckBoxBuildingClippingActive =>
			this.FindControl<CheckBox>("CheckBoxBuildingClippingActive");

		public CheckBox GetCheckBoxShowGroundMarkersForAllOwnUnits =>
			this.FindControl<CheckBox>("CheckBoxShowGroundMarkersForAllOwnUnits");

		public NumericUpDown GetNumericUpDownXScreenShotRes =>
			this.FindControl<NumericUpDown>("NumericUpDownXScreenShotRes");

		public NumericUpDown GetNumericUpDownYScreenShotRes =>
			this.FindControl<NumericUpDown>("NumericUpDownYScreenShotRes");

		public Em5SettingsView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.Bind(ViewModel, viewModel => viewModel.IsShineThroughActive,
					view => view.GetCheckBoxShineThroughActive.IsChecked);
				this.Bind(ViewModel, viewModel => viewModel.IsBuildingClippingActive,
					view => view.GetCheckBoxBuildingClippingActive.IsChecked);
				this.Bind(ViewModel, viewModel => viewModel.ShowGroundMarkersForAllOwnUnits,
					view => view.GetCheckBoxShowGroundMarkersForAllOwnUnits.IsChecked);
				this.Bind(ViewModel, viewModel => viewModel.ScreenshotResolutionX,
					view => view.GetNumericUpDownXScreenShotRes.Value, VmToViewConverter, ViewToVmConverter);
				this.Bind(ViewModel, viewModel => viewModel.ScreenshotResolutionY,
					view => view.GetNumericUpDownYScreenShotRes.Value,VmToViewConverter, ViewToVmConverter);
			});

			AvaloniaXamlLoader.Load(this);
		}

		private int ViewToVmConverter(decimal? arg)
		{
			return Convert.ToInt32(arg);
		}

		private decimal? VmToViewConverter(int arg)
		{
			return Convert.ToDecimal(arg);
		}
	}
}