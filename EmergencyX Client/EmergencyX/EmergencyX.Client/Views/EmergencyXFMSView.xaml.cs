using System;
using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using EmergencyX.Emergency5.Data;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class EmergencyXFMSView : ReactiveUserControl<EmergencyXFMSViewModel>
	{
		public EmergencyXFMSView()
		{
			this.WhenActivated(disposableRegistration => { });

			AvaloniaXamlLoader.Load(this);
		}
	}
}