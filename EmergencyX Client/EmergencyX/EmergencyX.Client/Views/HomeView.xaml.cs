using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class HomeView : ReactiveUserControl<HomeViewModel>
	{
		private DockPanel GetDockPanelEM5 => this.FindControl<DockPanel>("DockPanelEM5");
		private TextBlock GetTextBlockEm5Name => this.FindControl<TextBlock>("TextBlockEm5Name");
		private TextBlock GetTextBlockEm5GameVersion => this.FindControl<TextBlock>("TextBlockEm5GameVersion");

		private TextBlock GetTextBlockEm5InstalledMods =>
			this.FindControl<TextBlock>("TextBlockEm5InstalledMods");

		private Button GetButtonEm5OutdatedWarning =>
			this.FindControl<Button>("ButtonBlockEm5OutdatedWarning");

		private DockPanel GetDockPanelEM4 => this.FindControl<DockPanel>("DockPanelEM4");
		private TextBlock GetTextBlockEm4Name => this.FindControl<TextBlock>("TextBlockEm4Name");
		private TextBlock GetTextBlockEm4GameVersion => this.FindControl<TextBlock>("TextBlockEm4GameVersion");

		private TextBlock GetTextBlockEm4InstalledMods =>
			this.FindControl<TextBlock>("TextBlockEm4InstalledMods");

		private Button GetButtonEm4OutdatedWarning =>
			this.FindControl<Button>("ButtonBlockEm4OutdatedWarning");

		public HomeView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				// EM5
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency5.Name,
					view => view.GetTextBlockEm5Name.Text);
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency5.Version,
					view => view.GetTextBlockEm5GameVersion.Text);
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency5.NumberOfMods,
					view => view.GetTextBlockEm5InstalledMods.Text);
				this.OneWayBind(ViewModel, viewModel => viewModel.IsEm5Installed,
					view => view.GetDockPanelEM5.IsVisible);
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency5.IsOutdated,
					view => view.GetButtonEm5OutdatedWarning.IsVisible);

				// EM4
				this.OneWayBind(ViewModel, viewModel => viewModel.IsEm4Installed,
					view => view.GetDockPanelEM4.IsVisible);
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency4.Name,
					view => view.GetTextBlockEm4Name.Text);
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency4.Version,
					view => view.GetTextBlockEm4GameVersion.Text);
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency4.NumberOfMods,
					view => view.GetTextBlockEm4InstalledMods.Text);
				this.OneWayBind(ViewModel, viewModel => viewModel.Emergency4.IsOutdated,
					view => view.GetButtonEm4OutdatedWarning.IsVisible);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}