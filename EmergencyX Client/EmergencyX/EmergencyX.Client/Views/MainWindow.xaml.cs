using System;
using System.ComponentModel;
using System.Net;
using System.Reactive.Disposables;
using System.Runtime.CompilerServices;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using Avalonia.ReactiveUI;
using EmergencyX.Client.Styles;
using EmergencyX.Client.ViewModels;
using EmergencyX.Core.Annotations;
using log4net;
using NetSparkleUpdater;
using NetSparkleUpdater.Enums;
using NetSparkleUpdater.SignatureVerifiers;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class MainWindow : ReactiveWindow<MainWindowViewModel>, INotifyPropertyChanged
	{
		private SparkleUpdater _sparkle;
		private string _dialogMessage;

		public static readonly ILog Log =
			LogManager.GetLogger(typeof(MainWindow));

		public ContentControl GetCurrrentContentControl =>
			this.FindControl<ContentControl>("MainWindowContent");

		public ContentControl GetMenuViewContent => this.FindControl<ContentControl>("MenuViewContent");

		public string DialogMessage
		{
			get => _dialogMessage;
			set
			{
				_dialogMessage = value;
				OnPropertyChanged();
			}
		}

		public MainWindow()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.Bind(ViewModel,
						viewModel => viewModel.CurrentView,
						view => view.GetCurrrentContentControl.Content)
					.DisposeWith(disposableRegistration);
				this.Bind(ViewModel, viewModel => viewModel.MenuViewModel,
					view => view.GetMenuViewContent.Content).DisposeWith(disposableRegistration);
				this.Bind(ViewModel, viewModel => viewModel.DialogMessage,
					view => view.DialogMessage);
			});

			InitializeComponent();
			StyleConfig.LoadSelectedStyle();

#if DEBUG
			this.AttachDevTools();
#endif

			try
			{
				// NetSparkleUpdater
				string manifestModuleName = System.Reflection.Assembly.GetEntryAssembly()
					?.ManifestModule
					.FullyQualifiedName;
				var casturi =
					string.IsNullOrEmpty(Core.Logic.Settings.Settings.AppSettings
						.BetaChannel)
						? "https://clientcdn.emergencyx.de/client/updatecast.xml"
						: Core.Logic.Settings.Settings.AppSettings.BetaChannel;

				SolidColorBrush backgroundBrush = SolidColorBrush.Parse("Black");
				if (Application.Current != null)
				{
					backgroundBrush = StyleConfig.GetSelectedStyle() ==
					                  Assets.Resources.ThemeNameDark
						? Application.Current.Resources["EmXDarkBrush"] as SolidColorBrush
						: Application.Current.Resources["EmXLightBrush"] as SolidColorBrush;
				}

				_sparkle = new SparkleUpdater(casturi,
					new Ed25519Checker(SecurityMode.Strict,
						"BmobbCU5RBQn7ZNclRPb0o/YmiUHN8CEQDZUKFl3tqY="))
				{
					UIFactory = new NetSparkleUpdater.UI.Avalonia.UIFactory(Icon)
					{
						HideRemindMeLaterButton = true, HideSkipButton = true
					},
					// Avalonia version doesn't support separate UI threads: https://github.com/AvaloniaUI/Avalonia/issues/3434#issuecomment-573446972
					ShowsUIOnMainThread = true,
					//UseNotificationToast = false // Avalonia version doesn't yet support notification toast messages
				};

				_sparkle.LogWriter = Log.IsInfoEnabled ? new LogWriter(true) : new LogWriter();

				// use best TLS version possible without deactivating others
				_sparkle.SecurityProtocolType |= SecurityProtocolType.Tls |
				                                 SecurityProtocolType.Tls12 |
				                                 SecurityProtocolType.Tls13;

				_sparkle.StartLoop(true, true);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Log.Error(e);
			}
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}

		#region MiscPropertyChanged

		public new event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}