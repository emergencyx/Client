using System.Collections.Generic;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using DynamicData;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class MenuView : ReactiveUserControl<MenuViewModel>
	{

		public MenuView()
		{
			this.WhenActivated(disposableRegistration =>
			{
			});
			
			AvaloniaXamlLoader.Load(this);
		}
	}
}