using System;
using System.Diagnostics;
using System.Reactive.Disposables;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class ModStoreDetailView : ReactiveUserControl<ModStoreDetailViewModel>
	{
		#region ControlDefinitions

		public CheckBox GetCheckBoxIsMarkedForDownload =>
			this.FindControl<CheckBox>("CheckBoxIsMarkedForDownload");

		public TextBlock GetTextBlockModificationName =>
			this.FindControl<TextBlock>("TextBlockModificationName");

		public TextBlock GetTextBlockGameCategory => this.FindControl<TextBlock>("TextBlockGameCategory");

		public StackPanel GetPanelModInfos => this.FindControl<StackPanel>("PanelModInfos");

		public TextBlock GetTextBlockIsAlreadyInstalled =>
			this.FindControl<TextBlock>("TextBlockIsAlreadyInstalled");

		public TextBlock GetTextBlockIsOutdated => this.FindControl<TextBlock>("TextBlockIsOutdated");

		public TextBlock GetTextBlockLatestVersion => this.FindControl<TextBlock>("TextBlockLatestVersion");

		public ComboBox GetComboBoxVersions => this.FindControl<ComboBox>("ComboBoxVersions");

		#endregion

		public ModStoreDetailView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.Bind(ViewModel, viewModel => viewModel.IsMarkedForDownload,
						view => view.GetCheckBoxIsMarkedForDownload.IsChecked)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.Modification.Name,
						view => view.GetTextBlockModificationName.Text)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.GetIsAlreadyInstalledOrIsOutdated,
					view => view.GetPanelModInfos.IsVisible).DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.IsAlreadyInstalled,
						view => view.GetTextBlockIsAlreadyInstalled.IsVisible)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.IsOutdated,
						view => view.GetTextBlockIsOutdated.IsVisible)
					.DisposeWith(disposableRegistration);
				this.OneWayBind(ViewModel, viewModel => viewModel.Modification.LatestVersion,
						view => view.GetTextBlockLatestVersion.Text)
					.DisposeWith(disposableRegistration);

				this.OneWayBind(ViewModel, viewModel => viewModel.Versions,
					view => view.GetComboBoxVersions.ItemsSource).DisposeWith(disposableRegistration);
				this.BindCommand(ViewModel,
					viewModel => viewModel.LoadAllVersions,
					view => view.GetComboBoxVersions,
					"PointerEntered").DisposeWith(disposableRegistration);

				this.Bind(ViewModel, viewModel => viewModel.SelectedVersionIndex,
					view => view.GetComboBoxVersions.SelectedIndex);

				this.Bind(ViewModel, viewModel => viewModel.Modification.Game,
					view => view.GetTextBlockGameCategory.Text);

				// this is a fix for when the LoadAllVersions Command executes and the collection changes
				// then the value is not set by the viewmodel to 0 for some reason
				this.WhenAnyValue(x => x.GetComboBoxVersions.SelectedIndex).Subscribe(x =>
				{
					if (x == -1)
					{
						this.GetComboBoxVersions.SelectedIndex = 0;
					}
				}).DisposeWith(disposableRegistration);
			});
			AvaloniaXamlLoader.Load(this);
		}
	}
}