using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class ModStoreView : ReactiveUserControl<ModStoreViewModel>
	{
		public ProgressBar GetProgressBarDownloadProgress =>
			this.FindControl<ProgressBar>("ProgressBarDownloadProgress");

		public ItemsRepeater GetItemsRepeaterOnlineModifications =>
			this.FindControl<ItemsRepeater>("ItemsRepeaterOnlineModifications");

		public ModStoreView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.Bind(ViewModel, viewModel => viewModel.CompleteDownloadSize,
					view => view.GetProgressBarDownloadProgress.Maximum);
				this.Bind(ViewModel, viewModel => viewModel.CurrentDownloadProgress,
					view => view.GetProgressBarDownloadProgress.Value);
				this.OneWayBind(ViewModel, viewModel => viewModel.OnlineModList,
					view => view.GetItemsRepeaterOnlineModifications.ItemsSource);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}