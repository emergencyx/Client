using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using EmergencyX.Client.ViewModels;
using ReactiveUI;

namespace EmergencyX.Client.Views
{
	public class SettingsView : ReactiveUserControl<SettingsViewModel>
	{
		public CheckBox GetCheckBoxInstallToEm5DataDirectory =>
			this.FindControl<CheckBox>("CheckBoxInstallToEm5DataDirectory");

		public CheckBox GetCheckBoxCheckBoxSkipEm5Launcher =>
			this.FindControl<CheckBox>("CheckBoxSkipEm5Launcher");

		public CheckBox GetCheckBoxUseLegacyDecompression =>
			this.FindControl<CheckBox>("CheckBoxUseLegacyDecompression");

		public ComboBox GetComboBoxStyleSelection => this.FindControl<ComboBox>("ComboBoxStyleSelection");

		public CheckBox GetCheckBoxCheckBoxEnableEmergency4 =>
			this.FindControl<CheckBox>("CheckBoxEnableEmergency4");

		public TextBox GetTextBoxEm5Path => this.FindControl<TextBox>("TextBoxEm5Path");
		public TextBox GetTextBoxEm4Path => this.FindControl<TextBox>("TextBoxEm4Path");

		public CheckBox GetCheckboxAllowEmergencyXLinks =>
			this.FindControl<CheckBox>("CheckboxAllowEmergencyXLinks");

		public SettingsView()
		{
			this.WhenActivated(disposableRegistration =>
			{
				this.Bind(ViewModel, viewModel => viewModel.InstallToEmergency5DataDirectory,
					view => view.GetCheckBoxInstallToEm5DataDirectory.IsChecked);
				this.Bind(ViewModel, viewModel => viewModel.SkipEm5Launcher,
					view => view.GetCheckBoxCheckBoxSkipEm5Launcher.IsChecked);
				this.OneWayBind(ViewModel, viewModel => viewModel.Styles,
					view => view.GetComboBoxStyleSelection.ItemsSource);
				this.Bind(ViewModel, viewModel => viewModel.SelectedTheme,
					view => view.GetComboBoxStyleSelection.SelectedItem);
				this.Bind(ViewModel, viewModel => viewModel.Emergency5Path,
					view => view.GetTextBoxEm5Path.Text);
				this.Bind(ViewModel, viewModel => viewModel.Emergency4Path,
					view => view.GetTextBoxEm4Path.Text);
				this.Bind(ViewModel, viewModel => viewModel.UseLegacyDecompression,
					view => view.GetCheckBoxUseLegacyDecompression.IsChecked);
				this.Bind(ViewModel, viewModel => viewModel.AllowEmergencyXLinks,
					view => view.GetCheckboxAllowEmergencyXLinks.IsChecked);
			});

			AvaloniaXamlLoader.Load(this);
		}
	}
}