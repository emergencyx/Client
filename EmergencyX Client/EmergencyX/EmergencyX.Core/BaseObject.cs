using System.ComponentModel;
using System.Runtime.CompilerServices;
using EmergencyX.Core.Annotations;

namespace EmergencyX.Core
{
	public class BaseObject : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}