using System;
using Newtonsoft.Json;

namespace EmergencyX.Core.Converters.Json
{
	public class BooleanJsonConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if ((bool) value)
			{
				writer.WriteValue("true");
			}
			else
			{
				writer.WriteValue("false");
			}
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
			JsonSerializer serializer)
		{
			if (reader.Value.ToString().ToLower() == "true")
			{
				return true;
			}

			return false;
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(bool);
		}
	}
}