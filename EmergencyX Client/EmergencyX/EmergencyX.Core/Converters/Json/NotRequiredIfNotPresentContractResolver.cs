using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace EmergencyX.Core.Converters.Json
{
	public class NotRequiredIfNotPresentContractResolver : DefaultContractResolver
	{

		protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
		{
			var property = base.CreateProperty(member, memberSerialization);
			property.Required = Required.Default;
			return property;
		}
		
	}
}