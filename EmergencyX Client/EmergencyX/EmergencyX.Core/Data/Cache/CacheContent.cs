using System;
using System.Collections.ObjectModel;
using EmergencyX.Core.Data.NetworkRequest;

namespace EmergencyX.Core.Data.Cache
{
	public class CacheContent<T> : BaseObject
	{
		private DateTime _lastWrite;
		private ObservableCollection<T> _items;

		public DateTime LastWrite
		{
			get => _lastWrite;
			set
			{
				_lastWrite = value;
				OnPropertyChanged();
			}
		}

		public ObservableCollection<T> Items
		{
			get => _items;
			set
			{
				_items = value;
				OnPropertyChanged();
			}
		}

		public CacheContent(DateTime time, ObservableCollection<T> list)
		{
			LastWrite = time;
			Items = list;
		}
	}
}