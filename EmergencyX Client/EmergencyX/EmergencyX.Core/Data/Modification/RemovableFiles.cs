using System.Collections.Generic;

namespace EmergencyX.Core.Data.Modification
{
	public class RemovableFiles : BaseObject
	{
		private bool _allowDeletion;
		private List<string> _relativePathNamesOfDeletableFiles;

		public List<string> RelativePathNamesOfDeletableFiles
		{
			get => _relativePathNamesOfDeletableFiles;
			set
			{
				_relativePathNamesOfDeletableFiles = value;
				OnPropertyChanged();
			}
		}

		public bool AllowDeletion
		{
			get => _allowDeletion;
			set
			{
				_allowDeletion = value;
				OnPropertyChanged();
			}
		}

		public RemovableFiles(bool allowDeletion, List<string> relativePathNamesOfDeletableFiles)
		{
			AllowDeletion = allowDeletion;
			RelativePathNamesOfDeletableFiles = relativePathNamesOfDeletableFiles;
		}
	}
}