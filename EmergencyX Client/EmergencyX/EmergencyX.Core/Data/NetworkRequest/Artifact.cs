using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace EmergencyX.Core.Data.NetworkRequest
{
	public class Artifact : BaseObject
	{
		private string _href;
		private string _hash;
		private string _from;

		[JsonProperty("href")]
		public string Href
		{
			get => _href;
			set
			{
				_href = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("hash")]
		public string Hash
		{
			get => _hash;
			set
			{
				_hash = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("from", Required = Required.AllowNull)]
		[DataMember(IsRequired = false)]
		public string From
		{
			get => _from;
			set
			{
				_from = value;
				OnPropertyChanged();
			}
		}
	}
}