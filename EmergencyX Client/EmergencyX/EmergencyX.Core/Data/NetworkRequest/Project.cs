using System.Collections.ObjectModel;
using EmergencyX.Core.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EmergencyX.Core.Data.NetworkRequest
{
	public class Project : BaseObject
	{
		private string _id;
		private string _name;
		private ObservableCollection<Release> _releases;
		private PhonebookGames _game;

		[JsonProperty("id")]
		public string Id
		{
			get => _id;
			set
			{
				_id = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("name")]
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("releases")]
		public ObservableCollection<Release> Releases
		{
			get => _releases;
			set
			{
				_releases = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("game")]
		[JsonConverter(typeof(StringEnumConverter))]
		public PhonebookGames Game
		{
			get => _game;
			set
			{
				_game = value;
				OnPropertyChanged();
			}
		}
		
	}
}