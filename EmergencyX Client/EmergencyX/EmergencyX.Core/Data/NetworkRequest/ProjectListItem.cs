using EmergencyX.Core.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EmergencyX.Core.Data.NetworkRequest
{
	public class ProjectListItem : BaseObject
	{
		private string _id;
		private string _name;
		private string _latestVersion;
		private string _latestHash;
		private PhonebookGames _game;

		[JsonProperty("id")]
		public string Id
		{
			get => _id;
			set
			{
				_id = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("name")]
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("latestVersion")]
		public string LatestVersion
		{
			get => _latestVersion;
			set
			{
				_latestVersion = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("latestHash")]
		public string LatestHash
		{
			get => _latestHash.ToLower();
			set
			{
				_latestHash = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("game")]
		[JsonConverter(typeof(StringEnumConverter))]
		public PhonebookGames Game
		{
			get => _game;
			set
			{
				_game = value;
				OnPropertyChanged();
			}
		}
	}
}