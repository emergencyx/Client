using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace EmergencyX.Core.Data.NetworkRequest
{
	public class Release : BaseObject
	{
		private string _version;
		private string _hash;
		private ObservableCollection<Artifact> _artifacts;
		private string _e4modInfoHash;

		[JsonProperty("version")]
		public string Version
		{
			get => _version;
			set
			{
				_version = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("artifacts")]
		public ObservableCollection<Artifact> Artifacts
		{
			get => _artifacts;
			set
			{
				_artifacts = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("hash")]
		public string Hash
		{
			get => _hash.ToLower();
			set
			{
				_hash = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("e4mod_info_hash")]
		[DataMember(IsRequired = false)]
		[Obsolete("Do not use E4ModInfoHash as it is not delivered anymore")]
		public string E4ModInfoHash
		{
			get => _e4modInfoHash;

			set => _e4modInfoHash = value;
		}
	}
}