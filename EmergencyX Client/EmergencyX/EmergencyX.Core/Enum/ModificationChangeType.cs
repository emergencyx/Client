namespace EmergencyX.Core.Enum
{
	public enum ModificationChangeType
	{
		OrderingIndexIncrease,
		OrderingIndexDecrease,
		ActivationStateChanged,
		ModificationDeleted,
		ModificationIsDeleting
	}
}