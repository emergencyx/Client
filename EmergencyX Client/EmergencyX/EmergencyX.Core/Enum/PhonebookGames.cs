using System.Runtime.Serialization;

namespace EmergencyX.Core.Enum
{
	public enum PhonebookGames
	{
		[EnumMember(Value = "EM4")] EM4,
		[EnumMember(Value = "EM5")] EM5,
		[EnumMember(Value = "EMONE")] EMONE
	}
}