using System;
using System.Collections.Generic;

namespace EmergencyX.Core.Events;

public class CommunicationServerMessageReceivedEvent : EventArgs
{
    public string Message { get; set; }

    public CommunicationServerMessageReceivedEvent(string m)
    {
        Message = m;
    }

    public CommunicationServerMessageReceivedEvent(List<string> m)
    {
        var stringData = string.Concat(m);
        Message = stringData;
    }
}