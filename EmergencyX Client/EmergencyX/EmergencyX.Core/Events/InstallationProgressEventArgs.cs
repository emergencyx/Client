using System;

namespace EmergencyX.Core.Events
{
	public class InstallationProgressEventArgs : EventArgs
	{
		private int _progress;

		public int Progress
		{
			get => _progress;
			set => _progress = value;
		}
	}
}