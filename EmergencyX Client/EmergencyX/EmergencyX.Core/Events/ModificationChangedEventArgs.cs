using System;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Interfaces;

namespace EmergencyX.Core.Events
{
	public class ModificationChangedEventArgs : EventArgs
	{
		private ModificationChangeType _type;
		private object _payload;

		public ModificationChangeType Type
		{
			get => _type;
			set => _type = value;
		}

		public object Payload
		{
			get => _payload;
			set => _payload = value;
		}

		public ModificationChangedEventArgs(ModificationChangeType type, object payload)
		{

			if (type == ModificationChangeType.ModificationDeleted)
			{
				if (payload == null)
				{
					throw new ArgumentNullException();
				}

				if (!(payload is IGameModification) || (payload is Exception))
				{
					throw new ArgumentException("The payload has to be an GameModification");
				}
			}
			
			Type = type;
			Payload = payload;
		}
	}
}