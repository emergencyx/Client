using System;

namespace EmergencyX.Core.Events
{
	public class ModificationDiffProgressedEventArgs : EventArgs
	{
		private int _progress;

		public int Progress
		{
			get => _progress;
			set => _progress = value;
		}
	}
}