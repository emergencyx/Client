using System;

namespace EmergencyX.Core.Events
{
	public class ModificationInstallationEndEventArgs : EventArgs
	{
		public bool IsFinnishedWithError { get; set; }

		public ModificationInstallationEndEventArgs(bool isFinnishedWithError)
		{
			IsFinnishedWithError = isFinnishedWithError;
		}
	}
}