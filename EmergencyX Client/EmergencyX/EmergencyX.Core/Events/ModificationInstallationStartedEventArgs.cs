using System;

namespace EmergencyX.Core.Events
{
	public class ModificationInstallationStartedEventArgs : EventArgs
	{
		private string _modName;
		private int _noOfFiles;

		public string ModName
		{
			get => _modName;
			set => _modName = value;
		}

		public int NoOfFiles
		{
			get => _noOfFiles;
			set => _noOfFiles = value;
		}
	}
}