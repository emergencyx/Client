using System;
using System.Threading.Tasks;
using EmergencyX.Core.Events;

namespace EmergencyX.Core.Interfaces
{
	public abstract class AbstractModificationService
	{
		public EventHandler<ModificationInstallationStartedEventArgs> ModificationInstallationStartedEvent;
		public EventHandler<InstallationProgressEventArgs> ModificationInstallationProgressEvent;
		public EventHandler<ModificationInstallationEndEventArgs> ModificationEndedEvent;

		public abstract void OnModificationInstallationEventStart(ModificationInstallationStartedEventArgs e);
		public abstract void OnModificationInstallationProgress(InstallationProgressEventArgs e);
		public abstract void OnModificationInstallationEndedEvent(ModificationInstallationEndEventArgs e);
	}
}