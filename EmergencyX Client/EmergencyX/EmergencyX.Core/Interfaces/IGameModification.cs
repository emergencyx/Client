namespace EmergencyX.Core.Interfaces
{
	public interface IGameModification
	{
		string Name
		{
			get;
			set;
		}

		string Author
		{
			get;
			set;
		}

		string Description
		{
			get;
			set;
		}
		
		string DataPath
		{
			get;
			set;
		}

		string GetModIconName
		{
			get;
		}

		string GetProjectFileHash();
	}
}