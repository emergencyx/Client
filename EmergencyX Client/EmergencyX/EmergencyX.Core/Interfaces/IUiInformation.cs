namespace EmergencyX.Core.Interfaces
{
	public interface IUiInformation
	{

		public bool IsDarkMode();

	}
}