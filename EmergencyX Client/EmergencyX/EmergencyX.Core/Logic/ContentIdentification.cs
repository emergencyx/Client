using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using EmergencyX.Core.Annotations;
using EmergencyX.Core.Util;
using log4net;
using static Standart.Hash.xxHash.xxHash64;

namespace EmergencyX.Core.Logic
{
	public class ContentIdentification
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(ContentIdentification));

		/**
		* Enumerate the size and hash of a directory
		*/
		public static (long size, ulong hash) CreateContentHashForDirectory(DirectoryInfo directory)
		{
			long size = 0;
			ulong hash = 0;
			foreach (var subdirectory in directory.EnumerateDirectories())
			{
				var (subsize, subhash) = CreateContentHashForDirectory(subdirectory);
				size += subsize;
				hash ^= subhash;
			}

			foreach (var file in directory.EnumerateFiles())
			{
				size += file.Length;
				hash ^= ComputeHash(file.OpenRead());
			}

			return (size, hash);
		}

		public static async Task<string> GetSha512Async([NotNull] string file)
		{
			if (file == null) throw new ArgumentNullException(nameof(file));
			string result;
			// compute hash for file and compare it to the hash reported by the server
			await using var stream = File.Open(file, FileMode.Open, FileAccess.Read,
				FileShare.ReadWrite);
			using (var hasher = SHA512.Create())
			{
				var computeHash = hasher.ComputeHash(stream);
				result = Hash.ByteArrayToString(computeHash);
			}

			stream.Close();

			return result.ToLower();
		}

		public static string GetSha512([NotNull] string file)
		{
			if (file == null) throw new ArgumentNullException(nameof(file));
			string result;
			// compute hash for file and compare it to the hash reported by the server
			using var stream = File.Open(file, FileMode.Open, FileAccess.Read,
				FileShare.ReadWrite);
			using (var hasher = SHA512.Create())
			{
				var computeHash = hasher.ComputeHash(stream);
				result = Hash.ByteArrayToString(computeHash);
			}

			stream.Close();

			return result.ToLower();
		}
	}
}