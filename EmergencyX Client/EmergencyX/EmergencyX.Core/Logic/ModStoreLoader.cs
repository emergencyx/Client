using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Logic.WebServiceCache;
using EmergencyX.Core.Logic.Settings;

namespace EmergencyX.Core.Logic
{
	public class ModStoreLoader : BaseObject
	{
		private static ModStoreLoader _modStoreContent;
		private static readonly object _lock = new object();

		private ModStoreProjectCache _projectCache;
		private ModStoreProjectItemsCache _projectItemsCache;

		public static ModStoreLoader ModStoreContent
		{
			get
			{
				if (_modStoreContent == null)
				{
					lock (_lock)
					{
						if (_modStoreContent == null)
						{
							_modStoreContent = new ModStoreLoader();
						}
					}
				}

				return _modStoreContent;
			}
			private set => _modStoreContent = value;
		}

		internal ModStoreProjectCache ProjectCache
		{
			get => _projectCache;
			set
			{
				_projectCache = value;
				OnPropertyChanged();
			}
		}

		internal ModStoreProjectItemsCache ProjectItemsCache
		{
			get => _projectItemsCache;
			set
			{
				_projectItemsCache = value;
				OnPropertyChanged();
			}
		}

		private ModStoreLoader()
		{
		}

		public void Load()
		{
			if (!Directory.Exists(Path.GetDirectoryName(Settings.Settings.AppSettings.ProjectItemCache)))
			{
				Directory.CreateDirectory(
					Path.GetDirectoryName(Settings.Settings.AppSettings.ProjectItemCache) ??
					throw new InvalidOperationException());
			}

			if (!Directory.Exists(Path.GetDirectoryName(Settings.Settings.AppSettings.ProjectsCache)))
			{
				Directory.CreateDirectory(
					Path.GetDirectoryName(Settings.Settings.AppSettings.ProjectsCache) ??
					throw new InvalidOperationException());
			}

			ProjectItemsCache =
				new ModStoreProjectItemsCache(Settings.Settings.AppSettings.ProjectItemCache);
			ProjectCache = new ModStoreProjectCache(Settings.Settings.AppSettings.ProjectsCache);
		}

		public ObservableCollection<ProjectListItem> GetProjectOverview()
		{
			return ProjectItemsCache.GetCacheContent().data;
		}

		public ObservableCollection<Project> GetProjectsList()
		{
			return ProjectCache.GetCacheContent().data;
		}

		public Project GetProjectById(string id)
		{
			return ProjectCache.GetCacheContent().data.FirstOrDefault(x => x.Id == id);
		}

		public bool GetReleaseAndProjectByProjectFileHash(string hash,
			out (Release release, Project project) result)
		{
			result = new ValueTuple<Release, Project>();
			Project project = null;
			Release release = null;

			if (GetProjectsList().Any(x =>
				    x.Releases.Any(y =>
					    String.Equals(y.Hash, hash, StringComparison.CurrentCultureIgnoreCase))))
			{
				// get the release that matches our hash
				release = GetProjectsList().FirstOrDefault(x =>
						x.Releases.Any(y => String.Equals(y.Hash, hash,
							StringComparison.CurrentCultureIgnoreCase)))
					?.Releases.FirstOrDefault(x =>
						String.Equals(x.Hash, hash,
							StringComparison.CurrentCultureIgnoreCase)) ?? null;

				// get the project of that release
				project = GetProjectsList().FirstOrDefault(x =>
					          x.Releases.Any(y => String.Equals(y.Hash, hash,
						          StringComparison.CurrentCultureIgnoreCase))) ??
				          null;

				result.project = project;
				result.release = release;
			}

			// return if both are set
			return project != null && release != null;
		}
	}
}