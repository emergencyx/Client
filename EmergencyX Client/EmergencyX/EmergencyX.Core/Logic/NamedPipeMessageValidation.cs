using System.Text.RegularExpressions;
using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Enum;

namespace EmergencyX.Core.Logic;

public class NamedPipeMessageValidation
{

    public static NamedPipeActionType ValidateActionType(string dataStr)
    {
        if (dataStr.Contains("downloadAction"))
        {
            return NamedPipeActionType.DownloadAction;
        }

        return NamedPipeActionType.None;
    }
    
    public static bool ValidatePattern(string dataStr)
    {
        // if the data string is too long, it probably does not only contain the action and the emergencyx web id
        var regex = new Regex(@"^emergencyx:\/\/downloadAction\/\?parm=([0-9A-zA-Z]{20,375})$");
        return regex.Match(dataStr).Success;
    }

    public static Project ValidateEmergencyExplorerProjectId(string s)
    {

        var result = ModStoreLoader.ModStoreContent.GetProjectById(s);
        if (result != null && result.Id == s)
        {
            return result;
        }

        return null;
    }
}