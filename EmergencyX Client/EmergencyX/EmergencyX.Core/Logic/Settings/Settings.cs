using System;
using System.IO;
using Newtonsoft.Json;
using EmergencyX.Core.Util;

namespace EmergencyX.Core.Logic.Settings
{
	public class Settings : BaseObject
	{
		/// <summary>
		/// Static members
		/// </summary>
		[JsonIgnore] private static Settings _appSettings;

		[JsonIgnore] private static readonly object _lock = new object();
#if DEBUG
		[JsonIgnore] private static readonly string
			_path = Path.Combine(PathUtil.GetEmxAppDataPath(), "clientdevsettings.json");
#else
		[JsonIgnore] private static readonly string
			_path = Path.Combine(PathUtil.GetEmxAppDataPath(), "clientsettings.json");
#endif

		[JsonIgnore] private static readonly string
			_cachePath = Path.GetFullPath(Path.Combine(PathUtil.GetEmxAppDataPath(), "cache"));

		// Instance members

		private bool _installToEmergency5DataDirectory;

		private string _favouriteMenuItem;

		private bool _skipLauncher;

		private bool _enableEmergency4;

		private string _emergency5ManualPath;

		private string _emergency4ManualPath;

		private string _phoneBook;

		private string _appStyle;

		private string _betaChannel;

		private bool _hasAgreedToDownloadTerms;

		private bool _showFavStars;

		private bool _useLegacyDecrompession;

		private readonly string _userAgent;

		private string _fmsServer;

		private bool _allowEmergencyXLinks;

		/// <summary>
		/// Static properties
		/// </summary>

		[JsonIgnore]
		public static Settings AppSettings
		{
			get
			{
				if (_appSettings == null)
				{
					lock (_lock)
					{
						if (_appSettings == null)
						{
							if (File.Exists(_path))
							{
								_appSettings = Load();
								CheckDefaultValues(false);
							}
							else
							{
								_appSettings = new Settings();
								CheckDefaultValues(true);
							}
						}
					}
				}

				return _appSettings;
			}
			private set => _appSettings = value;
		}

		/// <summary>
		/// Instance properties
		/// </summary>
		[JsonProperty]
		public bool InstallToEmergency5DataDirectory
		{
			get => _installToEmergency5DataDirectory;
			set
			{
				_installToEmergency5DataDirectory = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty]
		public string FavouriteMenuItem
		{
			get => _favouriteMenuItem;
			set
			{
				_favouriteMenuItem = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty]
		public bool SkipLauncher
		{
			get => false;
			set
			{
				if (value == true)
				{
					value = false;
				}

				// ToDo: Deactivated due to problems with the SkipStartMenu parameter, check if enable is possible again at some point
				_skipLauncher = false;
				OnPropertyChanged();
			}
		}

		[JsonProperty]
		[Obsolete]
		public bool EnableEmergency4
		{
			get => _enableEmergency4;
			set
			{
				_enableEmergency4 = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty]
		public string Emergency5ManualPath
		{
			get => _emergency5ManualPath;
			set
			{
				_emergency5ManualPath = value;
				if (!string.IsNullOrEmpty(_emergency5ManualPath) &&
				    new DirectoryInfo(_emergency5ManualPath).Parent == null)
				{
					throw new Exception($"Path {_emergency5ManualPath} is illegal");
				}

				OnPropertyChanged();
			}
		}

		[JsonProperty]
		public string Emergency4ManualPath
		{
			get => _emergency4ManualPath;
			set
			{
				_emergency4ManualPath = value;
				if (!string.IsNullOrEmpty(_emergency4ManualPath) &&
				    new DirectoryInfo(_emergency4ManualPath).Parent == null)
				{
					throw new Exception($"Path {_emergency4ManualPath} is illegal");
				}

				OnPropertyChanged();
			}
		}

		[JsonProperty]
		[Obsolete]
		public string PhoneBook
		{
			get => _phoneBook;
			set => _phoneBook = value;
		}

		[JsonProperty]
		public string AppStyle
		{
			get => _appStyle;
			set
			{
				_appStyle = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty]
		public string BetaChannel
		{
			get => _betaChannel;
			set
			{
				_betaChannel = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty]
		public bool HasAgreedToDownloadTerms
		{
			get => _hasAgreedToDownloadTerms;
			set
			{
				_hasAgreedToDownloadTerms = value;
				OnPropertyChanged();
			}
		}

		[JsonIgnore]
		public bool ShowFavStars
		{
			get => _showFavStars;
			set
			{
				_showFavStars = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty]
		public bool UseLegacyDecompression
		{
			get => _useLegacyDecrompession;
			set
			{
				_useLegacyDecrompession = value;
				OnPropertyChanged();
			}
		}

		[JsonIgnore]
		public string UserAgent
		{
			get => _userAgent;
		}

		[JsonProperty]
		public string FmsServer
		{
			get => _fmsServer;
			set
			{
				_fmsServer = value;
				OnPropertyChanged();
			}
		}

		[JsonIgnore]
		public string ProjectItemCache { get; } =
			Path.GetFullPath(Path.Combine(_cachePath, "projectItemsCache.json"));

		[JsonIgnore]
		public string ProjectsCache { get; } = Path.GetFullPath(Path.Combine(_cachePath, "projectsCache.json"));

		[JsonProperty]
		public bool AllowEmergencyXLinks
		{
			get => _allowEmergencyXLinks;
			set
			{
				_allowEmergencyXLinks = value;
				OnPropertyChanged();
			}
		}

		private static Settings Load()
		{
			return JsonConvert.DeserializeObject<Settings>(File.ReadAllText(_path));
		}

		public void Save()
		{
			var text = JsonConvert.SerializeObject(this, Formatting.Indented);
			File.WriteAllText(_path, text);
		}

		private static void CheckDefaultValues(bool init)
		{
			if (init)
			{
				AppSettings.InstallToEmergency5DataDirectory = false;

				AppSettings.HasAgreedToDownloadTerms = false;

				AppSettings.UseLegacyDecompression = true;

				AppSettings.AllowEmergencyXLinks = true;
			}

			// ToDo: See ToDo of property SkipLauncher
			AppSettings.SkipLauncher = false;

			if (string.IsNullOrEmpty(AppSettings.PhoneBook))
			{
				AppSettings.PhoneBook = "https://phonebook.emergencyx.de/";
			}

			AppSettings.ShowFavStars = false;
		}

		/// <summary>
		/// Private constructor, used to initially set options that have not been included in the first version of Emergency Explorer
		/// Therefore ensuring the correct starting value is used
		/// </summary>
		private Settings()
		{
			UseLegacyDecompression = true;
			AllowEmergencyXLinks = true;
			_userAgent = "Emergency Explorer / " +
			             System.Reflection.Assembly.GetEntryAssembly()?.GetName().Version;
		}
	}
}