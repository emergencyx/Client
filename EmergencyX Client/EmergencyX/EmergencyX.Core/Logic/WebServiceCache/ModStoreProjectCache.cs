using System;
using System.IO;
using System.Collections.ObjectModel;
using EmergencyX.Core.Data.Cache;
using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Networking;
using Newtonsoft.Json;

namespace EmergencyX.Core.Logic.WebServiceCache
{
	public class ModStoreProjectCache : WebServiceCache<Project>
	{
		public ModStoreProjectCache(string file)
		{
			CacheFileType = WebServiceCacheFileType.JSON;
			CacheFilePath = file;
			CacheType = WebServiceCacheType.Project;
			Content = ReadCache();
		}

		public override (DateTime time, ObservableCollection<Project> data) GetCacheContent()
		{
			Content ??= ReadCache();

			return (Content.LastWrite, Content.Items);
		}

		protected override void WriteCacheContent(ObservableCollection<Project> data)
		{
			if (data == null)
			{
				data = new ObservableCollection<Project>();
			}

			var dataToWrite = new CacheContent<Project>(DateTime.Now, data);
			Content = dataToWrite;
			var text = JsonConvert.SerializeObject(dataToWrite);
			File.WriteAllText(CacheFilePath, text);
		}

		public override void UpdateCache()
		{
			var result = new ObservableCollection<Project>();
			foreach (var item in ModStoreLoader.ModStoreContent.ProjectItemsCache.GetCacheContent().data)
			{
				var webResult = ModStoreConnector.LoadProjectById(item.Id);
				result.Add(webResult);
			}

			WriteCacheContent(result);
			Content = ReadCache();
		}
	}
}