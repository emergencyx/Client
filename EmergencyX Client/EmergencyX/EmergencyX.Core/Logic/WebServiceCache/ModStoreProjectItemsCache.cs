using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using EmergencyX.Core.Data.Cache;
using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Enum;
using EmergencyX.Core.Networking;
using Newtonsoft.Json;

namespace EmergencyX.Core.Logic.WebServiceCache
{
	public class ModStoreProjectItemsCache : WebServiceCache<ProjectListItem>
	{
		public ModStoreProjectItemsCache(string file)
		{
			CacheFileType = WebServiceCacheFileType.JSON;
			CacheFilePath = file;
			CacheType = WebServiceCacheType.ProjectList;
			Content = ReadCache();
		}

		public override (DateTime time, ObservableCollection<ProjectListItem> data) GetCacheContent()
		{
			Content ??= ReadCache();

			return (Content.LastWrite, Content.Items);
		}

		protected override void WriteCacheContent(ObservableCollection<ProjectListItem> data)
		{
			data ??= new ObservableCollection<ProjectListItem>();

			var dataToWrite = new CacheContent<ProjectListItem>(DateTime.Now, data);
			Content = dataToWrite;
			var text = JsonConvert.SerializeObject(dataToWrite);
			File.WriteAllText(CacheFilePath, text);
		}

		public override void UpdateCache()
		{
			var result = ModStoreConnector.LoadProjects();
			WriteCacheContent(result);
			Content = ReadCache();
		}
	}
}