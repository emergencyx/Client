using System;
using System.Collections.ObjectModel;
using System.IO;
using EmergencyX.Core.Data.Cache;
using EmergencyX.Core.Enum;
using Newtonsoft.Json;

namespace EmergencyX.Core.Logic.WebServiceCache
{
	public abstract class WebServiceCache<T> : BaseObject
	{
		private CacheContent<T> _content;
		private WebServiceCacheType _cacheType;
		private string _cacheFilePath;
		private WebServiceCacheFileType _cacheFileType;

		public WebServiceCacheType CacheType
		{
			get => _cacheType;
			set
			{
				_cacheType = value;
				OnPropertyChanged();
			}
		}

		public string CacheFilePath
		{
			get => _cacheFilePath;
			set
			{
				_cacheFilePath = value;
				OnPropertyChanged();
			}
		}

		public WebServiceCacheFileType CacheFileType
		{
			get => _cacheFileType;
			set
			{
				_cacheFileType = value;
				OnPropertyChanged();
			}
		}

		protected CacheContent<T> Content
		{
			get => _content;
			set
			{
				_content = value;
				OnPropertyChanged();
			}
		}

		protected abstract void WriteCacheContent(ObservableCollection<T> data);

		protected CacheContent<T> ReadCache()
		{
			while (true)
			{
				if (File.Exists(CacheFilePath))
				{
					var value = JsonConvert.DeserializeObject<CacheContent<T>>(
						File.ReadAllText(CacheFilePath));
					if (IsCacheValid(ref value))
					{
						return value;
					}
				}

				UpdateCache();
			}
		}

		public abstract void UpdateCache();
		public abstract (DateTime time, ObservableCollection<T> data) GetCacheContent();

		protected static bool IsCacheValid(ref CacheContent<T> data)
		{
			TimeSpan dataLastWrite = DateTime.Now - data.LastWrite;

			if (dataLastWrite.TotalSeconds > 90 || data.Items.Count == 0)
			{
				return false;
			}

			return true;
		}
	}
}