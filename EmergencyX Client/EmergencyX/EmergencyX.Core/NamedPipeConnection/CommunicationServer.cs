using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Text;
using EmergencyX.Core.Events;
using EmergencyX.Core.OperatingSystems;
using log4net;

namespace EmergencyX.Core.NamedPipeConnection;

public class CommunicationServer
{
    private static readonly ILog Log = LogManager.GetLogger(typeof(CommunicationServer));


    private NamedPipeServerStream _pipeServerStream;
    public string ServerName { get; }


    public CommunicationServer(string serverName)
    {
        if (!Platform.IsWindows)
        {
            throw new NotSupportedException();
        }

        ServerName = serverName;
        PipeServerStream = Create(ServerName);
    }

    ~CommunicationServer()
    {
        PipeServerStream.Close();
        PipeServerStream.Dispose();
    }

    public EventHandler<CommunicationServerMessageReceivedEvent> MessageReceivedHandler;

    protected NamedPipeServerStream PipeServerStream
    {
        get => _pipeServerStream;
        set => _pipeServerStream = value;
    }

    private NamedPipeServerStream Create(string serverName)
    {
        return new NamedPipeServerStream(serverName, PipeDirection.In, 1, PipeTransmissionMode.Message,
            PipeOptions.Asynchronous);
    }

    public void RunClientAsync()
    {
        Log.Debug("Run pipe server");

        PipeServerStream.BeginWaitForConnection(WaitForConnectionCallback,
            PipeServerStream);
    }

    private void WaitForConnectionCallback(IAsyncResult asyncResult)
    {
        Log.Info("Start reading from stream");
        PipeServerStream.EndWaitForConnection(asyncResult);

        var data = new List<string>();
        byte[] buffer = new byte[255];

        do
        {
            var read = PipeServerStream.Read(buffer);
            var stringData = Encoding.UTF8.GetString(buffer, 0, read);
            // apparently a new line is added when sending data through the pipeline, thus .trim()
            data.Add(stringData.Trim());
        } while (!PipeServerStream.IsMessageComplete);

        MessageReceivedHandler.Invoke(this, new CommunicationServerMessageReceivedEvent(data));
        Log.Info("Reached end of message");

        // reset connection to wait for new handler connections
        PipeServerStream.Disconnect();
        PipeServerStream.Close();
        PipeServerStream.Dispose();
        PipeServerStream = null;
        PipeServerStream = Create(ServerName);

        PipeServerStream.BeginWaitForConnection(WaitForConnectionCallback, PipeServerStream);
    }
}