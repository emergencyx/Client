using System;
using System.IO;
using System.IO.Pipes;

namespace EmergencyX.Core.NamedPipeConnection;

public class CommunicationClient
{
    private NamedPipeClientStream _namedPipeClientStream;

    protected NamedPipeClientStream PipeClientStream
    {
        get => _namedPipeClientStream;
        set => _namedPipeClientStream = value;
    }

    public CommunicationClient(string pipeName)
    {
        PipeClientStream = new NamedPipeClientStream(".", pipeName, PipeDirection.Out);
    }

    public void Connect()
    {
        PipeClientStream.Connect(20_000);
    }

    public void Disconnect()
    {
        PipeClientStream.Dispose();
    }

    public bool IsConnected()
    {
        return PipeClientStream.IsConnected;
    }

    public async void SendString(string data)
    {
        await using (StreamWriter writer = new StreamWriter(PipeClientStream))
        {
            await writer.WriteLineAsync(data);

#if DEBUG
            Console.WriteLine("Sent message: " + data);
#endif
        }
    }
}