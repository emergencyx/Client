namespace EmergencyX.Core.NamedPipeConnection;

public class NamedPipeSettings
{
	public const string MAIN_PIPE_NAME = "EmergencyXClientComs";
	public const string MAIN_REGISTRY_KEY_NAME = "emergencyx";
	public const string MAIN_REGISTRY_VALUE_NAME = "URL Protocol";
	public const string MAIN_HANDLER_KEY_NAME = "emergencyx\\shell\\open\\command";
	public const string COMMUNICATION_EXE_NAME = "EmergencyX.Handler.exe";
}