using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using EmergencyX.Core.Converters.Json;
using EmergencyX.Core.Data.NetworkRequest;
using EmergencyX.Core.Logic.Settings;
using Newtonsoft.Json;
using RestSharp;

namespace EmergencyX.Core.Networking
{
	public class ModStoreConnector
	{
		// ToDo: Maybe use phone book agian?
		private static string _getModStoreUrl = "https://europe-west3-emergencyx-prod.cloudfunctions.net/";

		private static string ApiVersion => string.IsNullOrEmpty(Settings.AppSettings.BetaChannel)
			? "api"
			: "api-staging";

		internal static ObservableCollection<ProjectListItem> LoadProjects()
		{
			var client = RestConnectionProvider.GetRestClient(_getModStoreUrl);

			var request = new RestRequest($"/{ApiVersion}/projects");
			request.Method = Method.Get;

			var result = client.Execute(request);

			// if not 200 then throw error
			if (result.StatusCode != HttpStatusCode.OK &&
			    result.StatusCode != HttpStatusCode.InternalServerError)
			{
				throw new Exception($"Http status code {result.StatusCode}");
			}

			if (result.StatusCode != HttpStatusCode.OK &&
			    result.StatusCode == HttpStatusCode.InternalServerError)
			{
				return new ObservableCollection<ProjectListItem>();
			}

			return JsonConvert.DeserializeObject<ObservableCollection<ProjectListItem>>(result.Content);
		}

		internal static Project LoadProjectById(string projectId)
		{
			var client = RestConnectionProvider.GetRestClient(_getModStoreUrl);

			var request = new RestRequest("/"+ApiVersion+"/projects/{project}");
			request.Method = Method.Get;
			request.AddUrlSegment("project", projectId);

			var result = client.Execute(request);

			// if not 200 then throw error
			if (result.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception($"Http status code {result.StatusCode}");
			}

			var setting = new JsonSerializerSettings()
			{
				ContractResolver = new NotRequiredIfNotPresentContractResolver()
			};
			return JsonConvert.DeserializeObject<Project>(result.Content, setting);
		}

		public static void DownloadReleaseForProjectAsync(Artifact download, string target,
			DownloadProgressChangedEventHandler ClientOnDownloadProgressChanged,
			AsyncCompletedEventHandler ClientOnDownloadComplete)
		{
			var client = new WebClient();
			client.Headers.Add("user-agent", Settings.AppSettings.UserAgent);
			var uri = new Uri(download.Href);

			client.DownloadProgressChanged += ClientOnDownloadProgressChanged;
			client.DownloadFileCompleted += ClientOnDownloadComplete;

			client.DownloadFileAsync(uri, target);
		}
	}
}