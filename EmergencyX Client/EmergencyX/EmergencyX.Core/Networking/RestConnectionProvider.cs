using System;
using EmergencyX.Core.Logic.Settings;
using RestSharp;

namespace EmergencyX.Core.Networking
{
	public class RestConnectionProvider
	{
		public static RestClient GetRestClient(string baseUrl)
		{
			if (string.IsNullOrEmpty(baseUrl))
			{
				throw new ArgumentNullException("BaseUrl");
			}

			var client = new RestClient(baseUrl, options =>
			{
				var restClientOptions = new RestClientOptions()
				{
					UserAgent = Settings.AppSettings.UserAgent
				};
			});

			return client;
		}
	}
}