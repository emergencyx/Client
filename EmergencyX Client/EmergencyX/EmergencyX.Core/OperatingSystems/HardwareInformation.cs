using System;
using System.Runtime.Intrinsics.X86;

namespace EmergencyX.Core.OperatingSystems
{
	public class HardwareInformation
	{
		public static (int eax, int ebx, int ecx, int edx) GetCpuId()
		{
			var cpuid = X86Base.CpuId(0, 0);
			return cpuid;
		}
	}
}