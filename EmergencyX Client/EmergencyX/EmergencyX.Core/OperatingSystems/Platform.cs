using System.Runtime.InteropServices;
using log4net;

namespace EmergencyX.Core.OperatingSystems
{
	public static class Platform
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(Platform));
		
		public static bool IsWindows
		{
			get
			{
				Log.Debug(string.Format("OS Windows='{0}'",RuntimeInformation.IsOSPlatform(OSPlatform.Windows)));
				return RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
			}
		}
	}
}