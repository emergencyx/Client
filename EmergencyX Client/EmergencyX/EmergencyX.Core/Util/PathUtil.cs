using System;
using System.IO;

namespace EmergencyX.Core.Util
{
	public class PathUtil
	{
		private const string EMX_DIRECTORY = "EmergencyX";

		public static string GetEmxAppDataPath()
		{
			var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			appData = Path.GetFullPath(Path.Combine(appData, EMX_DIRECTORY));

			if (!Directory.Exists(appData))
			{
				Directory.CreateDirectory(appData);
			}

			return appData;
		}

		public static string GetLast30CharsOfFileName(string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				return String.Empty;
			}

			if (name.Length > 30)
			{
				return name.Substring(name.Length - 30);
			}

			return name;
		}
	}
}