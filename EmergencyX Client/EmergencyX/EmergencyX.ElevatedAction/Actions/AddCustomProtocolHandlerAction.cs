using System.Reflection;
using EmergencyX.Core.NamedPipeConnection;
using EmergencyX.ElevatedAction.Types;
using EmergencyX.Windows.Enum;
using EmergencyX.Windows.Logic;

namespace EmergencyX.ElevatedAction.Actions;

public class AddCustomProtocolHandlerAction : IElevatedAction
{
    private ActionTypes _type;
    private int _exitCode = 0;
    private Exception? _exception;

    public ActionTypes Type
    {
        get => _type;
        set => _type = value;
    }

    public int ExitCode
    {
        get => _exitCode;
        set => _exitCode = value;
    }

    public Exception? CurrentException
    {
        get => _exception;
        set => _exception = value;
    }

    public AddCustomProtocolHandlerAction()
    {
        Type = ActionTypes.AddCustomProtocolHandlerCommand;
    }

    public bool Execute()
    {
        var path = Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ??
            throw new InvalidOperationException(),
            NamedPipeSettings.COMMUNICATION_EXE_NAME);

        path = string.Format(@"""{0}"" ""%1""", path);

        try
        {
            RegistryAccess.AddRegistryKeyFromSubKey(NamedPipeSettings.MAIN_REGISTRY_KEY_NAME,
                SubKeys.CurrentClassesRoot);
            RegistryAccess.AddRegistryValueToSubkey(NamedPipeSettings.MAIN_REGISTRY_KEY_NAME,
                SubKeys.CurrentClassesRoot,
                new KeyValuePair<string, string>(NamedPipeSettings.MAIN_REGISTRY_VALUE_NAME, ""));
            RegistryAccess.AddRegistryKeyFromSubKey(NamedPipeSettings.MAIN_HANDLER_KEY_NAME,
                SubKeys.CurrentClassesRoot);
            RegistryAccess.AddRegistryValueToSubkey(NamedPipeSettings.MAIN_HANDLER_KEY_NAME,
                SubKeys.CurrentClassesRoot, new KeyValuePair<string, string>("", path));
        }
        catch (Exception? e)
        {
            CurrentException = e;
            ExitCode = -2;
            Console.WriteLine("Elevated action {0} failed. Error message is: {1}",
                nameof(AddCustomProtocolHandlerAction), e.Message);
            return false;
        }

        ExitCode = 0;
        return true;
    }
}