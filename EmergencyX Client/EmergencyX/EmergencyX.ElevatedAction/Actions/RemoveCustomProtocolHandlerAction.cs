using EmergencyX.Core.NamedPipeConnection;
using EmergencyX.ElevatedAction.Types;
using EmergencyX.Windows.Enum;
using EmergencyX.Windows.Logic;

namespace EmergencyX.ElevatedAction.Actions;

public class RemoveCustomProtocolHandlerAction : IElevatedAction
{
    private ActionTypes _type;
    private int _exitCode = 0;
    private Exception? _exception;

    public ActionTypes Type
    {
        get => _type;
        set => _type = value;
    }

    public int ExitCode
    {
        get => _exitCode;
        set => _exitCode = value;
    }

    public Exception? CurrentException
    {
        get => _exception;
        set => _exception = value;
    }

    public RemoveCustomProtocolHandlerAction()
    {
        Type = ActionTypes.RemoveCustomProtocolHandlerCommand;
    }

    public bool Execute()
    {
        try
        {
            RegistryAccess.RemoveRegistryKeyTreeFromSubKey(NamedPipeSettings.MAIN_REGISTRY_KEY_NAME,
                SubKeys.CurrentClassesRoot);
        }
        catch (Exception? e)
        {
            CurrentException = e;
            ExitCode = -3;
            Console.WriteLine("Elevated action {0} failed. Error message is: {1}",
                nameof(RemoveCustomProtocolHandlerAction), e.Message);
            return false;
        }

        ExitCode = 0;
        return true;
    }
}