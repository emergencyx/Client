﻿// See https://aka.ms/new-console-template for more information

using EmergencyX.ElevatedAction.Actions;
using EmergencyX.ElevatedAction.Types;

var actionString = args[0];
IElevatedAction action;

switch (actionString)
{
    case nameof(ActionTypes.AddCustomProtocolHandlerCommand):
        action = new AddCustomProtocolHandlerAction();
        break;
    case nameof(ActionTypes.RemoveCustomProtocolHandlerCommand):
        action = new RemoveCustomProtocolHandlerAction();
        break;
    default:
        action = null;
        break;
}

if (action == null)
{
    Console.WriteLine("Found no appropriate elevated action");
    return 111;
}

Console.WriteLine("Running elevated action...");

action.Execute();

Thread.Sleep(5000);

return action.ExitCode;