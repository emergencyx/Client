namespace EmergencyX.ElevatedAction.Types;

public enum ActionTypes
{
	RemoveCustomProtocolHandlerCommand,
	AddCustomProtocolHandlerCommand
}