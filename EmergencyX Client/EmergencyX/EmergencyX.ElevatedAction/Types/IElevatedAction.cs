namespace EmergencyX.ElevatedAction.Types;

public interface IElevatedAction
{
	ActionTypes Type { get; set; }
	int ExitCode { get; set; }
	Exception? CurrentException { get; set; }

	bool Execute();
}