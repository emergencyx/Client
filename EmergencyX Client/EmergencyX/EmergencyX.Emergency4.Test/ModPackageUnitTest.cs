using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using EmergencyX.Emergency4.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EmergencyX.Emergency4.Test
{
    [TestClass]
    public class ModPackageUnitTest
    {
        public const string TestFilePath = "TestData/schweden.e4mod";

        public static readonly Tuple<string, int, string> VerificationFileSpec = Tuple.Create(
            "readme.pdf", 180278, "3FB68696BA3252BED43C4DE4B27849E66C1F286E72011392CBF7AD72BF7260A1"
        );

        [TestMethod]
        public async Task ShouldReturnCorrectProjectInformationAsync()
        {
            var package = await GetTestPackageAsync();

            Assert.AreEqual(119, package.Files.Count);
            Assert.AreEqual("Schweden", package.Name);

            Assert.IsNotNull(package.Info);
            Assert.AreEqual("Schweden Modifikation", package.Info.Name);
        }

        [TestMethod]
        public async Task ShouldExtractFilesProperlyAsync()
        {
            var package = await GetTestPackageAsync();

            // Check if the verification file we have pinned above exists
            var verificationFile = package.Files.First(f => f.Name == VerificationFileSpec.Item1);
            Assert.IsNotNull(verificationFile, "package.Files did not contain the verification file {0}",
                VerificationFileSpec.Item1);

            // Check size
            var expectedFileSize = VerificationFileSpec.Item2;
            Assert.AreEqual(expectedFileSize, verificationFile.DataSize, "Expected file size to be {0} but was {1}",
                expectedFileSize, verificationFile.DataSize);

            // Check content
            var expectedHash = VerificationFileSpec.Item3;

            await using var fileStream = await package.GetDecompressedStreamAsync(verificationFile);
            var bytes = fileStream.ToArray();
           
            DebugFileLayout(package);

            var actualHash = GetHashFromByteArray(bytes);
            Assert.AreEqual(expectedHash, actualHash);
        }

        private static async Task<ModPackage> GetTestPackageAsync()
        {
            var inputFile = new FileInfo(TestFilePath);
            Assert.IsTrue(inputFile.Exists, "Test input file {0} does not exist.", TestFilePath);

            var package = await ModPackageExtractor.ReadPackageAsync(inputFile);
            Assert.IsNotNull(package);

            return package;
        }

        private static string GetHashFromByteArray(byte[] bytes)
        {
            var hash = SHA256.HashData(bytes);
            return BitConverter.ToString(hash).Replace("-", string.Empty);
        }

        private static void DebugFileLayout(ModPackage package)
        {
            // Sort by DataOffset for nicer representation before printing
            var files = package.Files.OrderBy(f => f.DataOffset);
            Debug.WriteLine("Offset     Size       Name");
            foreach (var file in files)
                Debug.WriteLine("{0,10} {1,10} {2}", file.DataOffset, file.DataSize, file.Name);
        }
    }
}