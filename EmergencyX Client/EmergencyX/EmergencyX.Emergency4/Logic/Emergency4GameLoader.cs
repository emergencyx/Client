using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Windows;
using EmergencyX.Windows.Enum;
using EmergencyX.Windows.Logic;
using EmergencyX.Windows.Logic.EmergencyX.Windows.Tools;
using log4net;
using Standart.Hash.xxHash;

namespace EmergencyX.Emergency4.Logic
{
	public class Emergency4GameLoader : IGameLoader
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(Emergency4GameLoader));

		public static DirectoryInfo GetEmergency4ModsPath =>
			new DirectoryInfo(Path.Combine(LoadEmergencyInstallationPath(), "Mods\\"));

		public static FileInfo GetEmergency4LogFile =>
			new FileInfo(Path.Combine(LoadEmergencyInstallationPath(), "logfile.txt"));

		private static int SteamAppId { get; set; } = 0;

		public static bool IsUsingSteamInstallation =>
			Emergency4GameLoader.SteamAppId != 0 &&
			string.IsNullOrEmpty(Settings.AppSettings.Emergency4ManualPath);

		public static void RunEmergency4(string modString = null)
		{
			ExecuteEmergency4(false, modString);
		}

		public static void RunEmergency4Editor()
		{
			ExecuteEmergency4(true, String.Empty);
		}

		private static void ExecuteEmergency4(bool runEditor, string modString)
		{
			string path = string.Empty;
			if (!IsUsingSteamInstallation)
			{
				// start "normal" installation
				path = Path.GetFullPath(Path.Combine(LoadEmergencyInstallationPath(), "Em4.exe"));
				Log.Info($"Starting EM4 using {path}");

				if (!Directory.Exists(Path.GetDirectoryName(path)))
				{
					throw new DirectoryNotFoundException();
				}

				if (!path.EndsWith("Em4.exe"))
				{
					throw new FileNotFoundException();
				}

				var startOptions = new ProcessStartInfo(path);
				startOptions.WorkingDirectory = Path.GetDirectoryName(path) ?? string.Empty;
				startOptions.UseShellExecute = true;

				if (runEditor)
				{
					startOptions.Arguments = " -editor";
				}
				else if (string.IsNullOrEmpty(modString))
				{
					startOptions.Arguments = " -game";
				}
				else
				{
					if (modString.Contains('&'))
					{
						modString = modString.Replace('&', ' ');
					}

					startOptions.Arguments = string.Format(" -game -mod \"{0}\"", modString);
				}

				Process.Start(startOptions);

				return;
			}

			// start Steam EM4
			var steam = Steam.GetSteamPath();
			path = Path.GetFullPath(Path.Combine(steam, Steam.STEAM_EXE));

			var option = new ProcessStartInfo(path);
			option.WorkingDirectory = steam;
			string argument = string.Empty;

			if (runEditor)
			{
				argument = string.Format("-applaunch {0} -editor", SteamAppId);
			}
			else if (string.IsNullOrEmpty(modString))
			{
				argument = string.Format("-applaunch {0}", SteamAppId);
			}
			else
			{
				if (modString.Contains('&'))
				{
					modString = modString.Replace('&', ' ');
				}

				argument = string.Format("-applaunch {0} -mod \"{1}\"", SteamAppId, modString);
			}

			option.Arguments = argument;
			Log.Info($"Steam start using args: {argument}");

			Process.Start(option);
		}

		public static string LoadEmergencyInstallationPath()
		{
			if (!string.IsNullOrEmpty(Settings.AppSettings.Emergency4ManualPath))
			{
				return Settings.AppSettings.Emergency4ManualPath;
			}

			// Emergency 4 Deluxe Steam
			var result = Steam.GetSteamGamePath(757210);
			if (!string.IsNullOrEmpty(result))
			{
				SteamAppId = 757210;
				return result;
			}

			// 911: First Responders
			result = Steam.GetSteamGamePath(323610);
			if (!string.IsNullOrEmpty(result))
			{
				SteamAppId = 323610;
				return result;
			}

			// Disc
			result = GetEmergency4PathFromRegistry();

			return !Directory.Exists(result) ? String.Empty : result;
		}

		private static string GetEmergency4PathFromRegistry()
		{
			// Try and test different Emergency 4 versions, this only works tho if the current user did actually install the software
			var value = RegistryAccess.GetRegistryValueFromSubKey(
				@"Software\sixteen tons entertainment\Emergency 4", SubKeys.CurrentUser,
				"InstallDir");

			if (!string.IsNullOrEmpty(value))
			{
				Log.Debug("Found EM4 from Registry via Current user (standard)");
				return value;
			}

			value = RegistryAccess.GetRegistryValueFromSubKey(
				@"Software\sixteen tons entertainment\Emergency 4 Gold", SubKeys.CurrentUser,
				"InstallDir");

			if (!string.IsNullOrEmpty(value))
			{
				Log.Debug("Found EM4 from Registry via Current user (Gold)");
				return value;
			}

			value = RegistryAccess.GetRegistryValueFromSubKey(
				@"Software\sixteen tons entertainment\Emergency 4 DELUXE", SubKeys.CurrentUser,
				"InstallDir");

			if (!string.IsNullOrEmpty(value))
			{
				Log.Debug("Found EM4 from Registry via Current user (Deluxe)");
				return value;
			}

			//at this point we got one trick left: Check the ModInstaller
			value = RegistryAccess.GetRegistryValueFromSubKey(
				@"SOFTWARE\Classes\e4modFile\DefaultIcon", SubKeys.LocalMachine,
				null);

			if (!string.IsNullOrEmpty(value))
			{
				value = value.Split("\",")[0].Replace("\"", "").Replace("ModInstaller.exe", "");
				if (Directory.Exists(value))
				{
					Log.Debug("Found EM4 from Registry via Local Machine");
					return value;
				}
			}

			Log.Error("Unable to determine installation path of Emergency 4. Please use manual path.");
			return String.Empty;
		}

		public static string GetEmergency4VersionOrGuess(DirectoryInfo directory)
		{
			try
			{
				var executable = directory.EnumerateFiles()
					.First(f => f.Name.EndsWith("4.exe") && f.Length < 10_000_000);
				using var stream = executable.OpenRead();
				var hash = xxHash64.ComputeHash(stream);
				Log.Info(
					$"Found Em4 installation {executable.Name} in {directory.FullName} with hash {hash}");
				switch (hash)
				{
					case 2022204605566823414:
					case 12429817427584750790:
					case 13501542384404721935:
					case 18295554646828052048:
						return "1.3.f";
					default:
						Log.Info(
							"Unknown Emergency 4 version - if you want, report this hash with the corresponding game version so we can update the database");
						return hash.ToString(); // Motivate to report this
				}
			}
			catch (InvalidOperationException e)
			{
				Log.Error("Failed to fetch Emergency 4 installation", e);
			}

			return "???";
		}

		public static void OpenEmergency4InstallDir()
		{
			var path = Path.GetFullPath(LoadEmergencyInstallationPath());
			var startCommand = new ProcessStartInfo("explorer.exe", path);

			Log.Debug($"Path is {path}");

			if (Platform.IsWindows)
			{
				Process.Start(startCommand);
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		public static bool IsEmergency4Running()
		{
			var processList = Process.GetProcesses();
			return processList.Count(x => x.ProcessName.ToLowerInvariant() == "em4") == 1;
		}
	}
}