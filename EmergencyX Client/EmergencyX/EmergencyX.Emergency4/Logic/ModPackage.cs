using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using log4net;

namespace EmergencyX.Emergency4.Logic
{
	public struct FileEntry
	{
		public int DataOffset;
		public int DataSize;
		public string Name;
		public string Path;
	}

	public struct ModInfo
	{
		public string Name { get; set; }
		public string Author { get; set; }
		public string Comment { get; set; }
	}

	public class ModPackage
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(ModPackage));

		private readonly BinaryReader _binaryReader;

		public ModPackage(BinaryReader binaryReader, List<FileEntry> files, string name)
		{
			_binaryReader = binaryReader;
			Files = files;
			Name = name;
		}

		public List<FileEntry> Files { get; }

		public string Name { get; }

		public ModInfo Info { get; set; }

		public async Task<byte[]> GetDecompressedBytesAsync(FileEntry file)
		{
			await using var reader = await GetDecompressedStreamAsync(file);
			return reader.ToArray();
		}

		public async Task<MemoryStream> GetDecompressedStreamAsync(FileEntry file)
		{
			_binaryReader.BaseStream.Seek(file.DataOffset, SeekOrigin.Begin);
			var output = new MemoryStream(file.DataSize);

			while (true)
			{
				var compressedSize = _binaryReader.ReadInt32();
				var decompressedSize = _binaryReader.ReadInt32();
				var buffer = _binaryReader.ReadBytes(compressedSize);
				await using var memory = new MemoryStream(buffer, false);
				await using var zlib = new InflaterInputStream(memory, new Inflater());

				var decompressedBuffer = new byte[decompressedSize];
				var read = await zlib.ReadAsync(decompressedBuffer, 0, decompressedSize);
				Debug.Assert(read == decompressedSize);
				
				await output.WriteAsync(decompressedBuffer, 0, read);
				
				if (decompressedSize == 0xffff)
				{
					// Emergency 4 uses a 0xffff sized buffer when compressing files
					// and these blocks will be appended after each other with
					// compressedSize and decompressedSize serialized between
					// them. If the decompressed size equals 0xffff we assume
					// that another block must follow.
					continue;
				}

				break;
			}

			output.Seek(0, SeekOrigin.Begin);
			return output;
		}
		
		public async Task ExtractAsync(DirectoryInfo modDirectory, IProgress<int> progress = null)
		{
			var installBase = modDirectory.CreateSubdirectory(Name).FullName;
			var batchSize = (int) Math.Floor(Files.Count / 100.0f);
			var processedFiles = 0;

			Log.Debug($"Extract {Name} to \"{installBase}\"");

			foreach (var entry in Files)
			{
				var destination = Path.Combine(installBase, entry.Path);
				var file = new FileInfo(destination);
				Debug.Assert(file.Directory != null,
					"d.Directory != null"); // TODO: Properly check this
				if (!file.Directory.Exists)
				{
					file.Directory.Create();
				}

				await using var fileStream = file.Create();
				await using var decompressorStream = await GetDecompressedStreamAsync(entry);
				await decompressorStream.CopyToAsync(fileStream);

				Debug.Assert(entry.DataSize == fileStream.Length, "Written file stream has unexpected length.");
				
				processedFiles++;
				if (processedFiles % batchSize == 0)
				{
					progress?.Report((int) (100f * processedFiles / Files.Count));
				}
			}

			Log.Debug($"Extracted {processedFiles} files");
		}
	}
}