using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using EmergencyX.Emergency4.Logic.Modifications;
using log4net;

namespace EmergencyX.Emergency4.Logic
{
	public class ModPackageExtractor
	{
		private static readonly Encoding Windows1252Encoding;
		public static readonly ILog Log =
			LogManager.GetLogger(typeof(ModPackageExtractor));

		static ModPackageExtractor()
		{
			Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
			Windows1252Encoding = Encoding.GetEncoding(1252);
		}

		public static async Task<ModPackage> ReadPackageAsync(FileInfo fileInfo)
		{
			if (!fileInfo.Exists)
			{
				throw new Exception("Cannot read package from file that does not exist");
			}

			var reader = new BinaryReader(fileInfo.OpenRead());

			// Header
			var header = Encoding.ASCII.GetString(reader.ReadBytes(5));
			var version = reader.ReadInt32(); //should be 257

			// Package info
			var nameLength = reader.ReadInt32();
			var name = Windows1252Encoding.GetString(reader.ReadBytes(nameLength)).TrimEnd('\0');

			var files = CreateStructure(reader, "");
			var package = new ModPackage(reader, files, name);
			package.Info = await ExtractModInfoAsync(package);

			return package;
		}

		private static List<FileEntry> CreateStructure(BinaryReader f, string prefix)
		{
			var directoryCount = f.ReadInt32();
			var directories = new List<string>(directoryCount);
			var files = new List<FileEntry>();

			for (var i = 0; i < directoryCount; i++)
			{
				var length = f.ReadInt32();
				var directoryName = Windows1252Encoding.GetString(f.ReadBytes(length));
				directories.Add(directoryName);
			}

			var fileCount = f.ReadInt32();
			for (var i = 0; i < fileCount; i++)
			{
				var length = f.ReadInt32();
				var fileName = Windows1252Encoding.GetString(f.ReadBytes(length)).TrimEnd('\0');
				var entry = new FileEntry
				{
					Name = fileName,
					DataOffset = f.ReadInt32(),
					DataSize = f.ReadInt32(),
					Path = Path.Combine(prefix, fileName)
				};
				files.Add(entry);
			}

			foreach (var _ in directories)
			{
				var length = f.ReadInt32();
				var directory = Windows1252Encoding.GetString(f.ReadBytes(length)).TrimEnd('\0');
				var filesInDirectory = CreateStructure(f, Path.Combine(prefix, directory));
				files.AddRange(filesInDirectory);
			}

			return files;
		}

		private static async Task<ModInfo> ExtractModInfoAsync(ModPackage package)
		{
			try
			{
				var modInfo = package.Files.Find(entry =>
					entry.Name == Emergency4Modification.E4MOD_INFO);
				// <mod name="Bieberfelde Modifikation 1.0" author="BFEMP-Team" comment="Support unter www.bfemp.de" major="1" minor="3" />
				// <mod name="MGBZ Greifszoll Bhf release 1.0" author="Landmark Studios" comment="Erlebt den Alltag am Bahnhof Bad Greifszoll" major="1" minor="1" />
				// <mod name="Blubb" author="Unknown" major="1" minor="3" />

				var content = await package.GetDecompressedBytesAsync(modInfo);
				var xml = Encoding.Unicode.GetString(content);
				return InfoFromXml(xml);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return new ModInfo {Author = "", Comment = "", Name = package.Name};
			}
		}

		public static ModInfo InfoFromXml(string xml)
		{
			XElement root;

			try
			{
				var doc = XDocument.Parse(xml.Trim('\uFEFF', '\u200B', '\0'));
				root = doc.Element("e4mod")?.Element("mod");
			}
			catch (Exception)
			{
				// Try to recover from invalid XML once
				xml = Regex.Replace(xml, "&(?!(amp|apos|quot|lt|gt);)", "&amp;");
				var doc = XDocument.Parse(xml.Trim('\uFEFF', '\u200B', '\0'));
				root = doc.Element("e4mod")?.Element("mod");
			}

			if (root == null || !root.HasAttributes)
			{
				throw new InvalidDataException("Missing or malformed XML in e4mod.info");
			}

			return new ModInfo
			{
				Author = root.Attribute("author")?.Value,
				Comment = root.Attribute("comment")?.Value,
				Name = root.Attribute("name")?.Value
			};
		}
	}
}