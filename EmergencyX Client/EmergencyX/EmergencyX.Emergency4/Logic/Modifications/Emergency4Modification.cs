using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EmergencyX.Core;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic;

namespace EmergencyX.Emergency4.Logic.Modifications
{
	public class Emergency4Modification : BaseObject, IGameModification
	{
		public const string E4MOD_INFO = "e4mod.info";
		public const string E4MOD_DDS = "e4mod.dds";
		private const string E4VERSION_REGEX = @"[0-9]{1,}\.[0-9]{1,}(\.[0-9]{1,}\.[0-9]{1,})?";

		public string Name { get; set; }
		public string Author { get; set; }
		public string Description { get; set; }
		public string DataPath { get; set; }

		public string GetModIconName
		{
			get
			{
				if (Directory.Exists(DataPath))
				{
					var path = Path.Combine(DataPath, E4MOD_DDS);
					if (File.Exists(path))
					{
						return path;
					}
				}

				return "";
			}
		}

		public Emergency4Modification()
		{
		}

		public Emergency4Modification(IGameModification modification)
		{
			Name = modification.Name;
			Author = modification.Author;
			Description = modification.Description;
			DataPath = modification.DataPath;
		}

		public string TryGetVersion()
		{
			string versionNumber;
			var versionMatcher = new Regex(E4VERSION_REGEX);

			versionNumber = TryGetVersionFromName(ref versionMatcher);

			if (!string.IsNullOrEmpty(versionNumber))
			{
				return versionNumber;
			}

			versionNumber = TryGetVersionFromDescription(ref versionMatcher);

			return versionNumber;
		}

		private string TryGetVersionFromName(ref Regex versionMatcher)
		{
			return versionMatcher.Match(Name).Value;
		}

		private string TryGetVersionFromDescription(ref Regex versionMatcher)
		{
			return versionMatcher.Match(Description).Value;
		}

		public string GetProjectFileHash()
		{
			var infoPath = Path.Combine(DataPath, E4MOD_INFO);
			return ContentIdentification.GetSha512(infoPath);
		}
	}
}