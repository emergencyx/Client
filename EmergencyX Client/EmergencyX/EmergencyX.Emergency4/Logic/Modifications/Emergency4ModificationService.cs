using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EmergencyX.Core.Events;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.OperatingSystems;
using log4net;

namespace EmergencyX.Emergency4.Logic.Modifications
{
	public class Emergency4ModificationService : AbstractModificationService
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(Emergency4ModificationService));

		public static List<IGameModification> FromModsDirectory(DirectoryInfo modsDirectory)
		{
			Log.Debug($"Scan {modsDirectory.FullName} for modifications");
			var mods = new List<IGameModification>();
			foreach (var directory in modsDirectory.EnumerateDirectories())
			{
				var info = new FileInfo(Path.Combine(directory.FullName,
					Emergency4Modification.E4MOD_INFO));
				if (!info.Exists)
				{
					continue;
				}

				try
				{
					using var reader = new StreamReader(info.OpenRead(), Encoding.Unicode, true);
					var content = reader.ReadToEnd();
					var modInfo = ModPackageExtractor.InfoFromXml(content);
					mods.Add(new Emergency4Modification
					{
						Author = modInfo.Author,
						Name = modInfo.Name,
						DataPath = directory.FullName,
						Description = modInfo.Comment
					});
					Log.Debug($"Found modification {modInfo.Name} in \"{directory.FullName}\"");
				}
				catch (XmlException e)
				{
					Log.Warn($"Failed to read metadata from {info.FullName}", e);
				}
			}

			return mods;
		}

		public static void Uninstall(IGameModification modification)
		{
			if (!(modification is Emergency4Modification))
			{
				return;
			}

			Log.Info($"Uninstall {modification.Name} at \"{modification.DataPath}\"");
			var directory = new DirectoryInfo(modification.DataPath);
			if (!directory.Exists) throw new Exception("Directory not found");
			if (directory.Parent == null || directory.Parent.Name.ToLower() != "mods")
			{
				Log.Error("Refuse to uninstall, not a save path");
				throw new Exception("Bad parent directory - must be named mods");
			}

			directory.Delete(true);
			Log.Info("Uninstall completed");
		}

		/// <summary>
		/// Installs .e4mod files and .zip files
		/// </summary>
		/// <param name="files">a list of files that should be installed</param>
		/// <param name="modsDirectory">a DirecotryInfo of the mods directory</param>
		public async Task InstallAsync(IEnumerable<string> files, DirectoryInfo modsDirectory)
		{
			foreach (var file in files.Where(name => name.EndsWith(".e4mod") || name.EndsWith(".zip")))
			{
				var mod = new FileInfo(file);
				if (!mod.Exists)
				{
					continue;
				}

				Log.Debug($"Trying to install {mod.Name}");

				var progress = new Progress<int>(update =>
				{
					OnModificationInstallationProgress(new InstallationProgressEventArgs
						{ Progress = update });
				});

				if (mod.Extension == ".e4mod")
				{
					await Task.Run(() => InstallAsync(mod, modsDirectory, progress));
				}

				if (mod.Extension == ".zip")
				{
					await Task.Run(() => { InstallModificationZipAsync(mod.FullName, progress); });
				}
			}

			OnModificationInstallationEndedEvent(new ModificationInstallationEndEventArgs(false));
		}

		private void InstallModificationZipAsync(string mod, IProgress<int> progress)
		{
			string extractPath = Path.GetFullPath(Emergency4GameLoader.GetEmergency4ModsPath.FullName);

			if (extractPath == null)
			{
				throw new ArgumentNullException();
			}

			// this is needed later but needs to be  declared outside of the using statement
			string keyFilePath = String.Empty;

			Encoding encode;

			if (Settings.AppSettings.UseLegacyDecompression)
			{
				// windows legacy encoding for unzipping
				encode = Encoding.GetEncoding(437);
			}
			else
			{
				encode = Encoding.Default;
			}

			using (Stream s = new FileStream(mod, FileMode.Open))
			using (ZipArchive archive = new ZipArchive(s, ZipArchiveMode.Read, true, encode))
			{
				if (archive.Entries.Count(x =>
					    x.Name.Equals(Emergency4Modification.E4MOD_INFO)) != 1)
				{
					Log.Error($"Invalid mod archive");
					throw new NotSupportedException("Invalid Emergency 4 archive");
				}

				// get this archiv entry for the e4mod.info file because it is needed several times
				var e4modInfoEntry = archive.Entries.Single(x =>
					x.Name.Equals(Emergency4Modification.E4MOD_INFO));

				if (e4modInfoEntry.Name == e4modInfoEntry.FullName)
				{
					// this means the ZIP contains no subfolder
					Log.Error($"Invalid mod archive, mod is not in a subfolder");
					throw new NotSupportedException(
						"Invalid Emergency 4 archive, mod is not in a subfolder");
				}

				keyFilePath = e4modInfoEntry.FullName.Replace(e4modInfoEntry.Name, "");
				if (keyFilePath.Count(x => x == '/') > 1 || keyFilePath.Count(x => x == '\\') > 1)
				{
					// this means that the e4mod.info is probably in a sub sub directory (eg BFEMP\Audio\e4mod.info)
					Log.Error($"Invalid mod archive, e4mod.info file is not in the correct folder");
					throw new NotSupportedException(
						"Invalid Emergency 4 archive, e4mod.info file is not in the correct folder");
				}

				keyFilePath = Path.Combine(extractPath, keyFilePath);

				string infoConent = String.Empty;

				using (Stream infoStream = e4modInfoEntry.Open())
				using (StreamReader streamReader = new StreamReader(infoStream))
				{
					infoConent = streamReader.ReadToEnd();
				}

				string modName = string.Empty;

				if (!string.IsNullOrEmpty(infoConent))
				{
					modName = ModPackageExtractor.InfoFromXml(infoConent).Name;
				}

				OnModificationInstallationEventStart(new ModificationInstallationStartedEventArgs()
					{ ModName = modName, NoOfFiles = archive.Entries.Count });

				var counter = 1;

				foreach (ZipArchiveEntry entry in archive.Entries)
				{
					var entryFull = entry.FullName;
					var entryDestination = Path.GetFullPath(Path.Combine(extractPath, entryFull));

					if (entry.Name.Length == 0)
					{
						if (!Directory.Exists(entryDestination))
						{
							Directory.CreateDirectory(entryDestination);
						}

						continue;
					}

					// some mods include the thumbs.db of windows
					if (entry.Name.ToLower() == "thumbs.db")
					{
						continue;
					}

					entry.ExtractToFile(entryDestination, true);

					if (progress != null)
					{
						progress.Report(counter);
					}

					counter++;
				}

				archive.Dispose();
			}

			GenerateE4ModKey(keyFilePath);
		}

		private async Task InstallAsync(FileInfo mod, DirectoryInfo modsDirectory, IProgress<int> progress)
		{
			var package = await ModPackageExtractor.ReadPackageAsync(mod);
			OnModificationInstallationEventStart(new ModificationInstallationStartedEventArgs
				{ ModName = package.Info.Name, NoOfFiles = 100 });
			await package.ExtractAsync(modsDirectory, progress);
			GenerateE4ModKey(Path.Combine(modsDirectory.FullName, package.Name));
		}

		private void GenerateE4ModKey(string path)
		{
			// lastly - create the e4mod.key file that "verifies" the validity of the modification
			var cpuid = HardwareInformation.GetCpuId();
			var em4keyGenerator = new KeyGenerator((uint)cpuid.eax, (uint)cpuid.ebx, (uint)cpuid.ecx,
				(uint)cpuid.edx, Environment.MachineName);
			var key = em4keyGenerator.Generate();
			em4keyGenerator.WriteKeyFile(key, path);
		}

		public static async Task<string> GenerateModificationZip(string path)
		{
			var currentMod = FromModsDirectory(Emergency4GameLoader.GetEmergency4ModsPath)
				.Single(m => m.DataPath == path);

			var fileName =
				string.Join("_", currentMod.Name.Split(Path.GetInvalidFileNameChars())) + "_" +
				DateTime.Now.ToString("yyyy-MM-dd");

			var zipPath = Path.Combine(path, "..", fileName);
			
			int c = 1;
			while (File.Exists(zipPath))
			{
				zipPath = Path.Combine(path, "..", $"{c.ToString()}_{fileName}.zip");
				c++;
			}
			
			await Task.Run(() =>
				ZipFile.CreateFromDirectory(path, zipPath, CompressionLevel.Optimal, true));

			return zipPath;
		}

		#region EventHandler

		public override void OnModificationInstallationEventStart(ModificationInstallationStartedEventArgs e)
		{
			ModificationInstallationStartedEvent?.Invoke(this, e);
		}

		public override void OnModificationInstallationProgress(InstallationProgressEventArgs e)
		{
			ModificationInstallationProgressEvent?.Invoke(this, e);
		}

		public override void OnModificationInstallationEndedEvent(ModificationInstallationEndEventArgs e)
		{
			ModificationEndedEvent?.Invoke(this, e);
		}

		#endregion
	}
}