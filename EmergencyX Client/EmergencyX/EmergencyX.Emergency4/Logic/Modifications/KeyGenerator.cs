using System;
using System.Diagnostics;
using System.IO;

namespace EmergencyX.Emergency4.Logic.Modifications
{
	public class KeyGenerator
	{
		private readonly string _computerName;
		private readonly uint _eax;
		private readonly uint _ebx;
		private readonly uint _ecx;
		private readonly uint _edx;
		private readonly uint[] _seeds;

		public KeyGenerator(uint eax, uint ebx, uint ecx, uint edx, string computerName)
		{
			_seeds = new uint[4];
			_eax = eax;
			_ebx = ebx;
			_ecx = ecx;
			_edx = edx;
			_computerName = computerName;
		}

		public uint Generate()
		{
			AddSeed(_eax);
			AddSeed(_ebx);
			AddSeed(_edx); // The original implementation changes the order of ECX and EDX,
			AddSeed(_ecx); // we account for this by changing the order here
			AddSeed(_computerName);
			return Transform();
		}

		public void WriteKeyFile(uint key, string path)
		{
			var fileInfo = new FileInfo(Path.Combine(path, "e4mod.key"));

			// some mods deliver the .key file in the ZIP package
			// but the key is computer depended, so lets throw it away
			if (fileInfo.Exists)
			{
				fileInfo.Delete();
			}

			using var keyStream = fileInfo.Create();
			using var binaryWriter = new BinaryWriter(keyStream);
			binaryWriter.Write(BitConverter.GetBytes(key));
		}

		private void AddSeed(uint value)
		{
			_seeds[0] += value;
			Transform();
		}

		private void AddSeed(string value)
		{
			foreach (var c in value.ToCharArray())
			{
				_seeds[0] += c;
				Transform();
			}
		}

		private uint Transform()
		{
			Debug.WriteLine("A {0,8:x} {1,8:x} {2,8:x} {3,8:x}", _seeds[0], _seeds[1], _seeds[2],
				_seeds[3]);
			var r = 0xcb72b0f5;
			r += 0x185d9e6d * (_seeds[0] ^ 0xc7b347cf);
			_seeds[0] = _seeds[1];
			r += 0x019e7f31 * (_seeds[1] ^ 0x37cacc44);
			_seeds[1] = _seeds[2];
			r += 0xe724da75 * (_seeds[2] ^ 0xc37bd473);
			_seeds[2] = _seeds[3];
			r += 0xd61ca29d * (_seeds[3] ^ 0x1dc1b026);
			r = (r << 1) | (r >> 31);
			_seeds[3] = r;
			Debug.WriteLine("X {0,8:x} {1,8:x} {2,8:x} {3,8:x}", _seeds[0], _seeds[1], _seeds[2],
				_seeds[3]);
			return r;
		}
	}
}