namespace EmergencyX.Emergency5.Data.Application
{
	public class Application
	{
		public string ApplicationName { get; set; }
		public string ApplicationVersion { get; set; }
		public string ApplicationTags { get; set; }
		public string OrganizationName { get; set; }
		public string OrganizationDomain { get; set; }
		public string SplashScreenInfoTexts { get; set; }
		public string Plugins { get; set; }
		public string Projects { get; set; }
		public string CurrentProject { get; set; }
		public string UserDirectory { get; set; }
		public string CacheDirectory { get; set; }
		public string ApplicationClassName { get; set; }
		public Parameters Parameters { get; set; }
	}
}