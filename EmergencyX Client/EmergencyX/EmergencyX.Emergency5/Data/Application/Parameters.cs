namespace EmergencyX.Emergency5.Data.Application
{
	public class Parameters
	{
		public string SkipStartMenu { get; set; }

		public Parameters()
		{
			SkipStartMenu = "false";
		}
	}
}