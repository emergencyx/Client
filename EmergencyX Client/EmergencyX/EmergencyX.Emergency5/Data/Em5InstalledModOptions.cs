namespace EmergencyX.Emergency5.Data
{
	public class Em5InstalledModOptions
	{
		private bool _enabled;
		private int _orderingIndex;

		public bool Enabled
		{
			get => _enabled;
			set => _enabled = value;
		}

		public int OrderingIndex
		{
			get => _orderingIndex;
			set => _orderingIndex = value;
		}
	}
}