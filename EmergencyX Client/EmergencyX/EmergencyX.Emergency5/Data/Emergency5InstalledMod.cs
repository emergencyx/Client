using System.Text.Json.Serialization;
using EmergencyX.Core;

namespace EmergencyX.Emergency5.Data
{
	public class Emergency5InstalledMod : BaseObject
	{
		private string _modificationName;
		private bool _enabled;
		private int _orderingIndex;
		public string ModificationName
		{
			get => _modificationName;
			set
			{
				_modificationName = value;
				OnPropertyChanged();
			}
		}

		public bool Enabled
		{
			get => _enabled;
			set
			{
				_enabled = value;
				OnPropertyChanged();
			}
		}

		public int OrderingIndex
		{
			get => _orderingIndex;
			set
			{
				_orderingIndex = value;
				OnPropertyChanged();
			}
		}

		public Emergency5InstalledMod()
		{
			
		}

		public Emergency5InstalledMod(string name, bool enabled, int orderingIndex)
		{
			this.ModificationName = name;
			this.Enabled = enabled;
			this.OrderingIndex = orderingIndex;
		}
		
		public override string ToString()
		{
			return ModificationName;
		}
	}
}