namespace EmergencyX.Emergency5.Data
{
	public class Format
	{
		private string _type;
		private string _version;

		public string Type
		{
			get
			{
				return _type;
			}

			set
			{
				_type = value;
			}
		}

		public string Version
		{
			get
			{
				return _version;
			}

			set
			{
				_version = value;
			}
		}
	}
}