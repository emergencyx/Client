using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EmergencyX.Core;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic;
using EmergencyX.Emergency5.Data.Project;

namespace EmergencyX.Emergency5.Data
{
	public class GameModification : BaseObject, IGameModification
	{
		private string _name;
		private string _author;
		private string _description;
		private string _dataPath;
		private bool _isEnabled;
		private int _orderingIndex;
		private EmergencyProject _emergencyProject;

		private const string PROJECT_JSON = "project.json";

		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		public string Author
		{
			get => _author;
			set
			{
				_author = value;
				OnPropertyChanged();
			}
		}

		public string Description
		{
			get => _description;
			set
			{
				_description = value;
				OnPropertyChanged();
				OnPropertyChanged("GetDescriptionWithLineBreak");
			}
		}

		public string DataPath
		{
			get => _dataPath;
			set
			{
				_dataPath = value;
				OnPropertyChanged();
			}
		}

		public bool IsEnabled
		{
			get => _isEnabled;
			set
			{
				_isEnabled = value;
				OnPropertyChanged();
			}
		}

		public int OrderingIndex
		{
			get => _orderingIndex;
			set
			{
				_orderingIndex = value;
				OnPropertyChanged();
			}
		}

		public string GetModIconName => "mod_icon.png";

		public string GetDescriptionWithLineBreak
		{
			get
			{
				var temp = Description;
				var pattern = @"<br[ \\]*/{0,1}>\s*";
				var regex = new Regex(pattern);
				temp = regex.Replace(temp, "\n\r");
				return temp;
			}
		}

		public EmergencyProject EmergencyProject
		{
			get => _emergencyProject;
			set
			{
				_emergencyProject = value;
				OnPropertyChanged();
			}
		}

		public GameModification()
		{
		}

		public GameModification(EmergencyProject project, string dataPath, bool isEnabled, int orderingIndex)
		{
			Name = project.Properties.Name;
			Description = project.Properties.Description;
			Author = project.Properties.Author;
			DataPath = dataPath;
			IsEnabled = isEnabled;
			OrderingIndex = orderingIndex;
			EmergencyProject = project;
		}

		public string GetProjectFileHash()
		{
			var path = Path.Combine(DataPath, PROJECT_JSON);
			return ContentIdentification.GetSha512(path);
		}
	}
}