using System.Collections.Generic;
using EmergencyX.Core;

namespace EmergencyX.Emergency5.Data.Project
{


	public class EmergencyProject : BaseObject
	{
		private Format _format;
		private ProjectProperties _properties;
		private List<string> _assetPackages;
		private Dictionary<string, string> _plugins;

		#region Propertys

		public Format Format
		{
			get { return _format; }

			set
			{
				_format = value;
				OnPropertyChanged();
			}
		}

		public ProjectProperties Properties
		{
			get { return _properties; }

			set
			{
				_properties = value;
				OnPropertyChanged();
			}
		}

		public List<string> AssetPackages
		{
			get { return _assetPackages; }

			set
			{
				_assetPackages = value;
				OnPropertyChanged();
			}
		}

		public Dictionary<string, string> Plugins
		{
			get { return _plugins; }

			set
			{
				_plugins = value;
				OnPropertyChanged();
			}
		}

		#endregion Propertys

		/// <summary>
		/// Concstructor for Emergency Project Type
		/// </summary>
		public EmergencyProject()
		{

		}
	}
}