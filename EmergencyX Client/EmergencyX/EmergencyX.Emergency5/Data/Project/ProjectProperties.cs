namespace EmergencyX.Emergency5.Data.Project
{
	/// <summary>
	/// Represents the Properties data from the Emergency Project file
	/// </summary>
	public class ProjectProperties
	{
		private string _name;
		private string _description;
		private string _author;
		private string _version;

		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}

			set
			{
				_description = value;
			}
		}

		public string Author
		{
			get
			{
				return _author;
			}

			set
			{
				_author = value;
			}
		}

		public string Version
		{
			get
			{
				return _version;
			}

			set
			{
				_version = value;
			}
		}
	}
}