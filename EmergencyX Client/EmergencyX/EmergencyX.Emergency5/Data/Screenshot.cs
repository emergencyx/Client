using System.IO;
using EmergencyX.Core;

namespace EmergencyX.Emergency5.Data
{
	public class Screenshot : BaseObject
	{
		private string _path;
		private string _screenshotName;

		public string Path
		{
			get => _path;
			set
			{
				_path = value;
				OnPropertyChanged();
			}
		}

		public string ScreenshotName
		{
			get => _screenshotName;
			set
			{
				_screenshotName = value;
				OnPropertyChanged();
			}
		}

		public Screenshot(string path)
		{
			Path = path;
			ScreenshotName = System.IO.Path.GetFileName(path);
		}
	}
}