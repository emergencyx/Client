using Newtonsoft.Json;

namespace EmergencyX.Emergency5.Data.Settings
{
	public class Em5GameSettingsGroup
	{
		public string Version { get; set; }
		public string Language { get; set; }
		public bool ShowVideoSubtitles { get; set; }
		public string FpsLimitation { get; set; }
		public string ScreenshotCapturingSize { get; set; }
		public bool BenchmarkEnabled { get; set; }
		public bool ShowFreeplayMapIntroVideos { get; set; }
		public bool MaterialCacheHeatingDialogEnabled { get; set; }
		public bool UseMacQuality { get; set; }
		public string OverAllQuality { get; set; }
		public string VideoQuality { get; set; }
		public string WaterjetQuality { get; set; }
		public string FireHoseQuality { get; set; }
		public string Population { get; set; }
		public string ShadowQuality { get; set; }
		public string LodQuality { get; set; }
		public string EffectQulity { get; set; }
		public string SavedAccountname { get; set; }
		public string SavedAccountPassword { get; set; }
		public bool ShowLicense { get; set; }
		public bool ShowRatingReminder { get; set; }
		public bool ShowSupportReminder { get; set; }
		public bool ShowNewFeaturesBox { get; set; }
		public string AutomatismValue { get; set; }
		public bool PlayTutorialActive { get; set; }
		public bool TooltipActive { get; set; }
		public bool DriveBackWithoutSirens { get; set; }
		public bool BuildingClippingActive { get; set; }
		public bool ShineThroughActive { get; set; }
		public bool ShowGroundMarkersForAllOwnUnits { get; set; }
		public string DifficultyEM20Missions { get; set; }

		[JsonIgnore] public bool SerializeWithPrivacy { get; set; } = true;

		public bool ShouldSerializeSavedAccountname()
		{
			return !SerializeWithPrivacy;
		}

		public bool ShouldSerializeSavedAccountPassword()
		{
			return !SerializeWithPrivacy;
		}
	}
}