using Newtonsoft.Json;

namespace EmergencyX.Emergency5.Data.Settings
{
	public class Em5MultiplayerSettingsGroup
	{
		public string LastUsedIpForDirectConnect { get; set; }

		[JsonIgnore] public bool SerializeWithPrivacy { get; set; } = true;

		public bool ShouldSerializeLastUsedIpForDirectConnect()
		{
			return !SerializeWithPrivacy;
		}
	}
}