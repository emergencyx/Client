namespace EmergencyX.Emergency5.Data.Settings
{
	public class Emergency5Settings
	{
		public Format Format { get; set; } 
		public ParameterGroups ParameterGroups { get; set; } 
	}
}