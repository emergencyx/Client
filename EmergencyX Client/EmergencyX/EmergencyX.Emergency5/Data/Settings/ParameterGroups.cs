using Newtonsoft.Json;

namespace EmergencyX.Emergency5.Data.Settings
{
	public class ParameterGroups    {
		[JsonProperty("qsf::RendererSettingsGroup")]
		public QsfRendererSettingsGroup QsfRendererSettingsGroup { get; set; } 
		[JsonProperty("qsf::AudioSettingsGroup")]
		public QsfAudioSettingsGroup QsfAudioSettingsGroup { get; set; } 
		[JsonProperty("qsf::compositing::CompositingSettingsGroup")]
		public QsfCompositingCompositingSettingsGroup QsfCompositingCompositingSettingsGroup { get; set; } 
		[JsonProperty("em5::GameSettingsGroup")]
		public Em5GameSettingsGroup Em5GameSettingsGroup { get; set; } 
		[JsonProperty("em5::MultiplayerSettingsGroup")]
		public Em5MultiplayerSettingsGroup Em5MultiplayerSettingsGroup { get; set; } 
	}
}