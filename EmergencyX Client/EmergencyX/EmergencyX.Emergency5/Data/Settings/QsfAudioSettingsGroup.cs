namespace EmergencyX.Emergency5.Data.Settings
{
	public class QsfAudioSettingsGroup    {
		public string AudioSystem { get; set; } 
		public bool EnableOpenALSoft { get; set; } 
		public bool EnableOpenALSoftHrtf { get; set; } 
		public string MixerChannelMasterVolume { get; set; } 
		public string MixerChannelSpeechVolume { get; set; } 
		public string MixerChannelSFXVolume { get; set; } 
		public string MixerChannelMusicVolume { get; set; } 
		public string MixerChannelGUIVolume { get; set; } 
		public string MixerChannelVideoVolume { get; set; } 
	}
}