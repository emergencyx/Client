namespace EmergencyX.Emergency5.Data.Settings
{
	public class QsfCompositingCompositingSettingsGroup    {
		public string CompositorQuality { get; set; } 
		public bool ReadMicrocodeCache { get; set; } 
		public bool WriteMicrocodeCache { get; set; } 
		public bool Shadows { get; set; } 
		public string ShadowTechnique { get; set; } 
		public string ShadowFilter { get; set; } 
		public string ShadowDistance { get; set; } 
		public string ShadowTextureSize { get; set; } 
		public string ShadowFadeStart { get; set; } 
		public string PssmNumberOfSplits { get; set; } 
		public string LightingQuality { get; set; } 
		public string TerrainQuality { get; set; } 
		public string LiquidQuality { get; set; } 
		public bool SoftParticles { get; set; } 
		public bool Fxaa { get; set; } 
		public bool DepthOfField { get; set; } 
	}

}