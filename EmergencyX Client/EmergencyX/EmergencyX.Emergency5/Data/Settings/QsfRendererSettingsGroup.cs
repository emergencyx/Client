namespace EmergencyX.Emergency5.Data.Settings
{
	public class QsfRendererSettingsGroup    {
		public string RendererSystem { get; set; } 
		public bool IntegratedGpuCheck { get; set; } 
		public bool VrEnabled { get; set; } 
		public string VrSystem { get; set; } 
		public string VrResolutionScale { get; set; } 
		public string VrMonitorMode { get; set; } 
		public bool Fullscreen { get; set; } 
		public bool UseBorderlessWindowForFullscreen { get; set; } 
		public bool FullscreenMouseCursorTrap { get; set; } 
		public string WindowSize { get; set; } 
		public string Resolution { get; set; } 
		public bool VerticalSynchronizationEnabled { get; set; } 
		public string VerticalSynchronizationInterval { get; set; } 
		public string MaximumNumberOfQueuedFrames { get; set; } 
		public string LodBias { get; set; } 
		public string MapQuality { get; set; } 
		public string MeshQuality { get; set; } 
		public string ParticlesQuality { get; set; } 
		public string DefaultTextureFiltering { get; set; } 
		public string NumberOfTopMipmapsToRemove { get; set; } 
		public bool UsedTexturesStreamingOnly { get; set; } 
		public string TextureStreamerDefaultMinimumMipLevel { get; set; } 
		public string TextureStreamerFrameTimeBudgetForIo { get; set; } 
		public string TextureStreamerFrameTimeBudgetForVramSync { get; set; } 
		public string TextureStreamerUnloadTimeSpan { get; set; } 
		public string Contrast { get; set; } 
		public string Brightness { get; set; } 
	}
}