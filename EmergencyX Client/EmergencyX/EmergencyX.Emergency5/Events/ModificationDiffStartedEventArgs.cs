using System;
using EmergencyX.Emergency5.Enum;

namespace EmergencyX.Emergency5.Events
{
	public class ModificationDiffStartedEventArgs : EventArgs
	{
		private int _total;
		private ModificationDiffStep _diffStep;

		public int Total
		{
			get => _total;
			set => _total = value;
		}

		public ModificationDiffStep DiffStep
		{
			get => _diffStep;
			set => _diffStep = value;
		}
	}
}