using System;

namespace EmergencyX.Emergency5.Exceptions
{
	public class NotSufficentSpaceForModInstallException : Exception
	{
		public string Info { get; }

		public NotSufficentSpaceForModInstallException(string info)
		{
			Info = info;
		}
	}
}