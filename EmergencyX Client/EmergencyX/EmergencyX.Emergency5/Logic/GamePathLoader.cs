using System;
using System.Diagnostics;
using System.IO;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Emergency5.Enum;
using EmergencyX.Emergency5.Logic.Modifications;
using EmergencyX.Windows;
using EmergencyX.Windows.Enum;
using EmergencyX.Windows.Logic;
using EmergencyX.Windows.Logic.EmergencyX.Windows.Tools;
using log4net;

namespace EmergencyX.Emergency5.Logic
{
	public class GamePathLoader : IGameLoader
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(GamePathLoader));

		internal static string GetAppDataPath =>
			Path.Combine(
				Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
				@"Promotion Software GmbH\EMERGENCY 5\");

		public static int SteamAppId { get; set; } = 0;

		public static string LoadEmergencyInstallationPath()
		{
			if (!string.IsNullOrEmpty(Settings.AppSettings.Emergency5ManualPath) &&
			    Directory.Exists(Settings.AppSettings.Emergency5ManualPath))
			{
				return Settings.AppSettings.Emergency5ManualPath;
			}

			// Try and get EM5 from the registry
			var path = LoadEmergencyPathFromRegistry();

			// if this didn't return a result try steam
			if (string.IsNullOrEmpty(path))
			{
				path = LoadEmergencyInstallationPathFromSteam();
			}

			// return empty if path in settings dosen't exist
			if (!Directory.Exists(path))
			{
				path = String.Empty;
			}

			return path;
		}

		public static string LoadEm5CachePath()
		{
			var path = Path.Combine(GetAppDataPath, "cache");
			return path;
		}

		public static string LoadEm5LogPath()
		{
			var path = Path.Combine(GetAppDataPath, "log");
			return path;
		}

		public static string LoadEm5AppDataModificationPath()
		{
			return ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.AppData);
		}

		public static string LoadEm5DataDirModificationPath()
		{
			return ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.InstallationDir);
		}

		public static string LoadEm5SettingsFilePath()
		{
			return Path.GetFullPath(Path.Combine(GetAppDataPath, "emergency_5_settings.json"));
		}

		public static string LoadEm5GameAppJsonPath()
		{
			Log.Debug(string.Format("Path should be: {0}",
				Path.Combine(LoadEmergencyInstallationPath(), "bin", "game.app")));

			if (File.Exists(Path.GetFullPath(Path.Combine(LoadEmergencyInstallationPath(), "bin",
				    "game.app"))))
			{
				Log.Debug(string.Format("Path is: {0}",
					Path.Combine(LoadEmergencyInstallationPath(), "bin", "game.app")));
				// in older Emergency 5 versions the file did not have a .json extension
				return Path.GetFullPath(
					Path.Combine(LoadEmergencyInstallationPath(), "bin", "game.app"));
			}

			return Path.GetFullPath(Path.Combine(LoadEmergencyInstallationPath(), "bin", "game.app.json"));
		}

		public static void OpenEmergency5InstallDir()
		{
			var path = Path.GetFullPath(GamePathLoader.LoadEmergencyInstallationPath());
			var startCommand = new ProcessStartInfo("explorer.exe", path);

			Log.Debug($"Path is {path}");

			if (Platform.IsWindows)
			{
				Process.Start(startCommand);
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		private static string LoadEmergencyPathFromRegistry()
		{
			var path = RegistryAccess.GetRegistryValueFromSubKey(
				@"SOFTWARE\Sixteen Tons Entertainment\EMERGENCY 5", SubKeys.LocalMachine,
				"Path");

			Log.Debug($"EM5 Path from registry is {path}");

			return path;
		}

		private static string LoadEmergencyInstallationPathFromSteam()
		{
			// EM 20 Years
			var result = Steam.GetSteamGamePath(735280);
			SteamAppId = 735280;

			if (string.IsNullOrEmpty(result))
			{
				// EM 2017
				result = Steam.GetSteamGamePath(524110);
				SteamAppId = 524110;
			}

			if (string.IsNullOrEmpty(result))
			{
				// EM 2016
				result = Steam.GetSteamGamePath(404920);
				SteamAppId = 404920;
			}

			if (string.IsNullOrEmpty(result))
			{
				// EM 5
				result = Steam.GetSteamGamePath(328140);
				SteamAppId = 328140;
			}

			Log.Debug($"Steam path is {result}");

			return result;
		}
	}
}