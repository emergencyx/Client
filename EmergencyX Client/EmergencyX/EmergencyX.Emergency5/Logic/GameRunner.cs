using System;
using System.Diagnostics;
using System.IO;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Core.Util;
using EmergencyX.Emergency5.Data;
using EmergencyX.Emergency5.Data.Application;
using EmergencyX.Windows.Logic.EmergencyX.Windows.Tools;
using log4net;
using Newtonsoft.Json;

namespace EmergencyX.Emergency5.Logic
{
	public class GameRunner
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(GameRunner));

		public static void RunEmergency5(bool skipLauncher)
		{
			if (!Platform.IsWindows)
			{
				throw new NotSupportedException();
			}

			// Use Steam Command Line Options to start the game
			var path = Path.GetFullPath(GamePathLoader.LoadEmergencyInstallationPath());

			Log.Debug(string.Format("{0}: using emergency path: {1}", "RunEmergency5", path));

			if (skipLauncher)
			{
				Log.Info("Skipping launcher");
				ExecuteRunGameSkipLauncher(); // very advanced...
			}
			else if (GamePathLoader.SteamAppId != 0)
			{
				// prepare steam launch
				path = Steam.GetSteamPath();
				if (!path.EndsWith("steam.exe"))
				{
					path = Path.GetFullPath(Path.Combine(path, Steam.STEAM_EXE));
				}

				var startOption = new ProcessStartInfo(path);
				startOption.Arguments = String.Format("-applaunch {0}", GamePathLoader.SteamAppId);
				startOption.WorkingDirectory = new FileInfo(path).DirectoryName ?? string.Empty;
				startOption.UseShellExecute = true;
				Process.Start(startOption);
			}
			else
			{
				// Start "normal"
				Log.Debug(string.Format("Starting the following exe file: {0}",
					string.Format("\"{0}\"", Path.Combine(path, @"bin\em5_launcher.exe"))));
				Process.Start(string.Format("\"{0}\"", Path.Combine(path, @"bin\em5_launcher.exe")));
			}
		}


		public static void RunEmergency5Editor()
		{
			var path = Path.GetFullPath(GamePathLoader.LoadEmergencyInstallationPath());
			var arguments = new ProcessStartInfo();

			arguments.FileName = string.Format("\"{0}\"", Path.Combine(path, @"bin\x64r\emergency5.exe"));

			if (!File.Exists(arguments.FileName))
			{
				return;
			}
			
			// in earlier versions of Emergency 5 the file did not have the .json extension
			var editorAppFilePath = File.Exists(Path.Combine(path, @"bin\editor.app"))
				? Path.Combine(path, @"bin\editor.app")
				: Path.Combine(path, @"bin\editor.app.json");

			if (!File.Exists(editorAppFilePath))
			{
				return;
			}
			
			arguments.Arguments = string.Format("{0} \"{1}\"", "--application",
				Path.GetFullPath(editorAppFilePath));
			arguments.WorkingDirectory = Path.Combine(path, @"bin\x64r");

			if (!Directory.Exists(arguments.WorkingDirectory))
			{
				return;
			}

			Log.Info("Starting Em5 Editor");
			Log.Debug(string.Format("FileName: {0}", arguments.FileName));
			Log.Debug(string.Format("Arguments: {0}", arguments.Arguments));
			Log.Debug(string.Format("WorkingDir: {0}", arguments.WorkingDirectory));

			Process.Start(arguments);
		}

		private static void ExecuteRunGameSkipLauncher()
		{
			var path = Path.GetFullPath(Path.Combine(GamePathLoader.LoadEmergencyInstallationPath(),
				"bin"));
			var arguments = new ProcessStartInfo();
			var gameapp = Path.Combine(PathUtil.GetEmxAppDataPath(), "emx.app.json");

			Log.Debug(
				string.Format("{0}: using emergency path: {1}", "ExecuteRunGameSkipLauncher", gameapp));

			// create custom game.app.json to run the game
			if (!File.Exists(gameapp))
			{
				CreateEmxGameAppJson(gameapp, path);
			}
			else
			{
				// check if emxgame.app.json is outdated
				var obj = JsonConvert.DeserializeObject<GameApp>(
					File.ReadAllText(Path.Combine(path, "game.app.json")));
				var emxObj = JsonConvert.DeserializeObject<GameApp>(File.ReadAllText(gameapp));

				if (obj.Application.ApplicationVersion != emxObj.Application.ApplicationVersion)
				{
					// Update emxgame.app.json due to being outdate
					File.Delete(gameapp);
					CreateEmxGameAppJson(gameapp, path);
				}
			}

			arguments.FileName = string.Format("\"{0}\"", Path.Combine(path, @"x64r\emergency5.exe"));
			arguments.Arguments = string.Format("{0} \"{1}\"", "--application", gameapp);
			arguments.WorkingDirectory = Path.Combine(path, @"x64r");

			Log.Info("Starting Em5 to game directly");
			Log.Debug(string.Format("FileName: {0}", arguments.FileName));
			Log.Debug(string.Format("Arguments: {0}", arguments.Arguments));
			Log.Debug(string.Format("WorkingDir: {0}", arguments.WorkingDirectory));

			Process.Start(arguments);
		}

		private static void CreateEmxGameAppJson(string emxGameAppPath, string em5Path)
		{
			var obj = JsonConvert.DeserializeObject<GameApp>(
				File.ReadAllText(Path.Combine(em5Path, "game.app.json")));

			if (obj.Application.Parameters == null)
			{
				obj.Application.Parameters = new Parameters();
			}

			obj.Application.Parameters.SkipStartMenu = "true";
			File.Create(emxGameAppPath).Close();
			File.WriteAllText(emxGameAppPath, JsonConvert.SerializeObject(obj, Formatting.Indented));
		}
	}
}