using System.Collections.Generic;
using System.IO;
using System.Linq;
using EmergencyX.Emergency5.Data;
using EmergencyX.Emergency5.Data.Project;
using EmergencyX.Emergency5.Enum;
using log4net;
using Newtonsoft.Json;

namespace EmergencyX.Emergency5.Logic.Modifications
{
	public class ModificationEditor
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(ModificationEditor));

		private List<Emergency5InstalledMod> ReadInstalledModifications()
		{
			var list = ReadInstalledsModFile();

			var returnList = new List<Emergency5InstalledMod>();
			if (list == null)
			{
				return returnList;
			}

			Log.Debug($"Found {list.Count} installed mods");

			foreach (var pair in list)
			{
				returnList.Add(new Emergency5InstalledMod(pair.Key, pair.Value.Enabled,
					pair.Value.OrderingIndex));
			}

			return returnList;
		}

		internal Dictionary<string, Em5InstalledModOptions> ReadInstalledsModFile()
		{
			var path = ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.AppData);
			path = Path.Combine(path, ModificationPathLoader.GetNameOfInstalledModSettingsFile);

			Log.Debug($"Path for installed mods file is {path}");

			if (!File.Exists(path))
			{
				return null;
			}

			Dictionary<string, Em5InstalledModOptions> list =
				JsonConvert.DeserializeObject<Dictionary<string, Em5InstalledModOptions>>(
					File.ReadAllText(path));
			return list;
		}

		internal List<GameModification> Read()
		{
			var list = new List<GameModification>();
			var mods = ReadInstalledModifications();

			foreach (var item in mods)
			{
				var modPath = ModificationPathLoader.LoadModificationPath(item.ModificationName);
				var projectjson = Path.GetFullPath(Path.Combine(modPath, "project.json"));
				if (!File.Exists(projectjson))
				{
					Log.Info($"Project json not found: {projectjson}");
					continue;
				}

				var emergencyProject =
					JsonConvert.DeserializeObject<EmergencyProject>(
						File.ReadAllText(projectjson));
				list.Add(new GameModification(emergencyProject, modPath, item.Enabled,
					item.OrderingIndex));
			}

			return list;
		}

		internal void UninstallModFromListOfInstalledMods(string name)
		{
			Log.Debug($"Trying to uninstall mod: {name}");

			// get current content of the file that holds the installed modifications
			var list = ReadInstalledsModFile();

			// Remove the item from the list
			list.Remove(name);

			// write it back
			var serializeObject = JsonConvert.SerializeObject(list);

			var path = ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.AppData);
			path = Path.Combine(path, ModificationPathLoader.GetNameOfInstalledModSettingsFile);

			Log.Debug($"Uninstalling mod from settings file at {path}");

			File.WriteAllText(path, serializeObject);
		}

		internal void InstallModToListOfInstalledMods(EmergencyProject project)
		{
			// get current content of the file that holds the mod data
			var list = ReadInstalledsModFile();

			// in case the game was never opend this can happen
			if (list == null || list.Count == 0)
			{
				return;
			}
			
			// get the data for the mod that should be added

			var key = project.Properties.Name;

			Log.Debug($"Trying to install project {key}");

			var projectOptions = new Em5InstalledModOptions();
			projectOptions.OrderingIndex =
				list.Count == 0 ? 0 : list.Max(pair => pair.Value.OrderingIndex) + 1;

			Log.Debug($"Trying to install project with ordering index {projectOptions.OrderingIndex}");

			projectOptions.Enabled = false;

			// add the new mod (in case a key already exists its an update
			if (list != null && list.Count(x => x.Key == key) == 0)
			{
				list.Add(key, projectOptions);
			}
			else if (list == null)
			{
				list = new Dictionary<string, Em5InstalledModOptions> { { key, projectOptions } };
			}

			// write it back
			var serializeObject = JsonConvert.SerializeObject(list);

			var path = ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.AppData);
			path = Path.Combine(path, ModificationPathLoader.GetNameOfInstalledModSettingsFile);

			Log.Debug($"Trying to install project to modifications file {path}");

			File.WriteAllText(path, serializeObject);
		}

		internal void SaveChangesInInstalledModList(List<GameModification> list)
		{
			var result = new Dictionary<string, Em5InstalledModOptions>();
			foreach (var gameModification in list)
			{
				result.Add(gameModification.Name,
					new Em5InstalledModOptions()
					{
						Enabled = gameModification.IsEnabled,
						OrderingIndex = list.IndexOf(gameModification)
					});
			}

			var json = JsonConvert.SerializeObject(result);
			var path = ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.AppData);
			path = Path.Combine(path, ModificationPathLoader.GetNameOfInstalledModSettingsFile);

			Log.Debug($"Writing to mod install file {path}");

			File.WriteAllText(path, json);
		}
	}
}