using System;
using System.IO;
using EmergencyX.Emergency5.Enum;
using log4net;

namespace EmergencyX.Emergency5.Logic.Modifications
{
	public class ModificationPathLoader
	{

		private static readonly ILog Log = LogManager.GetLogger(typeof(ModificationPathLoader));
		
		internal static string GetNameOfInstalledModSettingsFile => "mods_user_settings.json";

		internal static string LoadModificationPath(string name)
		{
			if (Directory.Exists(Path.Combine(GamePathLoader.LoadEmergencyInstallationPath(), "data",
				name)))
			{
				return Path.Combine(GamePathLoader.LoadEmergencyInstallationPath(), "data", name);
			}

			return Path.Combine(GamePathLoader.GetAppDataPath, "mods", name);
		}

		internal static string ProvideModificationsDirectory(Em5ModLocator location)
		{
			if (location == Em5ModLocator.AppData)
			{
				return Path.Combine(GamePathLoader.GetAppDataPath, "mods");
			}

			return Path.Combine(GamePathLoader.LoadEmergencyInstallationPath(), "data");
		}
	}
}