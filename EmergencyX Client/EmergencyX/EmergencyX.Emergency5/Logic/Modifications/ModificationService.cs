using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using EmergencyX.Core.Data.Modification;
using EmergencyX.Core.Events;
using EmergencyX.Core.Interfaces;
using EmergencyX.Core.Logic;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.Util;
using EmergencyX.Emergency5.Data;
using EmergencyX.Emergency5.Data.Project;
using EmergencyX.Emergency5.Enum;
using EmergencyX.Emergency5.Events;
using EmergencyX.Emergency5.Exceptions;
using log4net;
using Newtonsoft.Json;

namespace EmergencyX.Emergency5.Logic.Modifications
{
	public class ModificationService : AbstractModificationService
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(ModificationService));

		// Event stuff

		#region EventHandler

		public EventHandler<ModificationDiffStartedEventArgs> ModificationDiffStartedEvent;
		public EventHandler<ModificationDiffProgressedEventArgs> ModificationDiffProgressedEvent;

		public override void OnModificationInstallationEventStart(ModificationInstallationStartedEventArgs e)
		{
			ModificationInstallationStartedEvent?.Invoke(this, e);
		}

		public override void OnModificationInstallationProgress(InstallationProgressEventArgs e)
		{
			ModificationInstallationProgressEvent?.Invoke(this, e);
		}

		public override void OnModificationInstallationEndedEvent(ModificationInstallationEndEventArgs e)
		{
			ModificationEndedEvent?.Invoke(this, e);
		}

		public virtual void OnModificationDiffStartedEvent(ModificationDiffStartedEventArgs e)
		{
			ModificationDiffStartedEvent?.Invoke(this, e);
		}

		public virtual void OnModificationDiffProgressedEvent(ModificationDiffProgressedEventArgs e)
		{
			ModificationDiffProgressedEvent?.Invoke(this, e);
		}

		#endregion

		// Installation and Uninstall methods

		#region InstallationAndDeinstallation

		public static void UninstallModification(GameModification modification)
		{
			Log.Debug($"Trying to uninstall {modification.Name}");
			// delete files from disk
			var modPath = ModificationPathLoader.LoadModificationPath(modification.Name);

			Log.Debug($"Deleting files from path {modPath}");

			Directory.Delete(modPath, true);

			// delete files from mods_user_settings.json
			var editor = new ModificationEditor();
			editor.UninstallModFromListOfInstalledMods(modification.Name);
		}

		public async Task InstallAsync(string[] mods)
		{
			// enable installation of multi mods in one go
			foreach (var mod in mods)
			{
				Log.Debug($"Trying to install {mod}");

				if (mod.EndsWith(".zip"))
				{
					// Create progress reporter for task, so we can show a nice progressbar
					var progress = new Progress<int>(update =>
					{
						OnModificationInstallationProgress(
							new InstallationProgressEventArgs()
								{ Progress = update });
					});

					await Task.Run(
						() =>
						{
							InstallModificationAsync(mod, progress);
							return Task.CompletedTask;
						});
				}
			}

			OnModificationInstallationEndedEvent(
				new ModificationInstallationEndEventArgs(false));
		}

		private void InstallModificationAsync(string modFile,
			IProgress<int> progress)
		{
			string installPath;
			string extractPath;
			EmergencyProject project;
			RemovableFiles filesToDelete;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			using (ZipArchive archive = ZipFile.OpenRead(modFile))
			{
				// check if it is a valid EM5 mod zip
				if (archive.Entries.Count(x => x.Name == "project.json") != 1)
				{
					Log.Error($"Invalid mod archive");
					throw new NotSupportedException("Invalid Emergency 5 archive");
				}

				// load content of project.json
				var jsonContent = archive.Entries.Single(x => x.Name == "project.json").Open();
				using (StreamReader streamReader = new StreamReader(jsonContent))
				using (JsonReader jsonReader = new JsonTextReader(streamReader))
				{
					JsonSerializer serializer = new JsonSerializer();
					project = serializer.Deserialize<EmergencyProject>(jsonReader);
				}

				// load content of emx-remove.dem (this file will tell us which files have been deleted) 
				if (archive.Entries.Any(x => x.Name.Contains("emx-remove.dem")))
				{
					jsonContent = archive.Entries.Single(x => x.Name.Contains("emx-remove.dem"))
						.Open();
					using StreamReader streamReader = new StreamReader(jsonContent);
					using (JsonReader jsonReader = new JsonTextReader(streamReader))
					{
						JsonSerializer serializer = new JsonSerializer();
						filesToDelete = serializer.Deserialize<RemovableFiles>(jsonReader);
					}
				}
				else
				{
					filesToDelete = null;
				}

				if (project == null)
				{
					Log.Error("Project is null");
					throw new ArgumentNullException();
				}

				var modName = project.Properties.Name;

				// check if new installation or update
				bool isUpdate = Directory.Exists(GetModPath(modName)) &&
				                Directory.GetDirectories(GetModPath(modName)).Length > 0;

				Log.Info($"Is it a update: {isUpdate}");

				installPath = Settings.AppSettings.InstallToEmergency5DataDirectory
					? ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator
						.InstallationDir)
					: ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.AppData);

				// Normalizes the path.
				extractPath = isUpdate
					? Path.GetFullPath(GetModPath(modName))
					: Path.GetFullPath(Path.Combine(installPath, modName));

				// Ensures that the last character on the extraction path is the directory separator char
				// Without this, a malicious zip file could try to traverse outside of the expected extraction path
				if (!extractPath.EndsWith(Path.DirectorySeparatorChar.ToString(),
					    StringComparison.Ordinal))
				{
					extractPath += Path.DirectorySeparatorChar;
				}

				Log.Debug($"Extraction path is {extractPath}");

				// if path dosen't exist, create it
				if (!Directory.Exists(extractPath))
				{
					Log.Debug($"Creating extract path");
					Directory.CreateDirectory(extractPath);
				}

				// calculate uncompressed size
				long uncompressedSize = 0;
				foreach (var item in archive.Entries)
				{
					uncompressedSize += item.Length;
				}

				Log.Info(
					$"Size of the mod, unpacked, is: {uncompressedSize} (in GB: {uncompressedSize / 1000 / 1000 / 1000}");

				var directoryRoot = Directory.GetDirectoryRoot(installPath);
				var driveInfo = DriveInfo.GetDrives().Single(x => x.Name == directoryRoot);

				// check if space is sufficient to install including some margin
				if (driveInfo.AvailableFreeSpace < (uncompressedSize + 2073741824))
				{
					Log.Error(
						$"Disk space problem, space is: {driveInfo.AvailableFreeSpace}, size plus margine is {(uncompressedSize + 2073741824)}");
					throw new NotSufficentSpaceForModInstallException(
						$"Space: {driveInfo.AvailableFreeSpace}, Required: {uncompressedSize}, Mod: {modName}");
				}

				// check if a directory was packed instead of the modification
				var replacementString =
					archive.Entries.Single(x => x.Name.Contains("project.json")).FullName
						.Replace("project.json", "");
				bool replaceSubDirectoryName = !string.IsNullOrEmpty(replacementString);

				// add number of files that will be delted to the overall update size to be able to reflect this in the progressbar
				int noOfFilesToDelete = 0;
				if (filesToDelete != null && filesToDelete.AllowDeletion)
				{
					noOfFilesToDelete = filesToDelete.RelativePathNamesOfDeletableFiles.Count;
				}

				OnModificationInstallationEventStart(
					new ModificationInstallationStartedEventArgs()
					{
						ModName = modName,
						NoOfFiles = (archive.Entries.Count + noOfFilesToDelete)
					});
				var counter = 1;

				// delete old files (only if it is an update and the files should exist)
				if (noOfFilesToDelete > 0 && filesToDelete != null && isUpdate)
				{
					foreach (var file in filesToDelete.RelativePathNamesOfDeletableFiles)
					{
						var path = Path.GetFullPath(Path.Combine(extractPath, file));

						if (File.Exists(path))
						{
							File.Delete(path);
						}

						counter++;
					}
				}

				// install files
				foreach (ZipArchiveEntry entry in archive.Entries)
				{
					// in case of a mod file that contains the mod in a sub directory
					if (replaceSubDirectoryName &&
					    entry.FullName.Replace(replacementString, "")
						    .Trim(Path.DirectorySeparatorChar).Trim('/') == string.Empty)
					{
						continue;
					}

					// Gets the full path to ensure that relative segments are removed.
					string destinationPath = replaceSubDirectoryName
						? Path.GetFullPath(Path.Combine(extractPath,
							entry.FullName.Replace(replacementString, "")))
						: Path.GetFullPath(Path.Combine(extractPath, entry.FullName));
					;

					var dirName = new FileInfo(destinationPath).Directory?.FullName;
					if (dirName != string.Empty && !Directory.Exists(dirName))
					{
						Log.Debug($"Dir creation on install: {dirName}");
						Directory.CreateDirectory(dirName);
					}

					// only copy files, not directories
					if (entry.Name == string.Empty)
					{
						continue;
					}

					// Ordinal match is safest, case-sensitive volumes can be mounted within volumes that are case-insensitive
					if (destinationPath.StartsWith(extractPath, StringComparison.Ordinal))
					{
						// overwrite for updates
						entry.ExtractToFile(destinationPath, true);
					}

					if (progress != null)
					{
						progress.Report(counter);
					}

					counter++;
				}
			}

			// update mods_user_settings.json
			var editor = new ModificationEditor();
			editor.InstallModToListOfInstalledMods(project);


			stopwatch.Stop();
#if DEBUG

			Log.Debug($"Install time taken: {stopwatch.ElapsedMilliseconds / 1000}");
#endif
		}

		#endregion

		// Additional utilitarian methods

		#region Utility

		public static string GetModPath(string name)
		{
			return ModificationPathLoader.LoadModificationPath(name);
		}

		public static List<GameModification> LoadModList()
		{
			List<GameModification> list;
			var editor = new ModificationEditor();
			list = editor.Read();
			return list;
		}

		public static void SaveModList(List<GameModification> modList)
		{
			var editor = new ModificationEditor();
			editor.SaveChangesInInstalledModList(modList);
		}

		#endregion

		// Methods related to checking a mod installation and comparing modification zip files

		#region ChecksAndComparssion

		/// <summary>
		/// Checks for modifications that are not installed to the mods_user_settings.json
		/// </summary>
		public static void CheckForIncorrectInstalledMods()
		{
			Log.Debug($"Trying to identify incorrect installed mods");

			// first: What mods are installed according to the mods_user_settings.json
			var modDirPath = ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.AppData);
			var installed = new Dictionary<string, Em5InstalledModOptions>();
			var installedFile = Path.Combine(modDirPath,
				ModificationPathLoader.GetNameOfInstalledModSettingsFile);
			installed = JsonConvert.DeserializeObject<Dictionary<string, Em5InstalledModOptions>>(
				File.ReadAllText(installedFile));
			bool changed = false;

			// check mods dir first
			DirectoryInfo info =
				new DirectoryInfo(modDirPath);
			EmergencyProject project;
			string projectJson;
			foreach (var dir in info.GetDirectories())
			{
				projectJson = Path.Combine(dir.FullName, "project.json");
				if (!File.Exists(projectJson))
				{
					continue;
				}

				project = JsonConvert.DeserializeObject<EmergencyProject>(
					File.ReadAllText(projectJson));

				if (installed != null && installed.Any(x => x.Key == project.Properties.Name))
				{
					continue;
				}

				Log.Debug(
					$"Adding {project.Properties.Name} to the installed mod files, from app data path");

				if (installed == null)
				{
					installed = new Dictionary<string, Em5InstalledModOptions>();
				}

				installed.Add(project.Properties.Name,
					new Em5InstalledModOptions()
					{
						OrderingIndex = (installed == null ? 0 : installed.Count + 1),
						Enabled = false
					});
				changed = true;
			}

			projectJson = null;
			info = null;

			info = new DirectoryInfo(
				ModificationPathLoader.ProvideModificationsDirectory(Em5ModLocator.InstallationDir));

			foreach (var dir in info.GetDirectories())
			{
				// don't search default em dirs (from the game)
				switch (dir.Name)
				{
					case "em5":
					case "em20years":
					case "em2016":
					case "em2017":
					case "qsf":
						continue;
				}

				projectJson = Path.Combine(dir.FullName, "project.json");
				if (!File.Exists(projectJson))
				{
					continue;
				}

				project = JsonConvert.DeserializeObject<EmergencyProject>(
					File.ReadAllText(projectJson));

				if (installed.Any(x => x.Key == project.Properties.Name))
				{
					continue;
				}

				Log.Debug(
					$"Adding {project.Properties.Name} to the installed mod files, from game data path");

				installed.Add(project.Properties.Name,
					new Em5InstalledModOptions()
						{ OrderingIndex = installed.Count + 1, Enabled = false });
				changed = true;
			}

			if (changed)
			{
				var jsonStr = JsonConvert.SerializeObject(installed, Formatting.Indented);
				File.WriteAllText(installedFile, jsonStr);
			}
		}

		/// <summary>
		/// Creates a diff between to different EM5 Mod ZIP files
		/// </summary>
		/// <param name="baselineFile">The older mod file, should be a EM5 ZIP</param>
		/// <param name="compareFile">The new mod file, should be a EM5 ZIP</param>
		/// <param name="service">An instance of this class that has the Diff-EventHandlers set</param>
		/// <returns>A string that is the path to the diff zip file</returns>
		/// <exception cref="NotSupportedException">Is thrown when a ZIP file was given that is not a EM5 ZIP</exception>
		/// <exception cref="ArgumentNullException">Is thrown when one of the project.jsons can't be correctly converted</exception>
		/// <exception cref="InvalidOperationException">Is thrown when the project.jsons contain different project names</exception>
		public static string CompareEmergency5ModificationZips(string baselineFile, string compareFile,
			ModificationService service)
		{
			try
			{
				ZipArchive baseline = ZipFile.OpenRead(baselineFile);
				ZipArchive compare = ZipFile.OpenRead(compareFile);

				// check if the files are valid EM5 mods (as far as we can tell...)
				if (baseline.Entries.Count(x => x.Name == "project.json") != 1 ||
				    compare.Entries.Count(x => x.Name == "project.json") != 1)
				{
					Log.Error($"Invalid mod archive");
					throw new NotSupportedException("Invalid Emergency 5 archive");
				}

				// read project.json for each zip file
				EmergencyProject baselineProject;
				EmergencyProject compareProject;

				var jsonContentBaseline = baseline.Entries.Single(x => x.Name == "project.json").Open();
				using (StreamReader streamReader = new StreamReader(jsonContentBaseline))
				using (JsonReader jsonReader = new JsonTextReader(streamReader))
				{
					JsonSerializer serializer = new JsonSerializer();
					baselineProject = serializer.Deserialize<EmergencyProject>(jsonReader);
				}

				jsonContentBaseline.Dispose();

				var jsonContentCompare = compare.Entries.Single(x => x.Name == "project.json").Open();
				using (StreamReader streamReader = new StreamReader(jsonContentCompare))
				using (JsonReader jsonReader = new JsonTextReader(streamReader))
				{
					JsonSerializer serializer = new JsonSerializer();
					compareProject = serializer.Deserialize<EmergencyProject>(jsonReader);
				}

				jsonContentCompare.Dispose();

				// are the project.jsons empty?
				if (baselineProject == null || compareProject == null)
				{
					Log.Error("Project is null");
					throw new ArgumentNullException();
				}

				// are the project.jsons related to the same mod?
				if (baselineProject.Properties.Name != compareProject.Properties.Name)
				{
					Log.Error("The two ZIP files are not related to the same project!");
					throw new InvalidOperationException(
						"The two ZIP files are not related to the same project!");
				}

				// setup progress reporting
				IProgress<int> progress = new Progress<int>(update =>
					service.OnModificationDiffProgressedEvent(
						new ModificationDiffProgressedEventArgs()
							{ Progress = update }));

				// emit progress total for the first progressbar
				// foreach step 3 "parts" are used, the steps are first foreach, second foreach and creating the delta list
				// after the delta is created the second progressbar will be initiated
				service.OnModificationDiffStartedEvent(new ModificationDiffStartedEventArgs()
					{ Total = 3, DiffStep = ModificationDiffStep.Prerequisites });

				// check for changed files

				var listOfDeletedEntries = new List<string>();
				var listOfChangedEntries = new List<ZipArchiveEntry>();
				var listOfNewEntries = new List<ZipArchiveEntry>();
				var listOfNotChangedEntryNames = new List<string>();

				foreach (var baselineEntry in baseline.Entries)
				{
					// check if the file is present in both archives
					if (!compare.Entries.Any(x => x.FullName == baselineEntry.FullName))
					{
						// if it is in the baseline but not in the compare archive then it's been deleted or at least renamed
						// from the programs perspective that is the same tho, so mark it and move on
						// we are adding the baseline entry because that has to be deleted
						listOfDeletedEntries.Add(baselineEntry.FullName);
						continue;
					}

					// now check if the file has been modified
					var compareEntry =
						compare.Entries.Single(x => x.FullName == baselineEntry.FullName);
					if (baselineEntry.Crc32 == compareEntry.Crc32 &&
					    baselineEntry.Length == compareEntry.Length)
					{
						// nothing to see here (because nothing has changed)
						listOfNotChangedEntryNames.Add(compareEntry.FullName);
						continue;
					}

					// the file has been changed, so note it
					// here we are adding the compare entry because this is the updated file and this has to be put onto the users pc
					listOfChangedEntries.Add(compareEntry);
				}

				// report that step is done
				progress.Report(1);

				// at this point we have "processed" deleted files and updated files
				// however we still have to look at new added files
				foreach (var compareEntry in compare.Entries)
				{
					// if the current entry is in the list of the updated files (or the list of unchanged files) then we can move on
					if (listOfChangedEntries.Any(x => x.FullName == compareEntry.FullName) ||
					    listOfNotChangedEntryNames.Any(x => x == compareEntry.FullName))
					{
						continue;
					}

					// this item is new and should be noted accordingly
					listOfNewEntries.Add(compareEntry);
				}

				// report that step is done
				progress.Report(2);

				// at this point we know all the things about the baseline and the compare zip and we can create a ZIP file that has only the delta changes
				// sooo... lets make a new ZIP, shall we

				var temp = Path.Combine(PathUtil.GetEmxAppDataPath(), compareProject.Properties.Name);
				int diffCounter = 0;

				if (Directory.Exists(temp))
				{
					Directory.Delete(temp, true);
				}

				Directory.CreateDirectory(temp);

				foreach (var changed in listOfChangedEntries)
				{
					if (!Directory.Exists(Path.Combine(temp,
						    changed.FullName.Replace(changed.Name, ""))))
					{
						Directory.CreateDirectory(Path.Combine(temp,
							changed.FullName.Replace(changed.Name, "")));
					}

					changed.ExtractToFile(Path.Combine(temp, changed.FullName));
					diffCounter++;
				}

				foreach (var newEntry in listOfNewEntries)
				{
					if (!Directory.Exists(Path.Combine(temp,
						    newEntry.FullName.Replace(newEntry.Name, ""))))
					{
						Directory.CreateDirectory(Path.Combine(temp,
							newEntry.FullName.Replace(newEntry.Name, "")));
					}

					newEntry.ExtractToFile(Path.Combine(temp, newEntry.FullName));
					diffCounter++;
				}

				// add removable files
				var jsonContent =
					JsonConvert.SerializeObject(new RemovableFiles(true, listOfDeletedEntries));
				File.WriteAllText(Path.Combine(temp, "emx-remove.dem"), jsonContent);
				diffCounter++;

				// report that step is done
				progress.Report(3);

				// emit progress total for the second progressbar
				// this time we use the total number of files that need to be added to the diff zip
				service.OnModificationDiffStartedEvent(new ModificationDiffStartedEventArgs()
					{ Total = diffCounter, DiffStep = ModificationDiffStep.FileCreation });

				progress = new Progress<int>(update => service.OnModificationDiffProgressedEvent(
					new ModificationDiffProgressedEventArgs()
						{ Progress = update }));


				if (File.Exists(Path.Combine(PathUtil.GetEmxAppDataPath(),
					    string.Format("{0}.{1}", compareProject.Properties.Name, "zip"))))
				{
					File.Delete(Path.Combine(PathUtil.GetEmxAppDataPath(),
						string.Format("{0}.{1}", compareProject.Properties.Name, "zip")));
				}

				// manually create a zip file to be able to measure progress
				int counter = 1;
				using (ZipArchive archive = ZipFile.Open(Path.Combine(PathUtil.GetEmxAppDataPath(),
						       string.Format("{0}.{1}", compareProject.Properties.Name, "zip")),
					       ZipArchiveMode.Create))
				{
					foreach (var file in Directory.EnumerateFiles(temp, "*.*",
						         SearchOption.AllDirectories))
					{
						var entry = Path.GetFullPath(file).Replace(Path.GetFullPath(temp), "")
							.TrimStart('\\').TrimStart(Path.DirectorySeparatorChar);
						archive.CreateEntryFromFile(file, entry);
						progress.Report(counter++);
					}
				}

				Directory.Delete(temp, true);

				return Path.Combine(PathUtil.GetEmxAppDataPath(),
					string.Format("{0}.{1}", compareProject.Properties.Name, "zip"));
			}
			catch (Exception e)
			{
				Log.Error($"Failed to create patch file for mod {baselineFile}");
				Log.Error(e);
			}

			return null;
		}

		#endregion

		#region ModPackageCreation

		public static async Task<string> CreateModificationPackage(string folder)
		{
			// parse project.json to get version no and mod name
			ModificationEditor editor = new ModificationEditor();
			var currentMod = editor.Read().Single(m => m.DataPath == folder);
			var fileName =
				string.Join("_",
					currentMod.EmergencyProject.Properties.Name.Split(
						Path.GetInvalidFileNameChars())) + "_v" + string.Join("_",
					currentMod.EmergencyProject.Properties.Version.Split(
						Path.GetInvalidFileNameChars())) + "_" +
				DateTime.Now.ToString("yyyy-MM-dd");
			var zipName = Path.Combine(folder, "..", $"{fileName}.zip");

			int c = 1;
			while (File.Exists(zipName))
			{
				zipName = Path.Combine(folder, "..", $"{c.ToString()}_{fileName}.zip");
				c++;
			}

			await Task.Run(() => ZipFile.CreateFromDirectory(folder, zipName));

			return zipName;
		}

		#endregion
	}
}