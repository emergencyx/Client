using System.Collections.Generic;
using System.IO;
using System.Linq;
using EmergencyX.Emergency5.Data;

namespace EmergencyX.Emergency5.Logic.Screenshots
{
	public class ScreenshotService
	{
		public static string GetScreenshotsDirectory =>
			Path.Combine(GamePathLoader.GetAppDataPath, "screenshot");

		public static List<Screenshot> LoadScreenshots()
		{
			if (!Directory.Exists(GetScreenshotsDirectory))
			{
				return new List<Screenshot>();
			}

			var list = Directory.GetFiles(GetScreenshotsDirectory).Where(x => x.EndsWith(".tif"))
				.Select(x => new Screenshot(x)).ToList();

			return list;
		}
	}
}