using System.IO;
using EmergencyX.Core.Converters.Json;
using EmergencyX.Emergency5.Data;
using EmergencyX.Emergency5.Data.Settings;
using log4net;
using Newtonsoft.Json;

namespace EmergencyX.Emergency5.Logic
{
	public class SettingsEditor
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(GameRunner));

		/// <summary>
		/// Static members
		/// </summary>
		private static Emergency5Settings _gameSettings;

		private static JsonConverter[] _converters;

		private static GameApp _gameApp;

		private static readonly object _lockSettings = new object();
		private static readonly object _lockGameApp = new object();


		protected static JsonConverter[] Converters
		{
			get => _converters;
			set => _converters = value;
		}

		public static Emergency5Settings GameSettings
		{
			get
			{
				if (_gameSettings == null)
				{
					lock (_lockSettings)
					{
						if (_gameSettings == null)
						{
							Converters = new JsonConverter[] { new BooleanJsonConverter() };

							var info = new FileInfo(
								GamePathLoader.LoadEm5SettingsFilePath());
							
							if (!info.Directory.Exists || !info.Exists)
							{
								return new Emergency5Settings();
							}

							_gameSettings =
								JsonConvert.DeserializeObject<Emergency5Settings>(
									File.ReadAllText(GamePathLoader
										.LoadEm5SettingsFilePath()),
									Converters);
						}
					}
				}

				return _gameSettings;
			}
			private set => _gameSettings = value;
		}

		public static GameApp GameApp
		{
			get
			{
				Log.Debug("GameApp: Entering Get");
				if (_gameApp == null)
				{
					Log.Debug("Construct game app");
					lock (_lockGameApp)
					{
						Log.Debug("Locked game app");
						if (_gameApp == null)
						{
							Log.Debug("Game app is still null");
							_gameApp =
								JsonConvert.DeserializeObject<GameApp>(
									File.ReadAllText(GamePathLoader
										.LoadEm5GameAppJsonPath()));
							
							Log.Debug($"Game app is null? {_gameApp != null}");
						}
					}
				}

				return _gameApp;
			}
			private set => _gameApp = value;
		}

		public static string GetSerializedGameSettings(Formatting f = Formatting.Indented)
		{
			return JsonConvert.SerializeObject(GameSettings, f, Converters);
		}

		public static void SaveGameSettings()
		{
			DisablePrivacy();
			File.WriteAllText(GamePathLoader.LoadEm5SettingsFilePath(), GetSerializedGameSettings());
			Log.Debug($"Saved em5 settings to: {GamePathLoader.LoadEm5SettingsFilePath()}");
			EnablePrivacy();
		}

		private static void DisablePrivacy()
		{
			GameSettings.ParameterGroups.Em5GameSettingsGroup.SerializeWithPrivacy = false;
			GameSettings.ParameterGroups.Em5MultiplayerSettingsGroup.SerializeWithPrivacy = false;
		}

		private static void EnablePrivacy()
		{
			GameSettings.ParameterGroups.Em5GameSettingsGroup.SerializeWithPrivacy = true;
			GameSettings.ParameterGroups.Em5MultiplayerSettingsGroup.SerializeWithPrivacy = true;
		}
	}
}