using System;
using System.IO;

namespace EmergencyX.Emergency5.Utils
{
	public class CacheClearing
	{
		public static void ClearCache(string pathToClear)
		{
			if (pathToClear == null)
			{
				throw new ArgumentNullException();
			}

			if (!Directory.Exists(pathToClear))
			{
				throw new DirectoryNotFoundException(pathToClear);
			}
			
			// security against accidental deletions
			if (!pathToClear.Contains(@"Promotion Software GmbH\EMERGENCY 5\cache"))
			{
				throw new InvalidOperationException($"Should not delete {pathToClear}");
			}

			DirectoryInfo dir = new DirectoryInfo(pathToClear);
			foreach (FileInfo file in dir.GetFiles())
			{
				file.Delete();
			}

			foreach (DirectoryInfo dirs in dir.GetDirectories())
			{
				//clear sub dirs
				ClearCache(dirs.FullName);
				dirs.Delete();
			}
		}
	}
}