namespace EmergencyX.FMS.Data
{
	public class DefaultSettings
	{
		public static string DefaultFmsServer => @"http://fms.emergencyx.de";

		public static string DefaultFmsGameObjectConfig => "config.json";

	}
}