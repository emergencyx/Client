using EmergencyX.Core;
using Newtonsoft.Json;

namespace EmergencyX.FMS.Data
{
	public class GameObject : BaseObject
	{
		private string _name;
		private string _status;

		[JsonProperty("name")]
		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("status")]
		public string Status
		{
			get
			{
				return _status;
			}

			set
			{
				_status = value;
				OnPropertyChanged();
			}
		}
	}
}