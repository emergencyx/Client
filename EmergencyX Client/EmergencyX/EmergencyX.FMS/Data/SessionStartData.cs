using EmergencyX.Core;
using Newtonsoft.Json;

namespace EmergencyX.FMS.Data
{
	public class SessionStartData : BaseObject
	{
		private string _name;
		private string _password;
		private string _style;
		private object _objects;

		[JsonProperty("name")]
		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("password")]
		public string Password
		{
			get
			{
				return _password;
			}

			set
			{
				_password = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("style")]
		public string Style
		{
			get
			{
				return _style;
			}

			set
			{
				_style = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("objects")]
		public object Objects
		{
			get
			{
				return _objects;
			}

			set
			{
				_objects = value;
				OnPropertyChanged();
			}
		}
	}
}