using EmergencyX.Core;

namespace EmergencyX.FMS.Data
{
	public class SessionStartResponse : BaseObject
	{
		private string _status;
		private string _sessionId;

		public string SessionId
		{
			get
			{
				return _sessionId;
			}

			set
			{
				_sessionId = value;
				OnPropertyChanged();
			}
		}

		public string Status
		{
			get
			{
				return _status;
			}

			set
			{
				_status = value;
				OnPropertyChanged();
			}
		}
	}
}