using System.Collections.Generic;
using EmergencyX.Core;
using Newtonsoft.Json;

namespace EmergencyX.FMS.Data
{
	public class SessionUpdateData : BaseObject
	{
		private string _sessionId;
		private List<GameObject> _objects;

		[JsonProperty("sessionId")]
		public string SessionId
		{
			get
			{
				return _sessionId;
			}

			set
			{
				_sessionId = value;
				OnPropertyChanged();
			}
		}

		[JsonProperty("objects")]
		public List<GameObject> Objects
		{
			get
			{
				return _objects;
			}

			set
			{
				_objects = value;
				OnPropertyChanged();
			}
		}
	}
}