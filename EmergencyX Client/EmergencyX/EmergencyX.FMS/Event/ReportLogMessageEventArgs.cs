using System;
using System.Collections.ObjectModel;
using EmergencyX.FMS.Data;

namespace EmergencyX.FMS.Event
{
	public class ReportLogMessageEventArgs : EventArgs
	{
		private ObservableCollection<GameObject> _updatedItems;

		public ObservableCollection<GameObject> UpdatedItems
		{
			get
			{
				return _updatedItems;
			}

			private set
			{
				_updatedItems = value;
			}
		}

		public ReportLogMessageEventArgs(ObservableCollection<GameObject> items)
		{
			UpdatedItems = items;
		}
	}
}