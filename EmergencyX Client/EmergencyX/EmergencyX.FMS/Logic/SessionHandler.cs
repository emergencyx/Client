using System;
using EmergencyX.Core.Logic.Settings;
using EmergencyX.Core.Networking;
using EmergencyX.FMS.Data;
using Newtonsoft.Json;
using RestSharp;

namespace EmergencyX.FMS.Logic
{
	public class SessionHandler
	{
		private static string _baseUrl;

		public static string BaseUrl
		{
			get { return _baseUrl; }

			set { _baseUrl = value; }
		}

		/// <summary>
		/// Erzeugt einen RestSharp RestRequest mit JSON Body
		/// </summary>
		/// <param name="target"></param>
		/// <param name="method"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		private static RestRequest RestRequestFactotry(string target, Method method, string data)
		{
			RestRequest request = new RestRequest(target, method);
			request.RequestFormat = DataFormat.Json;
			request.AddJsonBody(data);

			return request;
		}

		/// <summary>
		/// Erstellt die Sessionstartquery und sendet diese an den Server
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static SessionStartResponse SessionStart(string data)
		{
			// Client zum Senden der Abfrage erstellen
			var client = RestConnectionProvider.GetRestClient(BaseUrl);

			// Requestformat vorbereiten
			var sessionCreate = RestRequestFactotry("/create", Method.Post, data);

			// senden und Rückmeldung emfangen
			RestResponse response = client.Execute(sessionCreate);

			return JsonConvert.DeserializeObject<SessionStartResponse>(response.Content);
		}

		/// <summary>
		/// Erstellt Sessionupdates und sendet diese an den FMS Server
		/// </summary>
		/// <param name="data"></param>
		public static void SessionUpdate(string data)
		{
			var client = RestConnectionProvider.GetRestClient(BaseUrl);

			var updateSession = RestRequestFactotry("/update", Method.Post, data);

			RestResponse response = client.Execute(updateSession);
		}
	}
}