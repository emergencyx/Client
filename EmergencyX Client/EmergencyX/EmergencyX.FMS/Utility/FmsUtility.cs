using System;
using System.IO;

namespace EmergencyX.FMS.Utility
{
	public class FmsUtility
	{
		public static string FindConfigurationFile(string modPath, string configFile)
		{
			// get all results in the mod directory for the given config file
			var dir = new DirectoryInfo(modPath);
			var result = dir.GetFiles(configFile, SearchOption.AllDirectories);

			// no result
			if (result.Length == 0)
			{
				return String.Empty;
			}

			// one match is the best case
			if (result.Length == 1)
			{
				return result[0].FullName;
			}

			// if there is more then one result see if any have "fms" in the folder path, and if so pick the first with fms in the folder name
			for (int i = 0; i < result.Length; i++)
			{
				if (result[i].FullName.Contains("fms",
					    StringComparison.InvariantCultureIgnoreCase))
				{
					return result[i].FullName;
				}
			}

			// if there is more then one and no one with fms in the path, just return the first one
			return result[0].FullName;
		}
	}
}