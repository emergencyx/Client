﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using System.Security.Principal;
using EmergencyX.Core.Logic;
using EmergencyX.Core.NamedPipeConnection;

if (args.Length == 0)
{
    Console.WriteLine("Started without parameters - exiting.");
    return;
}

// check if running as admin => exit if true as we do not want a situation 
// that allows external factors triggering something with admin perms
using var id = WindowsIdentity.GetCurrent();
var principal = new WindowsPrincipal(id);
if (principal.IsInRole(WindowsBuiltInRole.Administrator))
{
    Console.WriteLine("Please do not run this application as admin.");
    return;
}

// check if EmergencyX is running
var result = Process.GetProcesses().SingleOrDefault(x => x.ProcessName == "EmergencyX.Client");

bool wasRunning = true;
if (result == null)
{
    wasRunning = false;
#if !DEBUG
	var path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location) ?? string.Empty,
		"EmergencyX.Client.exe");
#else
    var path =
        "D:\\Programmierung\\Git\\Client\\EmergencyX Client\\EmergencyX\\EmergencyX.Client\\bin\\Debug\\net6.0\\EmergencyX.Client.exe";
#endif
    var startArgument = new ProcessStartInfo(path);
    startArgument.UseShellExecute = true;
    Process.Start(startArgument);

    do
    {
        Thread.Sleep(1500);
        result = Process.GetProcesses().SingleOrDefault(x => x.ProcessName == "EmergencyX.Client");
    } while (result == null);
}

Console.WriteLine("Connecting to Emergency Explorer application.");

var client = new CommunicationClient(NamedPipeSettings.MAIN_PIPE_NAME);

client.Connect();

if (!client.IsConnected())
{
    Console.WriteLine("Failed to connect to Emergency Explorer. Please contact support or try again.");
    client.Disconnect();
#if DEBUG
    Console.ReadKey();
#endif
    return;
}

// check inputs for bad stuff
var dataStr = args[0];

try
{
    if (!NamedPipeMessageValidation.ValidatePattern(dataStr))
    {
        Console.WriteLine("Illegal URL! Abort.");
        client.Disconnect();
// #if DEBUG
//         Console.ReadKey();
// #endif
        return;
    }

    if (!wasRunning)
    {
        // wait for Emergency Explorer to be ready to receive messages
        Thread.Sleep(5000);
    }
    
    client.SendString(dataStr);
    client.Disconnect();
}
catch (Exception e)
{
    Console.WriteLine(e);
    Console.Write("Please note this error message. Press any key to end..");
    Console.ReadKey();
}
//
// #if DEBUG
// Console.ReadKey();
// #endif