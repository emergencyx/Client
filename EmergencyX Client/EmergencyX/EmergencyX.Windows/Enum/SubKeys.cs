namespace EmergencyX.Windows.Enum
{
	public enum SubKeys
	{
		LocalMachine,
		CurrentUser,
		CurrentConfig,
		CurrentClassesRoot,
		Users
	}
}