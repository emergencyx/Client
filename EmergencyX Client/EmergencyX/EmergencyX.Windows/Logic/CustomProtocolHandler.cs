using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EmergencyX.Core.NamedPipeConnection;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Windows.Enum;
using EmergencyX.Windows.Exceptions;

namespace EmergencyX.Windows.Logic;

public class CustomProtocolHandler
{
    public static async void RemoveCustomProtocolHandler(string currentAppPath)
    {
        // check if registry still is set up in a way that contains the data
        var data = RegistryAccess.GetRegistryValueFromSubKey(NamedPipeSettings.MAIN_HANDLER_KEY_NAME,
            SubKeys.CurrentClassesRoot,
            "");

        if (data == String.Empty)
        {
            throw new RegistryKeyNotPresentException();
        }

        if (!File.Exists(Path.Combine(currentAppPath, "EmergencyX.ElevatedAction.exe")))
        {
            throw new FileNotFoundException(string.Format("Unable to load file: {0}]",
                Path.Combine(currentAppPath, "EmergencyX.ElevatedAction.exe")));
        }

        var executor = Path.Combine(currentAppPath, "EmergencyX.ElevatedAction.exe");
        var startArgs = new ProcessStartInfo(executor, "RemoveCustomProtocolHandlerCommand");
        startArgs.WindowStyle = ProcessWindowStyle.Hidden;
        if (Platform.IsWindows)
        {
            startArgs.Verb = "runas";
            startArgs.UseShellExecute = true;
        }

        await Process.Start(startArgs)?.WaitForExitAsync()!;
    }

    public static async void AddCustomProtocolHandler(string currentAppPath)
    {
        // check if registry still is set up in a way that contains the data
        var data = RegistryAccess.GetRegistryValueFromSubKey("emergencyx", SubKeys.CurrentClassesRoot,
            "URL Protocol");

        if (data != String.Empty)
        {
            throw new RegistryKeyNotPresentException();
        }

        if (!File.Exists(Path.Combine(currentAppPath, "EmergencyX.ElevatedAction.exe")))
        {
            throw new FileNotFoundException(string.Format("Unable to load file: {0}]",
                Path.Combine(currentAppPath, "EmergencyX.ElevatedAction.exe")));
        }

        var executor = Path.Combine(currentAppPath, "EmergencyX.ElevatedAction.exe");
        var startArgs = new ProcessStartInfo(executor, "AddCustomProtocolHandlerCommand");
        startArgs.WindowStyle = ProcessWindowStyle.Hidden;
        if (Platform.IsWindows)
        {
            startArgs.Verb = "runas";
            startArgs.UseShellExecute = true;
        }

        await Process.Start(startArgs)?.WaitForExitAsync()!;
    }
}