using System;
using System.Diagnostics;

namespace EmergencyX.Windows.Logic
{
	public class DxDiag
	{
		/// <summary>
		/// Creates a DxDiag file
		/// </summary>
		/// <param name="path">Path to the output file</param>
		public static void CreateDxDiagFile(string path)
		{
			// start information for dx diag tool
			// => /t saves the DxDiag output to a file
			var processstart = new ProcessStartInfo("dxdiag", $"/t {path}");
			
			// run
			var p= Process.Start(processstart);
			
			// Wait for DxDiag
			p.WaitForExit();
			
			// all good?
			if (p.ExitCode != 0)
			{
				throw new Exception($"DxDiag Error, exit Code {p.ExitCode}");
			}
		}
	}
}