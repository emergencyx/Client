using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using EmergencyX.Core.OperatingSystems;
using EmergencyX.Windows.Enum;
using log4net;
using Microsoft.Win32;

namespace EmergencyX.Windows.Logic
{
	public class RegistryAccess
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(RegistryAccess));

		public static string GetRegistryValueFromSubKey(string key, SubKeys type, string requestedValue)
		{
			Log.Debug(string.Format("Value of key='{0}'", key));
			Log.Debug(string.Format("Value of requestedValue='{0}'", requestedValue));
			Log.Debug(string.Format("Value of type='{0}'", type));

			if (!Platform.IsWindows)
			{
				return null;
			}

			// we don't check requestedValue because if that is null it means "default"
			if (string.IsNullOrEmpty(key))
			{
				return String.Empty;
			}

			if (type == SubKeys.CurrentClassesRoot)
			{
				Log.Debug("Using CurrentClassRoot");
				RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(key);

				//if there is no key
				//
				if (regKey == null)
				{
					return String.Empty;
				}

				string value = regKey.GetValue(requestedValue)?.ToString();
				Log.Debug(string.Format("Value ='{0}'", value));
				return value;
			}
			else if (type == SubKeys.CurrentConfig)
			{
				Log.Debug("Using CurrentConfig");
				RegistryKey regKey = Registry.CurrentConfig.OpenSubKey(key);

				//if there is no key
				//
				if (regKey == null)
				{
					return String.Empty;
				}

				string value = regKey.GetValue(requestedValue)?.ToString();
				Log.Debug(string.Format("Value ='{0}'", value));
				return value;
			}
			else if (type == SubKeys.CurrentUser)
			{
				Log.Debug("Using CurrentUser");
				RegistryKey regKey = Registry.CurrentUser.OpenSubKey(key);

				//if there is no key
				//
				if (regKey == null)
				{
					return String.Empty;
				}

				string value = regKey.GetValue(requestedValue)?.ToString();
				Log.Debug(string.Format("Value ='{0}'", value));
				return value;
			}
			else if (type == SubKeys.LocalMachine)
			{
				Log.Debug("Using LocalMachine");
				RegistryKey regKey = Registry.LocalMachine.OpenSubKey(key);

				//if there is no key
				//
				if (regKey == null)
				{
					return String.Empty;
				}

				string value = regKey.GetValue(requestedValue)?.ToString();
				Log.Debug(string.Format("Value ='{0}'", value));
				return value;
			}
			else if (type == SubKeys.Users)
			{
				Log.Debug("Using Users");
				RegistryKey regKey = Registry.Users.OpenSubKey(key);

				//if there is no key
				//
				if (regKey == null)
				{
					return String.Empty;
				}

				string value = regKey.GetValue(requestedValue)?.ToString();
				Log.Debug(string.Format("Value ='{0}'", value));
				return value;
			}

			throw new NotSupportedException("SubTree not found!");
		}

		public static void AddRegistryKeyFromSubKey(string key, SubKeys type)
		{
			if (!Platform.IsWindows || key == string.Empty)
			{
				return;
			}

			if (type == SubKeys.CurrentClassesRoot)
			{
				Log.Debug("Using CurrentClassRoot");
				using RegistryKey regKey = Registry.ClassesRoot.CreateSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree);
				return;
			}
			else if (type == SubKeys.CurrentConfig)
			{
				Log.Debug("Using CurrentConfig");
				using RegistryKey regKey = Registry.CurrentConfig.CreateSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree);
				return;
			}
			else if (type == SubKeys.CurrentUser)
			{
				Log.Debug("Using CurrentUser");
				using RegistryKey regKey = Registry.CurrentUser.CreateSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree);
				return;
			}
			else if (type == SubKeys.LocalMachine)
			{
				Log.Debug("Using LocalMachine");
				using RegistryKey regKey = Registry.LocalMachine.CreateSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree);
				return;
			}
			else if (type == SubKeys.Users)
			{
				Log.Debug("Using Users");
				using RegistryKey regKey = Registry.Users.CreateSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree);
				return;
			}

			throw new NotSupportedException("SubTree not found!");
		}

		public static void RemoveRegistryKeyTreeFromSubKey(string key, SubKeys type)
		{
			Log.Debug(string.Format("Value of key='{0}'", key));
			Log.Debug(string.Format("Value of type='{0}'", type));

			if (!Platform.IsWindows || string.IsNullOrEmpty(key))
			{
				return;
			}

			if (type != SubKeys.Users && type != SubKeys.CurrentConfig && type != SubKeys.CurrentClassesRoot &&
			    type != SubKeys.CurrentUser && type != SubKeys.LocalMachine)
			{
				throw new NotSupportedException();
			}

			RegistryKey regKey;
			switch (type)
			{
				case SubKeys.LocalMachine:
					regKey = Registry.LocalMachine.OpenSubKey(key);
					break;
				case SubKeys.CurrentUser:
					regKey = Registry.CurrentUser.OpenSubKey(key);
					break;
				case SubKeys.CurrentConfig:
					regKey = Registry.CurrentConfig.OpenSubKey(key);
					break;
				case SubKeys.CurrentClassesRoot:
					regKey = Registry.ClassesRoot.OpenSubKey(key);
					break;
				case SubKeys.Users:
					regKey = Registry.Users.OpenSubKey(key);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}

			if (key == null)
			{
				return;
			}
			switch (type)
			{
				case SubKeys.LocalMachine:
					Registry.LocalMachine.DeleteSubKeyTree(key);
					break;
				case SubKeys.CurrentUser:
					Registry.CurrentUser.DeleteSubKeyTree(key);
					break;
				case SubKeys.CurrentConfig:
					Registry.CurrentConfig.DeleteSubKeyTree(key);
					break;
				case SubKeys.CurrentClassesRoot:
					Registry.ClassesRoot.DeleteSubKeyTree(key);
					break;
				case SubKeys.Users:
					Registry.Users.DeleteSubKeyTree(key);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}
		
		public static void AddRegistryValueToSubkey(string key, SubKeys type, KeyValuePair<string, string> data)
		{
			Log.Debug(string.Format("Value of key='{0}'", key));
			Log.Debug(string.Format("Value of set value {0} - {1}", data.Key, data.Value));
			Log.Debug(string.Format("Value of type='{0}'", type));

			if (!Platform.IsWindows || string.IsNullOrEmpty(key))
			{
				return;
			}

			if (type == SubKeys.CurrentClassesRoot)
			{
				Log.Debug("Using CurrentClassRoot");
				using RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);
				//if there is no key
				//
				// if (regKey == null)
				// {
				// 	return;
				// }

				var dataString = data.Value;
				if (data.Value.Contains("\\"))
				{
					dataString = data.Value.Replace(@"\", @"\\");
				}

				regKey.SetValue(data.Key, dataString);
				regKey.Dispose();
				return;
			}
			else if (type == SubKeys.CurrentConfig)
			{
				Log.Debug("Using CurrentConfig");
				using RegistryKey regKey = Registry.CurrentConfig.OpenSubKey(key);

				//if there is no key
				//
				if (regKey == null)
				{
					return;
				}

				regKey.SetValue(data.Key, data.Value);
				return;
			}
			else if (type == SubKeys.CurrentUser)
			{
				Log.Debug("Using CurrentUser");
				using RegistryKey regKey = Registry.CurrentUser.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);

				//if there is no key
				//
				if (regKey == null)
				{
					return;
				}

				regKey.SetValue(data.Key, data.Value);
				return;
			}
			else if (type == SubKeys.LocalMachine)
			{
				Log.Debug("Using LocalMachine");
				using RegistryKey regKey = Registry.LocalMachine.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);

				//if there is no key
				//
				if (regKey == null)
				{
					return;
				}

				regKey.SetValue(data.Key, data.Value);
				return;
			}
			else if (type == SubKeys.Users)
			{
				Log.Debug("Using Users");
				using RegistryKey regKey = Registry.Users.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);

				//if there is no key
				//
				if (regKey == null)
				{
					return;
				}

				regKey.SetValue(data.Key, data.Value);
				return;
			}

			throw new NotSupportedException("SubTree not found!");
		}

		public static void RemoveRegistryValueFromSubKey(string key, SubKeys type, string valueName)
		{
						Log.Debug(string.Format("Value of key='{0}'", key));
			Log.Debug(string.Format("ValueName of remove value: {0}", valueName));
			Log.Debug(string.Format("Value of type='{0}'", type));

			if (!Platform.IsWindows || string.IsNullOrEmpty(key))
			{
				return;
			}

			if (type == SubKeys.CurrentClassesRoot)
			{
				Log.Debug("Using CurrentClassRoot");
				using RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);

				if (regKey != null)
				{
					regKey.DeleteValue(valueName);
				}
				return;
			}
			else if (type == SubKeys.CurrentConfig)
			{
				Log.Debug("Using CurrentConfig");
				using RegistryKey regKey = Registry.CurrentConfig.OpenSubKey(key);

				if (regKey != null)
				{
					regKey.DeleteValue(valueName);
				}
				return;
			}
			else if (type == SubKeys.CurrentUser)
			{
				Log.Debug("Using CurrentUser");
				using RegistryKey regKey = Registry.CurrentUser.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);

				if (regKey != null)
				{
					regKey.DeleteValue(valueName);
				}
				return;
			}
			else if (type == SubKeys.LocalMachine)
			{
				Log.Debug("Using LocalMachine");
				using RegistryKey regKey = Registry.LocalMachine.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);

				if (regKey != null)
				{
					regKey.DeleteValue(valueName);
				}
				return;
			}
			else if (type == SubKeys.Users)
			{
				Log.Debug("Using Users");
				using RegistryKey regKey = Registry.Users.OpenSubKey(key,
					RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.SetValue);

				if (regKey != null)
				{
					regKey.DeleteValue(valueName);
				}
				return;
			}

			throw new NotSupportedException("SubTree not found!");
		}
	}
}