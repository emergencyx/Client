using EmergencyX.Windows.Enum;

namespace EmergencyX.Windows.Logic
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using EmergencyX.Windows.Tools;
	using System.IO;
	using System.Text.RegularExpressions;

	namespace EmergencyX.Windows.Tools
	{
		public class Steam
		{
			public const string SteamLibraryPath = @"steamapps\libraryfolders.vdf";
			public const string STEAM_EXE = "steam.exe";

			/// <summary>
			/// Gets the steam instalation path from steam
			/// </summary>
			/// <returns></returns>
			public static string GetSteamPath()
			{
				return RegistryAccess.GetRegistryValueFromSubKey(@"SOFTWARE\WOW6432Node\Valve\Steam",
					SubKeys.LocalMachine, "InstallPath");
			}

			/// <summary>
			/// Returns a Steam Game Path for a given steam game id
			/// </summary>
			/// <param name="steamgamefoldername"></param>
			/// <returns></returns>
			public static string GetSteamGamePath(int steamgameid)
			{
				// get Steam libraries
				// two methods are used here due to a recent update in the beta branch of steam
				//
				var steampaths = GetSteamLibrariesByLibraryId(GetSteamPath());
				steampaths.AddRange(GetSteamLibrariesByPathProperty(GetSteamPath()));

				if (steampaths.Count == 0)
				{
					return String.Empty;
				}

				// the steam installation is the default library, so add that too
				//
				steampaths.Add(GetSteamPath());
				var result = String.Empty;
				foreach (var entry in steampaths)
				{
					var path = Path.Combine(entry, "steamapps");

					if (!Directory.Exists(path))
					{
						continue;
					}
					
					var files = Directory.GetFiles(path, "*.acf", SearchOption.TopDirectoryOnly);

					// if there are no acf files there might be no games installed
					//
					if (files.Length == 0)
					{
						break;
					}

					// check if the filename is related to the game we need
					//
					foreach (var file in files)
					{
						if (!file.Contains(steamgameid.ToString()))
						{
							continue;
						}

						var lines = File.ReadAllLines(file);

						// check the game acf file for the install dir
						//
						foreach (var line in lines)
						{
							if (line.Contains("installdir"))
							{
								var lineres = line.Trim().Split('\t');
								result = Path.Combine(path, "common",
									lineres[2].Replace("\"", ""));
								return result;
							}
						}
					}
				}

				// nothing found :(
				//
				return result;
			}

			/// <summary>
			/// Parses the libraryfolders.vdf file and returns an Key Value Dictionary search pattern that only fits lines that look like '	"1"		"D:\\SteamLibrary"'
			/// </summary>
			/// <returns>Returns all Steam libraries in the libraryfolders.vdf from Steam</returns>
			internal static List<string> GetSteamLibrariesByLibraryId(string steamdir)
			{
				var result = new List<string>();

				if (string.IsNullOrEmpty(steamdir))
				{
					return result;
				}

				var lines = File.ReadAllLines(Path.Combine(steamdir, SteamLibraryPath));

				// check for empty file
				//
				if (lines.Length == 0)
				{
					return result;
				}

				// search pattern that only fits lines that look like
				// '	"1"		"D:\\SteamLibrary"'
				//
				var linetosearch = new Regex("\t\"[0-9]\"\t\t");

				foreach (var line in lines)
				{
					// this line won't give us any library
					//
					if (line.Contains("TimeNextStatsReport") || line.Contains("ContentStatsID"))
					{
						continue;
					}

					// infact only lines that start with a tab followed by a number in quotationmarks followed by two tabs are of interest at all
					//
					if (!linetosearch.IsMatch(line))
					{
						continue;
					}

					// at this point, we are pretty sure we found a line in the file that fits our needs :)
					//
					var linecontent = line.Trim().Split('\t');
					result.Add(linecontent[2].Replace(@"\\", @"\").Replace("\"", ""));
				}

				return result;
			}

			/// <summary>
			/// Parses the libraryfolders.vdf file and returns an Key Value Dictionary search pattern that only fits lines that look like '		"path"		"F:\\SteamLibrary"'
			/// </summary>
			/// <returns>Returns all Steam libraries in the libraryfolders.vdf from Steam</returns>
			internal static List<string> GetSteamLibrariesByPathProperty(string steamdir)
			{
				var result = new List<string>();

				if (string.IsNullOrEmpty(steamdir))
				{
					return result;
				}

				var lines = File.ReadAllLines(Path.Combine(steamdir, SteamLibraryPath));

				// check for empty file
				//
				if (lines.Length == 0)
				{
					return result;
				}

				// search pattern that only fits lines that look like
				// '		"path"		"F:\\SteamLibrary"'
				//
				var linetosearch = new Regex("\t\t\"path\"\t\t\"[A-Z]:\\\\(.){0,}\"");

				foreach (var line in lines)
				{
					// this line won't give us any library
					//
					if (line.Contains("TimeNextStatsReport") || line.Contains("ContentStatsID"))
					{
						continue;
					}

					// infact only lines that start with a tab followed by a number in quotation's followed by two tabs are of interest at all
					//
					if (!linetosearch.IsMatch(line))
					{
						continue;
					}

					// at this point, we are pretty sure we found a line in the file that fits our needs :)
					//
					var linecontent = line.Trim().Split('\t');
					result.Add(linecontent[2]
						.Replace("\"",
							"")); // [2] and not [1] because the second \t in the string is [1]
				}

				return result;
			}
		}
	}
}