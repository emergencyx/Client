using System;
using EmergencyX.Core.Interfaces;
using EmergencyX.Windows.Enum;

namespace EmergencyX.Windows.Logic
{
	public class UiInformation : IUiInformation
	{
		public bool IsDarkMode()
		{
			var value = RegistryAccess.GetRegistryValueFromSubKey(
				@"Software\Microsoft\Windows\CurrentVersion\Themes\Personalize", SubKeys.CurrentUser,
				"AppsUseLightTheme");

			// If the value can not be read from the db then return false
			if (value == null)
			{
				return false;
			}

			// the above key will give "0" if dark mode or 1 if light mode
			bool result;
			switch (value)
			{
				case "0":
					result = false;
					break;
				case "1":
					result = true;
					break;
				default:
					result = true;
					break;
			}

			if (string.IsNullOrEmpty(value) || result)
			{
				return false;
			}

			return true;
		}
	}
}