# Client

# External Resources / Thanks
This software is built using Avalonia GUI technology, licensed under the MIT License https://github.com/AvaloniaUI/Avalonia

This program uses Json.NET (Newtonsoft.JSON) licensed under the MIT License https://github.com/JamesNK/Newtonsoft.Json

This software includes some icons from the well known Font Awesome, in XAML form https://fontawesome.com/ Icons: CC BY 4.0 Code: 
MIT License

This software makes use of the powerful log4net library, licensed under the Apache 2.0 License https://logging.apache.org/log4net/

This software is built  using RestSharp, licensed under the Apache 2.0 License https://github.com/restsharp/RestSharp

This program uses SharpZipLib licensed under the MIT License https://github.com/icsharpcode/SharpZipLib

This software uses NetSparkle, licensed under the MIT License https://github.com/NetSparkleUpdater/NetSparkle