Emergency Explorer Backend
==========================
Serves projects and releases for the Emergency Explorer Client.

## Public API
- `/api/projects`    
  Request all projects.
  ```json
    [
      {
        "id": "L76Scp9F5XXhw2AZbsYH",
        "name": "Bieberfelde Next Generation",
        "latestVersion": "0.1.0",
        "latestHash": "sha512-in-base16-encoding",
        "game": "EM5"
      }
    ]
  ```
- `/api/projects/:project`    
  Returns the project with id `:project`.
  ```json
    {
      "id": "L76Scp9F5XXhw2AZbsYH",
      "name": "Bieberfelde Next Generation",
      "game": "EM5",
      "releases": [
          {
            "version": "0.1.0",
            "hash": "sha512-in-base16-encoding",
            "artifacts": [
                {
                  "href": "https://full-url.to/download/destination.zip",
                  "hash": "sha512-in-base16-encoding"
                },
                {
                  "href": "https://full-url.to/another-download/destination.zip",
                  "hash": "sha512-in-base16-encoding",
                  "from": "0.0.5"
                }
            ]
          }
      ]
    }
  ```
  The `project` object includes a `game` field that takes one of the values `EM4, EM5, EMONE`.
  Within a project, a list of available `releases` can be found.
  Every `release` has a semver-compatible version and a checksum `hash` that can be used to verify the installed contents.
  The `artifacts` section can contain multiple download destinations. Choose one at random.
  Additionally any artifact may contain a `from` field to indicate an incremental update.
  The incremental update can reduce the required download size.
  The version indicated in the `from` field must be properly installed.
  The `hash` field must be used to validate the received content from the URL given in the `href` field.
  Only if the `hash` is valid the client may extract the contents.

  ## Requirements for modifications in the phonebook
  Em4
  - EM4 mods must be a ZIP file containing the folder already
