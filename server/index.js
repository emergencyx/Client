const express = require('express');
const api = express();

api.use(express.json());
api.use('/projects', require('./src/routes/projects'));
api.use('/health', (req, res) => res.send('🚀'));
api.use('/', (req, res) => res.sendStatus(404));

module.exports = { api };
