const express = require('express');
const basicAuth = require('express-basic-auth')
const router = express.Router();
const compareBuild = require('semver/functions/compare-build');
const valid = require('semver/functions/valid');

const Firestore = require('@google-cloud/firestore');
const {enabled} = require("express/lib/application");
const db = new Firestore({ projectId: process.env.PROJECT_ID });
const collection = process.env.DATABASE;

router.get('/', async (req, res) => {
    const projects = await db.collection(collection).where("enabled","==",true).get();
    res.json(projects.docs
        .map(document => {
            const project = document.data();
            const releases = project.releases || [];
            const versions = Array.from(releases).sort((a, b) => compareBuild(a.version, b.version));

            const response = {
                id: document.ref.id,
                name: project.name,
                game: project.game,
                enabled: project.enabled
            };

            if (versions.length > 0) {
                const latestRelease = versions.pop();
                response.latestVersion = latestRelease.version;
                response.latestHash = latestRelease.hash || '';
            }

            return response;
        }));
});

router.get('/:project', async (req, res) => {
    const document = await db.collection(collection).doc(req.params.project).get();
    if (!document.exists) {
        res.sendStatus(404);
        return;
    }

    const project = document.data();

    if(!project.enabled) {
        res.sendStatus(404);
        return;
    }

    res.json({
        id: document.ref.id,
        name: project.name,
        releases: project.releases,
        game: project.game,
        enabled: project.enabled
    });
});

router.use(basicAuth({
    users: { 'deploy': process.env.AUTH }
}));
router.use(express.json());

router.post('/:project/releases', async (req, res) => {
    const documentRef = db.collection(collection).doc(req.params.project);
    const document = await documentRef.get();
    if (!document.exists) {
        res.sendStatus(404);
        return;
    }

    const version = valid(req.body.version, {strict: true});
    if (version == null) {
        console.log('Bad version or failed to parse')
        res.sendStatus(400);
        return;
    }

    const project = document.data();
    console.log(`Create release ${version} for ${project.name}`);

    const releases = project.releases || [];
    const release = {
        artifacts: req.body.artifacts,
        version: version,
        hash: req.body.hash,
    };
    releases.push(release);

    await documentRef.update({
        releases: releases
    });

    res.sendStatus(201);
});

module.exports = router;